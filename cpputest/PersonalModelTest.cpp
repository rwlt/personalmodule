/*
 * PersonalModelTest.cpp
 *
 *  Created on: 28/07/2017
 *      Author: rodney
 */


#include "module/Person.h"
#include "module/Entity.h"
#include "module/PersonalModel.h"
#include "MockMainView.h"
//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"
#include "CppUTestExt/MockSupport.h"

//
//-------------------------------------------------------
// PersonalModelAccessor test group
//-------------------------------------------------------
TEST_GROUP(PersonalModelAccessor)
{
	MockControllerViewFactory factory;
    std::unique_ptr<Module::Controller> controller{std::unique_ptr<Module::Controller>(new Module::Controller(&factory))};
    std::unique_ptr<MockMainView> mainView{std::unique_ptr<MockMainView>(new MockMainView())};
    std::unique_ptr<MockDBLoginView> loginView{std::unique_ptr<MockDBLoginView>(new MockDBLoginView())};
	Module::PersonalModel model;
    Module::MainWindowInterface *pIMainView = mainView.get();
    Module::DBLoginWindowInterface *pILoginView{loginView.get()};
    std::unique_ptr<Module::MainPresenter> mainPresenter{std::unique_ptr<Module::MainPresenter>(new Module::MainPresenter(mainView.get(), controller.get()))};
    std::unique_ptr<Module::DBLoginPresenter> loginPresenter{std::unique_ptr<Module::DBLoginPresenter>(new Module::DBLoginPresenter(loginView.get(), controller.get()))};
    PersonComparator comparatorPerson;
    ViewIdentityComparator comparatorViewIdentity;
	std::function<void ( Module::Person& p, bool remove)> func;

    void setup() {
    	func = [&] ( Module::Person&, bool) {
        	mainPresenter->complete_dbworker();
        };
    	model.addBroadcast(func);

        mock().installComparator("Person", comparatorPerson);
        mock().installComparator("ViewIdentity", comparatorViewIdentity);
        mock().expectOneCall("getStatus").onObject(mainView.get());
        mock().expectOneCall("getPersonList").onObject(mainView.get());
        mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
        mock().expectNCalls(1, "setItem").onObject(&mainView->statusItem).withStringParameter("item", "Ready");
        mainPresenter->initialize();
        loginPresenter->initialize();
        mock().checkExpectations();
   }
    void teardown() {
         mock().clear();
         mock().removeAllComparatorsAndCopiers();
     }


};

TEST(PersonalModelAccessor, DBVersionAccessor)
{
	STRCMP_EQUAL("Module DBStorage 1.0 - September 6, 2016", model.getDBVersion().c_str());
}

TEST(PersonalModelAccessor, BroadcastAccessor)
{
	// Get the setup scenario done
    // Main presenter then mock to show the DBLogin View and that will cause the Username and password
    // fields to be setup. Also login role and is logged states
    // Then we can perform the PersonEdit presenter checks under that scenario.
    mock().expectOneCall("readEditControl").onObject(mainView.get());
    mock().expectNCalls(1, "createInstance").onObject(&factory);
    mock().expectNCalls(2, "loginInterface").onObject(&factory).andReturnValue(pILoginView);
    //The controller will call loginuser presenter loadMessage to set up the view
    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_username).withStringParameter("item", "");
    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_password).withStringParameter("item", "");
    mock().expectNCalls(1, "getRole").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_userrole);
    mock().expectNCalls(1, "getLoggedState").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_logged);
    mock().expectOneCall("getValidationError").onObject(loginView.get());
    mock().expectOneCall("clear").onObject(&loginView->m_validationList);
    mock().expectOneCall("showView").onObject(loginView.get());
    mainPresenter->loginUser();
    mock().checkExpectations();
    mock().clear();
    // Login a known user
    std::string username("TEST");
    std::string password("pass");
    Module::UserRole roletype = Module::UserRole::ADMIN;
    mock().expectOneCall("readEditControl").onObject(loginView.get());
    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_username).andReturnValue(&username);
    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_password).andReturnValue(&password);
    mock().expectNCalls(1, "getRole").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_userrole).andReturnValue(&roletype);
    // Validation happens on entered data so clear and prepare for validation errors.
    mock().expectOneCall("getValidationError").onObject(loginView.get());
    mock().expectOneCall("clear").onObject(&loginView->m_validationList);
    // Known user and then the ctrl get user login details and close login view
    mock().expectNCalls(1, "getLoggedState").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_logged);
    mock().expectNCalls(1, "loginInterface").onObject(&factory).andReturnValue(pILoginView);
    mock().expectOneCall("closeView").onObject(loginView.get());

    mock().expectNCalls(2, "mainInterface").onObject(&factory).andReturnValue(pIMainView);
    mock().expectNCalls(2, "showView").onObject(mainView.get());
    Module::ViewIdentity loginViewId = Module::ViewIdentity::LoginUser;
    mock().expectNCalls(1, "destroyInstance").onObject(&factory).withParameterOfType("ViewIdentity", "identity", &loginViewId);
	mock().expectOneCall("getPersonList").onObject(mainView.get());
	mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
	mock().expectNCalls(1, "add").onObject(&mainView->personBindList);
	loginPresenter->loginUser();
    mock().checkExpectations();
    mock().clear();

//	mock().expectOneCall("getPersonList").onObject(mainView.get());
//	mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
//	mock().expectNCalls(1, "add").onObject(&mainView->personBindList);
//	mock().expectOneCall("getPersonList").onObject(mainView.get());
//	mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
//	mock().expectNCalls(1, "add").onObject(&mainView->personBindList);
//	mock().expectOneCall("getPersonList").onObject(mainView.get());
//	mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
//	mock().expectNCalls(1, "add").onObject(&mainView->personBindList);
//	mock().expectOneCall("getPersonList").onObject(mainView.get());
//	mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
//	mock().expectNCalls(1, "add").onObject(&mainView->personBindList);
//
//    Module::Person newp(3, "Foo", "Doo");
//    model.loginUser("TEST", "pass");
//    auto n2 = model.addNewPerson(newp);
//    model.removePerson(n2);
//
//    Module::Person newp2(4, "Foo", "Doo");
//    auto n3 = model.addNewPerson(newp2);
//    model.removePerson(n3);

//    mock().checkExpectations();
//    mock().clear();


}


//
//-------------------------------------------------------
// PersonalModelConstruction test group
//-------------------------------------------------------
TEST_GROUP(PersonalModelConstruction)
{
    void setup() { }

};

TEST(PersonalModelConstruction, Constructor)
{
	Module::PersonalModel model;
	STRCMP_EQUAL("Module DBStorage 1.0 - September 6, 2016", model.getDBVersion().c_str());

}

TEST(PersonalModelConstruction, Deconstructor)
{
	Module::PersonalModel* pptr = new Module::PersonalModel();
	STRCMP_EQUAL("Module DBStorage 1.0 - September 6, 2016", pptr->getDBVersion().c_str());
	delete pptr;
}





