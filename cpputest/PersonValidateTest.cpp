
#include "module/Person.h"
#include "module/PersonValidate.h"
#include "module/Entity.h"
#include "module/validate/ValidationError.h"
#include <vector>
#include <cstring>
#include <memory>
#include <stdexcept>

//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

//
//-------------------------------------------------------
// Data test group
//-------------------------------------------------------
TEST_GROUP(PersonValidateConstruction)
{
    // Long surname
    Module::Person per1{1, "BLAKSKSKKSKSKDJJDJDJHKDHKHLDKHKDHKLDHLKDHKLDHLKHDKLJDHKLJDHKLHDLKJDHKLJDHKLJDHLKDHKLDHKLHD", "Jack"};

};

TEST(PersonValidateConstruction, Constructor)
{
	// Initialize state from Person validate after reading the validation rules
	Validate::PersonValidate vld1(per1);
    LONGS_EQUAL(4, vld1.getRules().size());// this will retrieve the rules
    LONGS_EQUAL(0, vld1.getValidationErrors().size()); // size of person errors is zero before validate function is called.

}

TEST(PersonValidateConstruction, CopyConstructor)
{
    Validate::PersonValidate vld1(per1);
    Validate::PersonValidate vld2(vld1);
    LONGS_EQUAL(4, vld1.getRules().size());
    LONGS_EQUAL(0, vld1.getValidationErrors().size());
    LONGS_EQUAL(4, vld2.getRules().size());
    LONGS_EQUAL(0, vld2.getValidationErrors().size());

}

TEST(PersonValidateConstruction, Assignment)
{
    Validate::PersonValidate vld1(per1);
    Validate::PersonValidate vld2 = vld1;
    LONGS_EQUAL(4, vld1.getRules().size());
    LONGS_EQUAL(0, vld1.getValidationErrors().size());
    LONGS_EQUAL(4, vld2.getRules().size());
    LONGS_EQUAL(0, vld2.getValidationErrors().size());

    vld2.setValidationErrors({Validate::ValidationError("name", "error")});
    vld1 = vld2;

}

TEST(PersonValidateConstruction, Deconstructor)
{
	// Initialize state from Person validate after reading the validation rules
	Validate::PersonValidate* pv = new Validate::PersonValidate(per1);
    LONGS_EQUAL(4, pv->getRules().size());// this will retrieve the rules
    LONGS_EQUAL(0, pv->getValidationErrors().size()); // size of person errors is zero before validate function is called.
    delete pv;

}

//
//-------------------------------------------------------
// Data test group
//-------------------------------------------------------
TEST_GROUP(PersonValidateAccessor)
{
    // Long surname
    Module::Person per1{1, "BLAKSKSKKSKSKDJJDJDJHKDHKHLDKHKDHKLDHLKDHKLDHLKHDKLJDHKLJDHKLHDLKJDHKLJDHKLJDHLKDHKLDHKLHD", "Jack"};
    Module::Person per2{2, "Black", "Jack"};
    Module::Person per3{3, "Black", "Jack example for a name that is too long for a sensible name "};

};

TEST(PersonValidateAccessor, ValidateAccessor)
{
    Validate::PersonValidate vld1(per1);
    LONGS_EQUAL(4, vld1.getRules().size());
    LONGS_EQUAL(0, vld1.getValidationErrors().size());

    std::vector<Validate::ValidationError> errors;

    errors = vld1.Validate();
	LONGS_EQUAL(1, errors.size());
	STRCMP_EQUAL("Surname", errors.at(0).getFieldName().c_str());
	STRCMP_EQUAL("Field is too long. maximum 40 characters.", errors.at(0).getErrorMessage().c_str());


	const char *xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
                      "<module-validation>\n"
                      "<object name=\"Module::Person\">\n"
                      "  <property-validations>\n"
                      "    <property nae=\"Firstname\">\n"
                      "      <rule required=\"yes\"/>\n"
                      "      <rule maxlength=\"40\"/>\n"
                      "    </property>\n"
                      "    <property name=\"Surname\">\n"
                      "      <rule required=\"yes\"/>\n"
                      "      <rule maxlength=\"40\"/>\n"
                      "    </property>\n"
                      "  </property-validations>\n"
                      "  <constraints>\n"
                      "    <constraint property=\"Firstname\"/>\n"
                      "  </constraints>\n"
                      "</object>\n"
                      "</module-validation>\n";

    Validate::PersonValidate vld2(per2, xml);
    LONGS_EQUAL(0, vld2.getValidationErrors().size());
    LONGS_EQUAL(4, vld2.getRules().size()); // Getting rules can give errors if xml config has an error
    LONGS_EQUAL(1, vld2.getValidationErrors().size());
    errors = vld2.getValidationErrors();
	STRCMP_EQUAL("Module::Person", errors.at(0).getFieldName().c_str());
	STRCMP_EQUAL(" Object: Module::Person - Property name attribute not found at line 5", errors.at(0).getErrorMessage().c_str());

    errors = vld2.Validate(); // can try to validate still

	LONGS_EQUAL(3, errors.size());
	STRCMP_EQUAL("Module::Person", errors.at(0).getFieldName().c_str());
	STRCMP_EQUAL(" Object: Module::Person - Property name attribute not found at line 5", errors.at(0).getErrorMessage().c_str());
	STRCMP_EQUAL("[missing]", errors.at(1).getFieldName().c_str());
	STRCMP_EQUAL("Required rule could not find that module property.", errors.at(1).getErrorMessage().c_str());
	STRCMP_EQUAL("[missing]", errors.at(2).getFieldName().c_str());
	STRCMP_EQUAL("Maxlength rule could not find that module property.", errors.at(2).getErrorMessage().c_str());


	const char *xml2 = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
	                      "<module-validation>\n"
	                      "<object name=\"Module::Person\">\n"
	                      "  <property-validations>\n"
	                      "    <property name=\"fistname\">\n"
	                      "      <rule required=\"yes\"/>\n"
	                      "      <rule maxlength=\"40\"/>\n"
	                      "    </property>\n"
	                      "      <rule required=\"yes\"/>\n"
	                      "      <rule maxlength=\"40\"/>\n"
	                      "    </property>\n"
	                      "  </property-validations>\n"
	                      "  <constraints>\n"
	                      "    <constraint property=\"Firstname\"/>\n"
	                      "  </constraints>\n"
	                      "</module-validation>\n";

	Validate::PersonValidate vld3(per2, xml2);
    LONGS_EQUAL(0, vld3.getValidationErrors().size());
    LONGS_EQUAL(4, vld3.getRules().size()); // Getting rules can give errors if xml config has an error
    LONGS_EQUAL(1, vld3.getValidationErrors().size());
    errors = vld3.getValidationErrors();
	STRCMP_EQUAL("Module::Person", errors.at(0).getFieldName().c_str());
	STRCMP_EQUAL(" mismatched tag at line 11", errors.at(0).getErrorMessage().c_str());

    errors = vld3.Validate(); // can try to validate still

	LONGS_EQUAL(5, errors.size());
	STRCMP_EQUAL("Module::Person", errors.at(0).getFieldName().c_str());
	STRCMP_EQUAL(" mismatched tag at line 11", errors.at(0).getErrorMessage().c_str());
	STRCMP_EQUAL("fistname", errors.at(1).getFieldName().c_str());
	STRCMP_EQUAL("Required rule could not find that module property.", errors.at(1).getErrorMessage().c_str());
	STRCMP_EQUAL("fistname", errors.at(2).getFieldName().c_str());
	STRCMP_EQUAL("Maxlength rule could not find that module property.", errors.at(2).getErrorMessage().c_str());
	STRCMP_EQUAL("fistname", errors.at(3).getFieldName().c_str());
	STRCMP_EQUAL("Required rule could not find that module property.", errors.at(3).getErrorMessage().c_str());
	STRCMP_EQUAL("fistname", errors.at(4).getFieldName().c_str());
	STRCMP_EQUAL("Maxlength rule could not find that module property.", errors.at(4).getErrorMessage().c_str());


	const char *xml3 = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
	                      "<module-validation>\n"
	                      "<object name=\"Module::Person\">\n"
	                      "  <property-validations>\n"
	                      "    <property name=\"firstname\">\n"
	                      "      <rule required=\"yes\"/>\n"
	                      "      <rule maxlength=\"40\"/>\n"
	                      "    </property>\n"
	                      "    <property name=\"Surname\">\n"
	                      "      <rule required=\"yes\"/>\n"
	                      "      <rule maxlength=\"40\"/>\n"
	                      "    </property>\n"
	                      "  </property-validations>\n"
	                      "  <constraints>\n"
	                      "    <constraint property=\"FirstName\"/>\n"
	                      "  </constraints>\n"
	                      "</object>\n"
	                      "</module-validation>\n";

	Validate::PersonValidate vld4(per2, xml3);
    LONGS_EQUAL(0, vld4.getValidationErrors().size());
    LONGS_EQUAL(4, vld4.getRules().size()); // Getting rules can give errors if xml config has an error
    LONGS_EQUAL(0, vld4.getValidationErrors().size());
    // No configuration xml errors but will have configuration problems in the rules.
    // These are found in Validate
    errors = vld4.Validate();

	LONGS_EQUAL(2, errors.size());
	STRCMP_EQUAL("firstname", errors.at(0).getFieldName().c_str());
	STRCMP_EQUAL("Required rule could not find that module property.", errors.at(0).getErrorMessage().c_str());
	STRCMP_EQUAL("firstname", errors.at(1).getFieldName().c_str());
	STRCMP_EQUAL("Maxlength rule could not find that module property.", errors.at(1).getErrorMessage().c_str());


	// Now check a person with a long first name with the same xml config as above which was a correct person object
	Validate::PersonValidate vld5(per3, xml3);
    LONGS_EQUAL(0, vld5.getValidationErrors().size());
    LONGS_EQUAL(4, vld5.getRules().size()); // Getting rules can give errors if xml config has an error
    LONGS_EQUAL(0, vld5.getValidationErrors().size());
    // No configuration xml errors but will have configuration problems in the rules.
    // These are found in Validate
    errors = vld5.Validate();
    // It still shows same errors but not what is expected for a wrong firstname, the config needs correcting.
	LONGS_EQUAL(2, errors.size());
	STRCMP_EQUAL("firstname", errors.at(0).getFieldName().c_str());
	STRCMP_EQUAL("Required rule could not find that module property.", errors.at(0).getErrorMessage().c_str());
	STRCMP_EQUAL("firstname", errors.at(1).getFieldName().c_str());
	STRCMP_EQUAL("Maxlength rule could not find that module property.", errors.at(1).getErrorMessage().c_str());


	// It is possible to alter the messages - put the message in the xml config.
	// see <rule maxlength=\"40\">First name is too long</rule>
    const char *xml4 = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
	                      "<module-validation>\n"
	                      "<object name=\"Module::Person\">\n"
	                      "  <property-validations>\n"
	                      "    <property name=\"Firstname\">\n"
	                      "      <rule required=\"yes\"/>\n"
	                      "      <rule maxlength=\"40\">First name is too long</rule>\n"
	                      "    </property>\n"
	                      "    <property name=\"Surname\">\n"
	                      "      <rule required=\"yes\">Surname needs to be entered.</rule>\n"
	                      "      <rule maxlength=\"40\"/>\n"
	                      "    </property>\n"
	                      "  </property-validations>\n"
	                      "  <constraints>\n"
	                      "    <constraint property=\"Firstname\"/>\n"
	                      "  </constraints>\n"
	                      "</object>\n"
	                      "</module-validation>\n";
	Validate::PersonValidate vld6(per3, xml4);  // A long first name
    LONGS_EQUAL(0, vld6.getValidationErrors().size());
    LONGS_EQUAL(4, vld6.getRules().size()); // Getting rules can give errors if xml config has an error
    LONGS_EQUAL(0, vld6.getValidationErrors().size());
    // No configuration xml errors but will have configuration problems in the rules.
    // These are found in Validate
    errors = vld6.Validate();

	LONGS_EQUAL(1, errors.size());
	STRCMP_EQUAL("Firstname", errors.at(0).getFieldName().c_str());
	STRCMP_EQUAL("First name is too long", errors.at(0).getErrorMessage().c_str());

}

