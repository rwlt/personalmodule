
#include "MockMainView.h"

//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"
#include "CppUTestExt/MockSupport.h"

//
//-------------------------------------------------------
// Data test group
//-------------------------------------------------------
TEST_GROUP(PersonPresenterAccessor)
{
	MockControllerViewFactory factory;
    std::unique_ptr<Module::Controller> controller{std::unique_ptr<Module::Controller>(new Module::Controller(&factory))};
    std::unique_ptr<MockMainView> mainView{std::unique_ptr<MockMainView>(new MockMainView())};
    std::unique_ptr<MockPersonView> personView{std::unique_ptr<MockPersonView>(new MockPersonView())};
    std::unique_ptr<MockDBLoginView> loginView{std::unique_ptr<MockDBLoginView>(new MockDBLoginView())};
    Module::MainWindowInterface *pIMainView = mainView.get();
    Module::PersonWindowInterface *pIPersonView = personView.get();
    Module::DBLoginWindowInterface *pILoginView{loginView.get()};
    std::unique_ptr<Module::MainPresenter> mainPresenter{std::unique_ptr<Module::MainPresenter>(new Module::MainPresenter(mainView.get(), controller.get()))};
    std::unique_ptr<Module::PersonPresenter> presenter{std::unique_ptr<Module::PersonPresenter>(new Module::PersonPresenter(personView.get(), controller.get()))};
    std::unique_ptr<Module::DBLoginPresenter> loginPresenter{std::unique_ptr<Module::DBLoginPresenter>(new Module::DBLoginPresenter(loginView.get(), controller.get()))};
    PersonComparator comparatorPerson;
    ViewIdentityComparator comparatorViewIdentity;

    void setup() {
        mock().installComparator("Person", comparatorPerson);
        mock().installComparator("ViewIdentity", comparatorViewIdentity);

        Module::Person p;
        mock().expectOneCall("getStatus").onObject(mainView.get());
        mock().expectOneCall("getPersonList").onObject(mainView.get());
        mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
        mock().expectNCalls(1, "setItem").onObject(&mainView->statusItem).withStringParameter("item", "Ready");
        mock().expectOneCall("getStatus").onObject(personView.get());
        mock().expectNCalls(1, "getPerson").onObject(personView.get());
        mock().expectNCalls(1, "setItem").onObject(&personView->personItem).withParameterOfType("Person", "item", &p);
        mock().expectOneCall("getValidationError").onObject(personView.get());
        mock().expectOneCall("clear").onObject(&personView->validationList);
        mock().expectOneCall("setItem").onObject(&personView->statusItem).withStringParameter("item", "Ready");
        mainPresenter->initialize();
        loginPresenter->initialize();
        presenter->initialize();
        mock().checkExpectations();
    }
    void teardown() {
        mock().clear();
        mock().removeAllComparatorsAndCopiers();
    }

};


TEST(PersonPresenterAccessor, AddNewPerson)
{
	// Get the setup scenario done
    // Main presenter then mock to show the DBLogin View and that will cause the Username and password
    // fields to be setup. Also login role and is logged states
    // Then we can perform the PersonEdit presenter checks under that scenario.
    mock().expectOneCall("readEditControl").onObject(mainView.get());
    mock().expectNCalls(1, "createInstance").onObject(&factory);
    mock().expectNCalls(2, "loginInterface").onObject(&factory).andReturnValue(pILoginView);
    //The controller will call loginuser presenter loadMessage to set up the view
    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_username).withStringParameter("item", "");
    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_password).withStringParameter("item", "");
    mock().expectNCalls(1, "getRole").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_userrole);
    mock().expectNCalls(1, "getLoggedState").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_logged);
    mock().expectOneCall("getValidationError").onObject(loginView.get());
    mock().expectOneCall("clear").onObject(&loginView->m_validationList);
    mock().expectOneCall("showView").onObject(loginView.get());
    mainPresenter->loginUser();
    mock().checkExpectations();
    mock().clear();

    // Login a known user
    std::string username("TEST");
    std::string password("pass");
    Module::UserRole roletype = Module::UserRole::ADMIN;
    mock().expectOneCall("readEditControl").onObject(loginView.get());
    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_username).andReturnValue(&username);
    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_password).andReturnValue(&password);
    mock().expectNCalls(1, "getRole").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_userrole).andReturnValue(&roletype);
    // Validation happens on entered data so clear and prepare for validation errors.
    mock().expectOneCall("getValidationError").onObject(loginView.get());
    mock().expectOneCall("clear").onObject(&loginView->m_validationList);
    // Known user and then the ctrl get user login details and close login view
    mock().expectNCalls(1, "getLoggedState").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_logged);
    mock().expectNCalls(1, "loginInterface").onObject(&factory).andReturnValue(pILoginView);
    mock().expectOneCall("closeView").onObject(loginView.get());

    mock().expectNCalls(2, "mainInterface").onObject(&factory).andReturnValue(pIMainView);
    mock().expectNCalls(2, "showView").onObject(mainView.get());
    Module::ViewIdentity loginViewId = Module::ViewIdentity::LoginUser;
    mock().expectNCalls(1, "destroyInstance").onObject(&factory).withParameterOfType("ViewIdentity", "identity", &loginViewId);
	mock().expectOneCall("getPersonList").onObject(mainView.get());
	mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
	mock().expectNCalls(1, "add").onObject(&mainView->personBindList);
	loginPresenter->loginUser();
    mock().checkExpectations();
    mock().clear();
    //
    // Scenario of User Logged in setup
    //

    // Now need a person presenter to setup the new person to edit and add
    Module::Person newitem{};
    Module::Person p;

    mock().expectOneCall("getStatus").onObject(mainView.get());
    mock().expectNCalls(1, "setItem").onObject(&mainView->statusItem).withStringParameter("item", "");
    mock().expectOneCall("readEditControl").onObject(mainView.get());
    mock().expectNCalls(1, "createInstance").onObject(&factory);
    mock().expectNCalls(2, "personInterface").onObject(&factory).andReturnValue(pIPersonView);
    //The controller will call person presenter loadMessage to set up the view
    mock().expectNCalls(1, "getPerson").onObject(personView.get());
    mock().expectNCalls(1, "setItem").onObject(&personView->personItem).withParameterOfType("Person", "item", &newitem);
    mock().expectNCalls(1, "getState").onObject(personView.get());
    mock().expectNCalls(1, "setItem").onObject(&personView->viewEditState);
    mock().expectOneCall("getValidationError").onObject(personView.get());
    mock().expectOneCall("clear").onObject(&personView->validationList);
    mock().expectOneCall("getStatus").onObject(personView.get());
    mock().expectNCalls(1, "setItem").onObject(&personView->statusItem).withStringParameter("item", "Ready");
    mock().expectOneCall("showView").onObject(personView.get());
    mainPresenter->addNewPerson();
    mock().checkExpectations();
    mock().clear();

    // Add new person validate correct example
    Module::Person newp(0, "Davis", "Fred");
    mock().setDataObject("EditPerson", "Module::Person", &newp);
    Storage::Bind_iterator<Module::Person> foundIter(&p, 1);
    mock().setDataObject("FoundIter", "Storage::Bind_iterator<Module::Person>", &foundIter);
    Storage::Bind_iterator<Module::Person> foundIterEnd(&p, 1);
    mock().setDataObject("FoundIterEnd", "Storage::Bind_iterator<Module::Person>", &foundIterEnd);

    mock().expectOneCall("readEditControl").onObject(personView.get());
    mock().expectNCalls(1, "getPerson").onObject(personView.get());
    mock().expectOneCall("getItem").onObject(&personView->personItem);
    mock().expectOneCall("getValidationError").onObject(personView.get());
    mock().expectOneCall("clear").onObject(&personView->validationList);
    // Test is valid.
    mock().expectNCalls(1, "getPerson").onObject(personView.get());
    mock().expectNCalls(1, "setItem").onObject(&personView->personItem).withParameterOfType("Person", "item", &newp);
    mock().expectOneCall("getStatus").onObject(personView.get());
    mock().expectOneCall("setItem").onObject(&personView->statusItem).withStringParameter("item", "Added Person");
    mock().expectNCalls(1, "personInterface").onObject(&factory).andReturnValue(pIPersonView);
    mock().expectOneCall("closeView").onObject(personView.get());

    mock().expectNCalls(1, "mainInterface").onObject(&factory).andReturnValue(pIMainView);
    mock().expectNCalls(1, "showView").onObject(mainView.get());
	mock().expectOneCall("getPersonList").onObject(mainView.get());
	mock().expectNCalls(1, "findById").onObject(&mainView->personBindList);
	mock().expectNCalls(1, "end").onObject(&mainView->personBindList);
	mock().expectNCalls(1, "add").onObject(&mainView->personBindList);
    loginViewId = Module::ViewIdentity::PersonEdit;
    mock().expectNCalls(1, "destroyInstance").onObject(&factory).withParameterOfType("ViewIdentity", "identity", &loginViewId);
    presenter->addNewPerson();
    mock().checkExpectations();
    mock().clear();

    // This will undo the add new person just made
    Storage::Bind_iterator<Module::Person> foundIter2(&newp, 1);
    Storage::Bind_iterator<Module::Person> foundIterEnd2(&p, 2);
    mock().setDataObject("FoundIter", "Storage::Bind_iterator<Module::Person>", &foundIter2);
    mock().setDataObject("FoundIterEnd", "Storage::Bind_iterator<Module::Person>", &foundIterEnd2);

    mock().expectOneCall("getStatus").onObject(mainView.get());
    mock().expectNCalls(1, "setItem").onObject(&mainView->statusItem).withStringParameter("item", "Undo complete");
    mock().expectNCalls(1, "mainInterface").onObject(&factory).andReturnValue(pIMainView);
    mock().expectNCalls(1, "showView").onObject(mainView.get());
	mock().expectOneCall("getPersonList").onObject(mainView.get());
	mock().expectNCalls(1, "findById").onObject(&mainView->personBindList);
	mock().expectNCalls(1, "end").onObject(&mainView->personBindList);
	mock().expectNCalls(1, "remove").onObject(&mainView->personBindList);
    mainPresenter->undo();
    mock().checkExpectations();
    mock().clear();

    // Test a error with validation on add new person
    mock().expectOneCall("getStatus").onObject(mainView.get());
    mock().expectNCalls(1, "setItem").onObject(&mainView->statusItem).withStringParameter("item", "");
    mock().expectOneCall("readEditControl").onObject(mainView.get());
    mock().expectNCalls(1, "createInstance").onObject(&factory);
    mock().expectNCalls(2, "personInterface").onObject(&factory).andReturnValue(pIPersonView);
    //The controller will call person presenter loadMessage to set up the view
    mock().expectNCalls(1, "getPerson").onObject(personView.get());
    mock().expectNCalls(1, "setItem").onObject(&personView->personItem).withParameterOfType("Person", "item", &newitem);
    mock().expectNCalls(1, "getState").onObject(personView.get());
    mock().expectNCalls(1, "setItem").onObject(&personView->viewEditState);
    mock().expectOneCall("getValidationError").onObject(personView.get());
    mock().expectOneCall("clear").onObject(&personView->validationList);
    mock().expectOneCall("getStatus").onObject(personView.get());
    mock().expectNCalls(1, "setItem").onObject(&personView->statusItem).withStringParameter("item", "Ready");
    mock().expectOneCall("showView").onObject(personView.get());
    mainPresenter->addNewPerson();
    mock().checkExpectations();
    mock().clear();

    // Add new person with validate error example
    Module::Person newbadp(5, "DavisDavisDavisDavisDavisDavisDavisDavisDavisDavis", "Fred");
    mock().setDataObject("EditPerson", "Module::Person", &newbadp);

    mock().expectOneCall("readEditControl").onObject(personView.get());
    mock().expectNCalls(1, "getPerson").onObject(personView.get());
    mock().expectOneCall("getItem").onObject(&personView->personItem);
    mock().expectOneCall("getValidationError").onObject(personView.get());
    mock().expectOneCall("clear").onObject(&personView->validationList);
    // Test is not valid.
    mock().expectOneCall("getValidationError").onObject(personView.get());
    mock().expectOneCall("add").onObject(&personView->validationList);
    presenter->addNewPerson();
    mock().checkExpectations();

}


TEST(PersonPresenterAccessor, ChangePerson)
{
	// Get the setup scenario done
    // Main presenter then mock to show the DBLogin View and that will cause the Username and password
    // fields to be setup. Also login role and is logged states
    // Then we can perform the PersonEdit presenter checks under that scenario.
    mock().expectOneCall("readEditControl").onObject(mainView.get());
    mock().expectNCalls(1, "createInstance").onObject(&factory);
    mock().expectNCalls(2, "loginInterface").onObject(&factory).andReturnValue(pILoginView);
    //The controller will call loginuser presenter loadMessage to set up the view
    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_username).withStringParameter("item", "");
    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_password).withStringParameter("item", "");
    mock().expectNCalls(1, "getRole").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_userrole);
    mock().expectNCalls(1, "getLoggedState").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_logged);
    mock().expectOneCall("getValidationError").onObject(loginView.get());
    mock().expectOneCall("clear").onObject(&loginView->m_validationList);
    mock().expectOneCall("showView").onObject(loginView.get());
    mainPresenter->loginUser();
    //controller->loginUser(pIMainView, Module::ViewIdentity::MainWindow);
    mock().checkExpectations();
    mock().clear();

    // Login a known user
    std::string username("TEST");
    std::string password("pass");
    Module::UserRole roletype = Module::UserRole::ADMIN;
    mock().expectOneCall("readEditControl").onObject(loginView.get());
    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_username).andReturnValue(&username);
    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_password).andReturnValue(&password);
    mock().expectNCalls(1, "getRole").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_userrole).andReturnValue(&roletype);
    // Validation happens on entered data so clear and prepare for validation errors.
    mock().expectOneCall("getValidationError").onObject(loginView.get());
    mock().expectOneCall("clear").onObject(&loginView->m_validationList);
    // Known user and then the ctrl get user login details and close login view
    mock().expectNCalls(1, "getLoggedState").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_logged);
    mock().expectNCalls(1, "loginInterface").onObject(&factory).andReturnValue(pILoginView);
    mock().expectOneCall("closeView").onObject(loginView.get());

    mock().expectNCalls(2, "mainInterface").onObject(&factory).andReturnValue(pIMainView);
    mock().expectNCalls(2, "showView").onObject(mainView.get());
    Module::ViewIdentity loginViewId = Module::ViewIdentity::LoginUser;
    mock().expectNCalls(1, "destroyInstance").onObject(&factory).withParameterOfType("ViewIdentity", "identity", &loginViewId);
	mock().expectOneCall("getPersonList").onObject(mainView.get());
	mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
	mock().expectNCalls(1, "add").onObject(&mainView->personBindList);
	loginPresenter->loginUser();
    //controller->loginUser(pILoginView, Module::ViewIdentity::LoginUser);
    mock().checkExpectations();
    mock().clear();
    //
    // Scenario of User Logged in setup
    //

    // Now need a person presenter to setup the person to edit
    Module::Person newitem{157, "Flintstone", "Fred"};
    Module::Person p;
	mock().setDataObject("SelectedPerson", "Module::Person", &newitem);

    mock().expectOneCall("getStatus").onObject(mainView.get());
    mock().expectNCalls(1, "setItem").onObject(&mainView->statusItem).withStringParameter("item", "");
    mock().expectOneCall("readEditControl").onObject(mainView.get());
	mock().expectOneCall("getPersonList").onObject(mainView.get());
	mock().expectNCalls(1, "getSelectItem").onObject(&mainView->personBindList);
    mock().expectNCalls(1, "createInstance").onObject(&factory);
    mock().expectNCalls(2, "personInterface").onObject(&factory).andReturnValue(pIPersonView);
    //The controller will call person presenter loadMessage to set up the view
    mock().expectNCalls(1, "getPerson").onObject(personView.get());
    mock().expectNCalls(1, "setItem").onObject(&personView->personItem).withParameterOfType("Person", "item", &newitem);
    mock().expectNCalls(1, "getState").onObject(personView.get());
    mock().expectNCalls(1, "setItem").onObject(&personView->viewEditState);
    mock().expectOneCall("getValidationError").onObject(personView.get());
    mock().expectOneCall("clear").onObject(&personView->validationList);
    mock().expectOneCall("getStatus").onObject(personView.get());
    mock().expectNCalls(1, "setItem").onObject(&personView->statusItem).withStringParameter("item", "Ready");
    mock().expectOneCall("showView").onObject(personView.get());
    mainPresenter->changePerson();
    mock().checkExpectations();
    mock().clear();

    // Add new person validate correct example
    Module::Person editperson(157, "Flintstone", "Fred");
    Storage::Bind_iterator<Module::Person> foundIter(&editperson, 0);
    mock().setDataObject("EditPerson", "Module::Person", &editperson);
    mock().setDataObject("FoundIter", "Storage::Bind_iterator<Module::Person>", &foundIter);
    Storage::Bind_iterator<Module::Person> foundIterEnd(&p, 1);
    mock().setDataObject("FoundIterEnd", "Storage::Bind_iterator<Module::Person>", &foundIterEnd);

    mock().expectOneCall("readEditControl").onObject(personView.get());
    mock().expectNCalls(1, "getPerson").onObject(personView.get());
    mock().expectOneCall("getItem").onObject(&personView->personItem);
    mock().expectOneCall("getValidationError").onObject(personView.get());
    mock().expectOneCall("clear").onObject(&personView->validationList);
    // Test is valid.
    mock().expectOneCall("getStatus").onObject(personView.get());
    mock().expectOneCall("setItem").onObject(&personView->statusItem).withStringParameter("item", "Changed Person");
    mock().expectNCalls(1, "personInterface").onObject(&factory).andReturnValue(pIPersonView);
    mock().expectOneCall("closeView").onObject(personView.get());

    mock().expectNCalls(1, "mainInterface").onObject(&factory).andReturnValue(pIMainView);
    mock().expectNCalls(1, "showView").onObject(mainView.get());
	mock().expectOneCall("getPersonList").onObject(mainView.get());
	mock().expectNCalls(1, "findById").onObject(&mainView->personBindList);
	mock().expectNCalls(1, "end").onObject(&mainView->personBindList);
    Module::ViewIdentity personViewId = Module::ViewIdentity::PersonEdit;
    mock().expectNCalls(1, "destroyInstance").onObject(&factory).withParameterOfType("ViewIdentity", "identity", &personViewId);
    presenter->changePerson();
    mock().checkExpectations();
    mock().clear();

    // Test a error with validation on add new person
	mock().setDataObject("SelectedPerson", "Module::Person", &newitem);
    mock().setDataObject("EditPerson", "Module::Person", &editperson);
    mock().expectOneCall("getStatus").onObject(mainView.get());
    mock().expectNCalls(1, "setItem").onObject(&mainView->statusItem).withStringParameter("item", "");
    mock().expectOneCall("readEditControl").onObject(mainView.get());
	mock().expectOneCall("getPersonList").onObject(mainView.get());
	mock().expectNCalls(1, "getSelectItem").onObject(&mainView->personBindList);
    mock().expectNCalls(1, "createInstance").onObject(&factory);
    mock().expectNCalls(2, "personInterface").onObject(&factory).andReturnValue(pIPersonView);
    //The controller will call person presenter loadMessage to set up the view
    mock().expectNCalls(1, "getPerson").onObject(personView.get());
    mock().expectNCalls(1, "setItem").onObject(&personView->personItem).withParameterOfType("Person", "item", &editperson);
    mock().expectNCalls(1, "getState").onObject(personView.get());
    mock().expectNCalls(1, "setItem").onObject(&personView->viewEditState);
    mock().expectOneCall("getValidationError").onObject(personView.get());
    mock().expectOneCall("clear").onObject(&personView->validationList);
    mock().expectOneCall("getStatus").onObject(personView.get());
    mock().expectNCalls(1, "setItem").onObject(&personView->statusItem).withStringParameter("item", "Ready");
    mock().expectOneCall("showView").onObject(personView.get());
    mainPresenter->changePerson();
    mock().checkExpectations();
    mock().clear();

    // Add new person with validate error example
    Module::Person editperson2(157, "FlintstoneFlintstoneFlintstoneFlintstoneFlintstone", "Fred");
    mock().setDataObject("EditPerson", "Module::Person", &editperson2);

    mock().expectOneCall("readEditControl").onObject(personView.get());
    mock().expectNCalls(1, "getPerson").onObject(personView.get());
    mock().expectOneCall("getItem").onObject(&personView->personItem);
    mock().expectOneCall("getValidationError").onObject(personView.get());
    mock().expectOneCall("clear").onObject(&personView->validationList);
    // Test is not valid.
    mock().expectOneCall("getValidationError").onObject(personView.get());
    mock().expectOneCall("add").onObject(&personView->validationList);

    presenter->changePerson();
    mock().checkExpectations();

}

TEST(PersonPresenterAccessor, RemovePerson)
{
	// Get the setup scenario done
    // Main presenter then mock to show the DBLogin View and that will cause the Username and password
    // fields to be setup. Also login role and is logged states
    // Then we can perform the PersonEdit presenter checks under that scenario.
    mock().expectOneCall("readEditControl").onObject(mainView.get());
    mock().expectNCalls(1, "createInstance").onObject(&factory);
    mock().expectNCalls(2, "loginInterface").onObject(&factory).andReturnValue(pILoginView);
    //The controller will call loginuser presenter loadMessage to set up the view
    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_username).withStringParameter("item", "");
    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_password).withStringParameter("item", "");
    mock().expectNCalls(1, "getRole").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_userrole);
    mock().expectNCalls(1, "getLoggedState").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_logged);
    mock().expectOneCall("getValidationError").onObject(loginView.get());
    mock().expectOneCall("clear").onObject(&loginView->m_validationList);
    mock().expectOneCall("showView").onObject(loginView.get());
    mainPresenter->loginUser();
    mock().checkExpectations();
    mock().clear();

    // Login a known user
    std::string username("TEST");
    std::string password("pass");
    Module::UserRole roletype = Module::UserRole::ADMIN;
    mock().expectOneCall("readEditControl").onObject(loginView.get());
    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_username).andReturnValue(&username);
    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_password).andReturnValue(&password);
    mock().expectNCalls(1, "getRole").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_userrole).andReturnValue(&roletype);
    // Validation happens on entered data so clear and prepare for validation errors.
    mock().expectOneCall("getValidationError").onObject(loginView.get());
    mock().expectOneCall("clear").onObject(&loginView->m_validationList);
    // Known user and then the ctrl get user login details and close login view
    mock().expectNCalls(1, "getLoggedState").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_logged);
    mock().expectNCalls(1, "loginInterface").onObject(&factory).andReturnValue(pILoginView);
    mock().expectOneCall("closeView").onObject(loginView.get());

    mock().expectNCalls(2, "mainInterface").onObject(&factory).andReturnValue(pIMainView);
    mock().expectNCalls(2, "showView").onObject(mainView.get());
    Module::ViewIdentity loginViewId = Module::ViewIdentity::LoginUser;
    mock().expectNCalls(1, "destroyInstance").onObject(&factory).withParameterOfType("ViewIdentity", "identity", &loginViewId);
	mock().expectOneCall("getPersonList").onObject(mainView.get());
	mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
	mock().expectNCalls(1, "add").onObject(&mainView->personBindList);
	loginPresenter->loginUser();
    mock().checkExpectations();
    mock().clear();
    //
    // Scenario of User Logged in setup
    //

    // Now need a person presenter to setup the person to remove
    Module::Person removeitem{1, "Smithy", "Terry"};
    Module::Person p;
	mock().setDataObject("SelectedPerson", "Module::Person", &removeitem);

    mock().expectOneCall("getStatus").onObject(mainView.get());
    mock().expectNCalls(1, "setItem").onObject(&mainView->statusItem).withStringParameter("item", "");
    mock().expectOneCall("readEditControl").onObject(mainView.get());
	mock().expectOneCall("getPersonList").onObject(mainView.get());
	mock().expectNCalls(1, "getSelectItem").onObject(&mainView->personBindList);
    mock().expectNCalls(1, "createInstance").onObject(&factory);
    mock().expectNCalls(2, "personInterface").onObject(&factory).andReturnValue(pIPersonView);
    //The controller will call person presenter loadMessage to set up the view
    mock().expectNCalls(1, "getPerson").onObject(personView.get());
    mock().expectNCalls(1, "setItem").onObject(&personView->personItem).withParameterOfType("Person", "item", &removeitem);
    mock().expectNCalls(1, "getState").onObject(personView.get());
    mock().expectNCalls(1, "setItem").onObject(&personView->viewEditState);
    mock().expectOneCall("getValidationError").onObject(personView.get());
    mock().expectOneCall("clear").onObject(&personView->validationList);
    mock().expectOneCall("getStatus").onObject(personView.get());
    mock().expectNCalls(1, "setItem").onObject(&personView->statusItem).withStringParameter("item", "Ready");
    mock().expectOneCall("showView").onObject(personView.get());
    mainPresenter->changePerson();
    mock().checkExpectations();
    mock().clear();

    mock().setDataObject("EditPerson", "Module::Person", &removeitem);
    Storage::Bind_iterator<Module::Person> foundIter(&removeitem, 0);
    mock().setDataObject("FoundIter", "Storage::Bind_iterator<Module::Person>", &foundIter);
    Storage::Bind_iterator<Module::Person> foundIterEnd(&p, 1);
    mock().setDataObject("FoundIterEnd", "Storage::Bind_iterator<Module::Person>", &foundIterEnd);

    // Remove a person from the PersonEdit view
    mock().expectOneCall("getStatus").onObject(personView.get());
    mock().expectOneCall("setItem").onObject(&personView->statusItem).withStringParameter("item", "Removed Person");
    mock().expectNCalls(1, "getState").onObject(personView.get());
    mock().expectNCalls(1, "setItem").onObject(&personView->viewEditState);
    mock().expectNCalls(1, "personInterface").onObject(&factory).andReturnValue(pIPersonView);
    mock().expectOneCall("closeView").onObject(personView.get());

    mock().expectNCalls(1, "mainInterface").onObject(&factory).andReturnValue(pIMainView);
    mock().expectNCalls(1, "showView").onObject(mainView.get());
	mock().expectOneCall("getPersonList").onObject(mainView.get());
	mock().expectNCalls(1, "findById").onObject(&mainView->personBindList);
	mock().expectNCalls(1, "end").onObject(&mainView->personBindList);
	mock().expectNCalls(1, "remove").onObject(&mainView->personBindList);
    Module::ViewIdentity personViewId = Module::ViewIdentity::PersonEdit;
    mock().expectNCalls(1, "destroyInstance").onObject(&factory).withParameterOfType("ViewIdentity", "identity", &personViewId);
    presenter->removePerson();
    mock().checkExpectations();

}
