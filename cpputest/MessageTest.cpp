/*
 * MessageTest.cpp
 *
 *  Created on: 23/06/2017
 *      Author: rodney
 */


#include "module/Message.h"
#include <sstream>
#include <string>
#include <iostream>

//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

//
//-------------------------------------------------------
// MessageConstruction test group
//-------------------------------------------------------

TEST_GROUP(MessageConstruction) {
};

TEST(MessageConstruction, Constructor) {
	Module::Message m{};
    LONGS_EQUAL(0, m.kind()); // Default init is OneArg
    LONGS_EQUAL(0, m.argumments().oneArg.message); // O message
    LONGS_EQUAL(0, m.fieldArgumments().size()); // No field details value pairs.

}

TEST(MessageConstruction, CopyConstructor) {
	Module::Message m{};
    LONGS_EQUAL(0, m.kind()); // Default init is OneArg
    LONGS_EQUAL(0, m.argumments().oneArg.message); // O message
    LONGS_EQUAL(0, m.fieldArgumments().size()); // No field details value pairs.

	Module::Message m2(m);
    LONGS_EQUAL(0, m.kind()); // Default init is OneArg
    LONGS_EQUAL(0, m.argumments().oneArg.message); // O message
    LONGS_EQUAL(0, m.fieldArgumments().size()); // No field details value pairs.

	Module::Message m3(Module::Message{});
    LONGS_EQUAL(0, m.kind()); // Default init is OneArg
    LONGS_EQUAL(0, m.argumments().oneArg.message); // O message
    LONGS_EQUAL(0, m.fieldArgumments().size()); // No field details value pairs.

}

TEST(MessageConstruction, AssignmentConstructor) {
	Module::Message m{};
    LONGS_EQUAL(0, m.kind()); // Default init is OneArg
    LONGS_EQUAL(0, m.argumments().oneArg.message); // O message
    LONGS_EQUAL(0, m.fieldArgumments().size()); // No field details value pairs.

    Module::Message m2 = m;
    LONGS_EQUAL(0, m2.kind()); // Default init is OneArg
    LONGS_EQUAL(0, m2.argumments().oneArg.message); // O message
    LONGS_EQUAL(0, m2.fieldArgumments().size()); // No field details value pairs.

	Module::Message &m3 = m;
    LONGS_EQUAL(0, m3.kind()); // Default init is OneArg
    LONGS_EQUAL(0, m3.argumments().oneArg.message); // O message
    LONGS_EQUAL(0, m3.fieldArgumments().size()); // No field details value pairs.

	Module::Parameter paramArgs;
	paramArgs.kind = Module::ParameterKind::TWO_ARG;
	paramArgs.args.twoArgs.valueA = 1;
	paramArgs.args.twoArgs.valueB = 2;
    m2 = m3;
    m3.setParameter(paramArgs);
    LONGS_EQUAL(1, m.kind());
    LONGS_EQUAL(1, m.argumments().twoArgs.valueA); // O message
    LONGS_EQUAL(2, m.argumments().twoArgs.valueB); // O message
    LONGS_EQUAL(0, m.fieldArgumments().size()); // No field details value pairs.

    LONGS_EQUAL(0, m2.kind()); // Default init is OneArg
    LONGS_EQUAL(0, m2.argumments().oneArg.message); // O message
    LONGS_EQUAL(0, m2.fieldArgumments().size()); // No field details value pairs.

}


TEST(MessageConstruction, Deconstructor)
{
	Module::Message* mptr = new Module::Message{};
    LONGS_EQUAL(0, mptr->kind());
    LONGS_EQUAL(0, mptr->argumments().oneArg.message); // O message
    LONGS_EQUAL(0, mptr->fieldArgumments().size()); // No field details value pairs.
	delete mptr;
}

//
//-------------------------------------------------------
// MessageAccessor test group
//-------------------------------------------------------

TEST_GROUP(MessageAccessor) {
};


TEST(MessageAccessor, ArgumentsAccessor) {
	Module::Message m{};
    LONGS_EQUAL(0, m.argumments().oneArg.message); // Default OneArg message

	Module::Parameter paramArgs;
	paramArgs.args.twoArgs.valueA = 1;
	paramArgs.args.twoArgs.valueB = 2;
    m.setParameter(paramArgs);
    LONGS_EQUAL(1, m.argumments().twoArgs.valueA); // O message
    LONGS_EQUAL(2, m.argumments().twoArgs.valueB); // O message

//	paramArgs.args.threeArgs.valueA = 10;
//	paramArgs.args.threeArgs.valueB = 20;
//	paramArgs.args.threeArgs.valueC = 30;
//    m.setParameter(paramArgs);
//    LONGS_EQUAL(10, m.argumments().threeArgs.valueA); // O message
//    LONGS_EQUAL(20, m.argumments().threeArgs.valueB); // O message
//    LONGS_EQUAL(30, m.argumments().threeArgs.valueC); // O message

}

TEST(MessageAccessor, FieldArgumentsAccessor) {
	Module::Message m { };
	LONGS_EQUAL(0, m.fieldArgumments().size()); // Default empty fieldArguments

	Module::Parameter paramArgs;
	paramArgs.fieldArgs = { {"Sss1" , "Sss"}, {"Sss2" , "Sss"} };

	m.setParameter(paramArgs);
	LONGS_EQUAL(2, m.fieldArgumments().size());

	Module::FieldArgs args =  m.fieldArgumments();
	Storage::FieldDetail fd = args.at(0);
	STRCMP_EQUAL("Sss", fd.str_value.c_str());
	STRCMP_EQUAL("Sss1", fd.field_name.c_str());
	CHECK(true == fd.is_set);
}

TEST(MessageAccessor, KindAccessor) {
	Module::Message m { };
    LONGS_EQUAL(0, m.kind());

	Module::Parameter paramArgs;
	paramArgs.kind = Module::ParameterKind::FIELD_ARG;
	paramArgs.fieldArgs = { {"Sss1" , "Sss"}, {"Sss2" , "Sss"} };
	m.setParameter(paramArgs);
    LONGS_EQUAL(3, m.kind());
	LONGS_EQUAL(2, m.fieldArgumments().size());

}

