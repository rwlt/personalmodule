
#include "MockMainView.h"

//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"
#include "CppUTestExt/MockSupport.h"


//
//-------------------------------------------------------
// ControllerAccessor test group
//-------------------------------------------------------
TEST_GROUP(ControllerAccessor)
{
    std::unique_ptr<MockControllerViewFactory> viewFactory{std::unique_ptr<MockControllerViewFactory>(new MockControllerViewFactory())};
    Module::Controller controller{viewFactory.get()};
    std::unique_ptr<MockMainView> mainView{std::unique_ptr<MockMainView>(new MockMainView())};
    std::unique_ptr<MockPersonView> personView{std::unique_ptr<MockPersonView>(new MockPersonView())};
    ViewIdentityComparator comparatorViewIdentity;
    PersonComparator comparatorPerson;

    void setup() {
        mock().installComparator("Person", comparatorPerson);
        mock().installComparator("ViewIdentity", comparatorViewIdentity);
    }
    void teardown() {
        mock().clear();
        mock().removeAllComparatorsAndCopiers();
    }
    
};

TEST(ControllerAccessor, RunAccessor)
{
	Module::Message viewMessage; // Setup the module message for Views
	Module::Parameter paramArgs;
	paramArgs.kind = Module::ParameterKind::ONE_ARG;
	paramArgs.args.oneArg.message = 1;
	viewMessage.setParameter(paramArgs);
    Module::MainWindowInterface *pIMainView = mainView.get();

    // MainPresenter created here as the MainView would do this
    std::unique_ptr<Module::MainPresenter> mainPresenter;
    mainPresenter = std::unique_ptr<Module::MainPresenter>(new Module::MainPresenter(mainView.get(), &controller));

    mock().expectOneCall("getStatus").onObject(mainView.get());
    mock().expectOneCall("getPersonList").onObject(mainView.get());
    mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
    mock().expectNCalls(1, "setItem").onObject(&mainView->statusItem).withStringParameter("item", "Ready");
    mainPresenter->initialize();
    mock().checkExpectations();

    // Load
    mock().expectOneCall("createInstance").onObject(viewFactory.get());
    mock().expectOneCall("mainInterface").onObject(viewFactory.get()).andReturnValue(pIMainView);
    //controller.load(Module::ViewIdentity::MainWindow, Module::ViewIdentity::NullView);
    //mock().checkExpectations();

    // And show
    mock().expectNCalls(1, "mainInterface").onObject(viewFactory.get()).andReturnValue(pIMainView);
    // Will do the DB read to get Person list.
    //mock().expectOneCall("getPersonList").onObject(mainView.get());
    //mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
    // There is no logged user so no population of Person List
    mock().expectOneCall("showView").onObject(mainView.get());

    controller.run();
    mock().checkExpectations();

}

//TEST(ControllerAccessor, LoadAccessor)
//{
//    Module::MainWindowInterface *pIMainView = mainView.get();
//
//    // MainPresenter created here as the MainView would do this
//    std::unique_ptr<Module::MainPresenter> mainPresenter;
//    mainPresenter = std::unique_ptr<Module::MainPresenter>(new Module::MainPresenter(mainView.get(), &controller));
//
//    mock().expectOneCall("getStatus").onObject(mainView.get());
//    mock().expectOneCall("getPersonList").onObject(mainView.get());
//    mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
//    mock().expectNCalls(1, "setItem").onObject(&mainView->statusItem).withStringParameter("item", "Ready");
//    mainPresenter->initialize();
//    mock().checkExpectations();
//
//    mock().expectOneCall("createInstance").onObject(viewFactory.get());
//    mock().expectOneCall("mainInterface").onObject(viewFactory.get()).andReturnValue(pIMainView);
//    controller.load(Module::ViewIdentity::MainWindow, Module::ViewIdentity::NullView);
//    mock().checkExpectations();
//
//}

//TEST(ControllerAccessor, CloseAccessor)
//{
//    Module::MainWindowInterface *pIMainView = mainView.get();
//
//    // MainPresenter created here as the MainView would do this
//    std::unique_ptr<Module::MainPresenter> mainPresenter;
//    mainPresenter = std::unique_ptr<Module::MainPresenter>(new Module::MainPresenter(mainView.get(), &controller));
//
//    mock().expectOneCall("getStatus").onObject(mainView.get());
//    mock().expectOneCall("getPersonList").onObject(mainView.get());
//    mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
//    mock().expectNCalls(1, "setItem").onObject(&mainView->statusItem).withStringParameter("item", "Ready");
//    mainPresenter->initialize();
//    mock().checkExpectations();
//
//    mock().expectOneCall("createInstance").onObject(viewFactory.get());
//    mock().expectOneCall("mainInterface").onObject(viewFactory.get()).andReturnValue(pIMainView);
//    controller.load(Module::ViewIdentity::MainWindow, Module::ViewIdentity::NullView);
//    mock().checkExpectations();
//
//    mock().expectOneCall("mainInterface").onObject(viewFactory.get()).andReturnValue(pIMainView);
//    Module::ViewIdentity loginViewId = Module::ViewIdentity::MainWindow;
//    mock().expectNCalls(1, "destroyInstance").onObject(viewFactory.get()).withParameterOfType("ViewIdentity", "identity", &loginViewId);
//    controller.close(Module::ViewIdentity::MainWindow);
//    mock().checkExpectations();
//
//
//}

//TEST(ControllerAccessor, ShowWithMessageOneArgAccessor)
//{
//
//	Module::Message viewMessage; // Setup the module message for Views
//	Module::Parameter paramArgs;
//	paramArgs.kind = Module::ParameterKind::ONE_ARG;
//	paramArgs.args.oneArg.message = 1;
//	viewMessage.setParameter(paramArgs);
//    Module::MainWindowInterface *pIMainView = mainView.get();
//
//    // MainPresenter created here as the MainView would do this
//    std::unique_ptr<Module::MainPresenter> mainPresenter;
//    mainPresenter = std::unique_ptr<Module::MainPresenter>(new Module::MainPresenter(mainView.get(), &controller));
//
//    mock().expectOneCall("getStatus").onObject(mainView.get());
//    mock().expectOneCall("getPersonList").onObject(mainView.get());
//    mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
//    mock().expectNCalls(1, "setItem").onObject(&mainView->statusItem).withStringParameter("item", "Ready");
//    mainPresenter->initialize();
//    mock().checkExpectations();
//
//    mock().expectOneCall("mainInterface").onObject(viewFactory.get()).andReturnValue(pIMainView);
//    // Will do the DB read to get Person list.
//    mock().expectOneCall("getPersonList").onObject(mainView.get());
//    mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
//    // There is no logged user so no population of Person List
//    mock().expectOneCall("showView").onObject(mainView.get());
//
//    controller.show(Module::ViewIdentity::MainWindow, viewMessage);
//    mock().checkExpectations();
//
//}

//TEST(ControllerAccessor, ShowWithFieldArgAccessor)
//{
//	Module::Message m { };
//	Module::Parameter paramArgs;
//	paramArgs.kind = Module::ParameterKind::FIELD_ARG;
//	paramArgs.fieldArgs = {
//			{"ID" , 234},
//			{"Firstname" , "Field First name"},
//			{"Surname" , "Field Surname"} };
//	m.setParameter(paramArgs);
//
//	Module::Person p;
//	Module::Person cp{234, "Field Surname", "Field First name"};
//    Module::PersonWindowInterface *pIPersonView = personView.get();
//
//    // PersonPresenter created here as the PersonView would do this
//    std::unique_ptr<Module::PersonPresenter> personPresenter;
//    personPresenter = std::unique_ptr<Module::PersonPresenter>(new Module::PersonPresenter(personView.get(), &controller));
//
//    mock().expectOneCall("getStatus").onObject(personView.get());
//    mock().expectOneCall("getPerson").onObject(personView.get());
//    mock().expectNCalls(1, "setItem").onObject(&personView->personItem).withParameterOfType("Person", "item", &p);
//    mock().expectOneCall("getValidationError").onObject(personView.get());
//    mock().expectNCalls(1, "clear").onObject(&personView->validationList);
//    mock().expectNCalls(1, "setItem").onObject(&personView->statusItem).withStringParameter("item", "Ready");
//    personPresenter->initialize();
//    mock().checkExpectations();
//
//    mock().expectOneCall("personInterface").onObject(viewFactory.get()).andReturnValue(pIPersonView);
//
//    mock().expectOneCall("getPerson").onObject(personView.get());
//    mock().expectNCalls(1, "setItem").onObject(&personView->personItem).withParameterOfType("Person", "item", &cp);;
//    mock().expectOneCall("getState").onObject(personView.get());
//    mock().expectNCalls(1, "setItem").onObject(&personView->viewEditState);
//    mock().expectOneCall("getValidationError").onObject(personView.get());
//    mock().expectNCalls(1, "clear").onObject(&personView->validationList);
//    mock().expectOneCall("getStatus").onObject(personView.get());
//    mock().expectNCalls(1, "setItem").onObject(&personView->statusItem).withStringParameter("item", "Ready");
//    mock().expectOneCall("showView").onObject(personView.get());
//
//    controller.show(Module::ViewIdentity::PersonEdit, m);
//    mock().checkExpectations();
//
//}
