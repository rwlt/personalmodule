/*
 * CommandAddPersonTest.cpp
 *
 *  Created on: 19/06/2017
 *      Author: rodney
 */


#include "module/Command.h"
#include "module/CommandAddPerson.h"
#include <memory>
#include <exception>
//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

//
//-------------------------------------------------------
// CommandAddPersonAccessor test group
//-------------------------------------------------------
TEST_GROUP(CommandAddPersonAccessor)
{
    Module::PersonalModel model;
    std::string UserName{"TEST"};
    std::string UserPassword{"pass"};
    Module::Person p{0, "Add Surname", "Add First name"};
    void setup() {
    	model.loginUser(UserName, UserPassword);
    }

};

TEST(CommandAddPersonAccessor, RedoAccessor)
{
	// User TEST can add persons.
	CHECK(true == model.loggedUser());
	Module::CommandAddPerson cmd{&model, p};
	Module::Person addedPerson = cmd.getPerson();
	LONGS_EQUAL(0, addedPerson.getPersonID());
	STRCMP_EQUAL("Add First name", addedPerson.getFirstname().c_str());
	STRCMP_EQUAL("Add Surname", addedPerson.getSurname().c_str());

	try {
		cmd.redo();
	} catch (std::exception& e) {
		LONGS_EQUAL(0,1);
	}
	// CommandAddPerson can give us the newly created Person
	// So we can check if it got a generated id.
	// Should be new ID greater than 0.
	CHECK(0 < cmd.getPerson().getPersonID()); // AddItem will return ID generated if we set it zero and field is autogen
	STRCMP_EQUAL("Add First name", cmd.getPerson().getFirstname().c_str());
	STRCMP_EQUAL("Add Surname", cmd.getPerson().getSurname().c_str());

	// User can redo again - although this wont normally happen in real code
	// No change with the added person - all data kept same and an database error will occur
	// as an Insert will happen with a duplicate id.
	// CommandAddperson has kept the person created for its undo/redo processess.
//	int PersonID = cmd.getPerson().getPersonID();
//	try {
//		cmd.redo();
//		LONGS_EQUAL(0,1);
//	} catch (std::exception& e) {
//		STRCMP_EQUAL("Add item Person database exception and unable to read.", e.what());
//	}
//	// CommandAddPerson will have no change to Person
//	CHECK(PersonID == cmd.getPerson().getPersonID());
//	STRCMP_EQUAL("Add First name", cmd.getPerson().getFirstname().c_str());
//	STRCMP_EQUAL("Add Surname", cmd.getPerson().getSurname().c_str());
//
	try {
		cmd.undo();
	} catch (std::exception& e) {
		STRCMP_EQUAL("", e.what());
		LONGS_EQUAL(0,1);
	}


}

TEST(CommandAddPersonAccessor, UndoAccessor)
{
	// User TEST can add persons.
	CHECK(true == model.loggedUser());
	Module::CommandAddPerson cmd{&model, p};
	LONGS_EQUAL(0, p.getPersonID());
	try {
		cmd.redo();
	} catch (std::exception& e) {
		LONGS_EQUAL(0,1);
	}
	// CommandAddPerson can give us the newly created Person
	// So we can check if it got a generated id.
	// Should be new ID greater than 0.
	CHECK(0 < cmd.getPerson().getPersonID()); // AddItem will return ID generated if we set it zero and field is autogen
	STRCMP_EQUAL("Add First name", cmd.getPerson().getFirstname().c_str());
	STRCMP_EQUAL("Add Surname", cmd.getPerson().getSurname().c_str());
	int PersonID = cmd.getPerson().getPersonID();

	// This will remove added person from model with newly made ID
	try {
		cmd.undo();
	} catch (std::exception& e) {
		LONGS_EQUAL(0,1);
	}
	// CommandAddPerson will have no change to Person after removal, so it can redo same data ID and all
	CHECK(0 < cmd.getPerson().getPersonID()); // AddItem will return ID generated if we set it zero and field is autogen
	CHECK(PersonID == cmd.getPerson().getPersonID());
	STRCMP_EQUAL("Add First name", cmd.getPerson().getFirstname().c_str());
	STRCMP_EQUAL("Add Surname", cmd.getPerson().getSurname().c_str());

}

//
//-------------------------------------------------------
// CommandAddPersonConstruction test group
//-------------------------------------------------------
TEST_GROUP(CommandAddPersonConstruction)
{
    std::string UserName{"JOE"};
    std::string UserPassword{"password"};
    Module::PersonalModel model;
    Module::Person p{20, "Add Surname", "Add First name"};
    void setup() {
    	model.loginUser(UserName, UserPassword);
    }

};

TEST(CommandAddPersonConstruction, Constructor)
{
	CHECK(true == model.loggedUser());
	Module::CommandAddPerson cmd{&model, p};
	STRCMP_EQUAL("Add First name", p.getFirstname().c_str());
	CHECK(true == model.loggedUser());

}

TEST(CommandAddPersonConstruction, CopyConstructor)
{
	CHECK(true == model.loggedUser());
	Module::CommandAddPerson cmd{&model, p};
	CHECK(true == model.loggedUser());
	Module::CommandAddPerson cmd2(cmd);
	CHECK(true == model.loggedUser());

}

TEST(CommandAddPersonConstruction, AssignConstructor)
{
	CHECK(true == model.loggedUser());
	Module::CommandAddPerson cmd{&model, p};
	CHECK(true == model.loggedUser());
	Module::CommandAddPerson cmd2 = cmd;
	CHECK(true == model.loggedUser());

}

TEST(CommandAddPersonConstruction, Deconstructor)
{
	Module::CommandAddPerson* cmd = new Module::CommandAddPerson(&model, p);
	CHECK(cmd != 0);
	delete cmd;
}




