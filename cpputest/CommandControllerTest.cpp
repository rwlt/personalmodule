/*
 * CommandTest.cpp
 *
 *  Created on: 17/06/2017
 *      Author: rodney
 */

#include "module/Command.h"
#include "module/CommandChangePerson.h"
#include "module/CommandAddPerson.h"
#include "module/Entity.h"
#include <memory>
//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

//
//-------------------------------------------------------
// CommandControllerAccessor test group
//-------------------------------------------------------
TEST_GROUP(CommandControllerAccessor)
{
	Module::CommandController cdctrl;
	std::vector<Module::Person> source;
	Module::Person po1{20, "Surname2", "First2 Name"};
    void setup() {
    	source.push_back({10, "Surname1", "First1 Name"});
    	source.push_back({20, "Surname2", "First2 Name"});
    	source.push_back({30, "Surname3", "First3 Name"});
    	source.push_back({40, "Surname4", "First4 Name"});
    	source.push_back({50, "Surname5", "First5 Name"});
    	//source.setPosition(2);
    }

};

TEST(CommandControllerAccessor, ExecuteCmdAccessor)
{
	CHECK(cdctrl.changesActive() == false);
	//auto pos = source.getPosition();
    //auto po1 = source.getSelectItem();
    auto pc1 = po1;
    pc1.setFirstname("Changed First Name");
    //LONGS_EQUAL(2, pos);
    //LONGS_EQUAL(5, source.size());
    //cdctrl.executeCmd(std::make_shared<Module::CommandChangePerson>(&source, source.getPosition(), po1, pc1));
	//CHECK(cdctrl.changesActive() == true);
	//STRCMP_EQUAL("Changed First Name", source.getSelectItem().getFirstName().c_str());
}

TEST(CommandControllerAccessor, RedoAccessor)
{
	// No changes to redo the no active changes
//	CHECK(cdctrl.changesActive() == false);
//    cdctrl.redo();
//	CHECK(cdctrl.changesActive() == false);
//
//	auto pos = source.getPosition();
//    auto po1 = source.getSelectItem();
//    auto pc1 = po1;
//    pc1.setFirstname("Changed First Name");
//    LONGS_EQUAL(2, pos);
//    LONGS_EQUAL(5, source.size());
//    cdctrl.executeCmd(std::make_shared<Module::CommandChangePerson>(&source, source.getPosition(), po1, pc1));
//	CHECK(cdctrl.changesActive() == true);
//	STRCMP_EQUAL("Changed First Name", source.getSelectItem().getFirstName().c_str());
//
//    cdctrl.redo(); // Doing redo on same executeCmd is like redoing the cmd.
//	CHECK(cdctrl.changesActive() == true);
//	STRCMP_EQUAL("Changed First Name", source.getSelectItem().getFirstName().c_str());

}

TEST(CommandControllerAccessor, UndoAccessor)
{
	// No changes to redo the no active changes
//	CHECK(cdctrl.changesActive() == false);
//    cdctrl.undo();
//	CHECK(cdctrl.changesActive() == false);
//
//	auto pos = source.getPosition();
//    auto po1 = source.getSelectItem();
//    auto pc1 = po1;
//    pc1.setFirstname("Changed First Name");
//    LONGS_EQUAL(2, pos);
//    LONGS_EQUAL(5, source.size());
//    cdctrl.executeCmd(std::make_shared<Module::CommandChangePerson>(&source, source.getPosition(), po1, pc1));
//	CHECK(cdctrl.changesActive() == true);
//	STRCMP_EQUAL("Changed First Name", source.getSelectItem().getFirstName().c_str());
//
//    cdctrl.undo(); // Doing undo on same executeCmd is like undoing to what redo did.
//	CHECK(cdctrl.changesActive() == true);
//	STRCMP_EQUAL("First2 Name", source.getSelectItem().getFirstName().c_str());
//
//    cdctrl.undo(); // Doing undo on same executeCmd is like undoing to what redo did.
//	CHECK(cdctrl.changesActive() == true);
//	STRCMP_EQUAL("First2 Name", source.getSelectItem().getFirstName().c_str());
//
//    cdctrl.redo();
//	CHECK(cdctrl.changesActive() == true);
//	STRCMP_EQUAL("Changed First Name", source.getSelectItem().getFirstName().c_str());

}


TEST(CommandControllerAccessor, MultipleCommandRedoAccessor)
{
	// No changes to redo the no active changes
	CHECK(cdctrl.changesActive() == false);
    cdctrl.redo();
	CHECK(cdctrl.changesActive() == false);

//	auto pos = source.getPosition();
//    auto po1 = source.getSelectItem();
    auto pc1 = po1;
//    pc1.setFirstname("Changed First Name");
//    LONGS_EQUAL(2, pos);
//    LONGS_EQUAL(5, source.size());
//    cdctrl.executeCmd(std::make_shared<Module::CommandChangePerson>(&source, source.getPosition(), po1, pc1));
//	CHECK(cdctrl.changesActive() == true);
//	STRCMP_EQUAL("Changed First Name", source.getSelectItem().getFirstName().c_str());
//
//
//    po1 = pc1;
//    pc1.setFirstname("Changed First Name 2");
//    cdctrl.executeCmd(std::make_shared<Module::CommandChangePerson>(&source, source.getPosition(), po1, pc1));
//    po1 = pc1;
//    pc1.setFirstname("Changed First Name 3");
//    cdctrl.executeCmd(std::make_shared<Module::CommandChangePerson>(&source, source.getPosition(), po1, pc1));
//    po1 = pc1;
//    pc1.setFirstname("Changed First Name 4");
//    cdctrl.executeCmd(std::make_shared<Module::CommandChangePerson>(&source, source.getPosition(), po1, pc1));
//    po1 = pc1;
//    pc1.setFirstname("Changed First Name 5");
//    cdctrl.executeCmd(std::make_shared<Module::CommandChangePerson>(&source, source.getPosition(), po1, pc1));
//    po1 = pc1;
//    pc1.setFirstname("Changed First Name 6");
//    cdctrl.executeCmd(std::make_shared<Module::CommandChangePerson>(&source, source.getPosition(), po1, pc1));
//    po1 = pc1;
//    pc1.setFirstname("Changed First Name 7");
//    cdctrl.executeCmd(std::make_shared<Module::CommandChangePerson>(&source, source.getPosition(), po1, pc1));
//    po1 = pc1;
//    pc1.setFirstname("Changed First Name 9");
//    cdctrl.executeCmd(std::make_shared<Module::CommandChangePerson>(&source, source.getPosition(), po1, pc1));
//    po1 = pc1;
//    pc1.setFirstname("Changed First Name 8");
//    cdctrl.executeCmd(std::make_shared<Module::CommandChangePerson>(&source, source.getPosition(), po1, pc1));
//    po1 = pc1;
//    pc1.setFirstname("Changed First Name 10");
//    cdctrl.executeCmd(std::make_shared<Module::CommandChangePerson>(&source, source.getPosition(), po1, pc1));
//    po1 = pc1;
//    pc1.setFirstname("Changed First Name 11");
//    cdctrl.executeCmd(std::make_shared<Module::CommandChangePerson>(&source, source.getPosition(), po1, pc1));
//    po1 = pc1;
//    pc1.setFirstname("Changed First Name 12");
//    cdctrl.executeCmd(std::make_shared<Module::CommandChangePerson>(&source, source.getPosition(), po1, pc1));
//	CHECK(cdctrl.changesActive() == true);

    cdctrl = Module::CommandController(); // we erase previous cmds.
	CHECK(cdctrl.changesActive() == false);

	Storage::DBStorage dbStorage{"Module DBStorage 1.0 - September 6, 2016", "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
        "<module-dbstorage>\n"
        "  <database>\n"
        "   <file role=\"live\" driver=\"firebird\" name=\"/home/rodney/databases/personal.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
        "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
        "  </database>\n"
        "<table entity=\"Module::Person\" name=\"Person\" key=\"PersonID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"PersonID\" type=\"autogenint\"/>\n"
        "        <field name=\"Firstname\" type=\"string\"/>\n"
        "        <field name=\"Surname\" type=\"string\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
		"<table entity=\"Module::Chest\" name=\"Chest\" key=\"ChestID\">\n"
		"  <database-statements>\n"
		"      <statement type=\"select\">\n"
		"        <field name=\"ChestID\" type=\"integer\"/>\n"
		"        <field name=\"Name\" type=\"string\"/>\n"
		"        <field name=\"PersonID\" type=\"integer\"/>\n"
		"      </statement>\n"
		"  </database-statements>\n"
		"</table>\n"
        "</module-dbstorage>\n"};
	Module::PersonalModel model;
	model.loginUser("TEST", "pass");
	dbStorage.loginUser("TEST", "pass");
	pc1.setPersonID(0);
	pc1.setSurname("Added Surname");

	pc1.setFirstname("Added First Name 1");
	std::shared_ptr<Module::CommandAddPerson> shrCmd = std::make_shared<Module::CommandAddPerson>(&model, pc1);
	try {
		cdctrl.executeCmd(shrCmd);
	} catch (std::exception& e) {
		LONGS_EQUAL(0,1);
	}
	CHECK(shrCmd->getPerson().getPersonID() > 0); // New ID Found

	pc1.setFirstname("Added First Name 2");
	cdctrl.executeCmd(std::make_shared<Module::CommandAddPerson>(&model, pc1));
	pc1.setFirstname("Added First Name 3");
	cdctrl.executeCmd(std::make_shared<Module::CommandAddPerson>(&model, pc1));
	pc1.setFirstname("Added First Name 4");
	cdctrl.executeCmd(std::make_shared<Module::CommandAddPerson>(&model, pc1));
	pc1.setFirstname("Added First Name 5");
	cdctrl.executeCmd(std::make_shared<Module::CommandAddPerson>(&model, pc1));
	pc1.setFirstname("Added First Name 6");
	cdctrl.executeCmd(std::make_shared<Module::CommandAddPerson>(&model, pc1));
	pc1.setFirstname("Added First Name 7");
	cdctrl.executeCmd(std::make_shared<Module::CommandAddPerson>(&model, pc1));
	pc1.setFirstname("Added First Name 8");
	cdctrl.executeCmd(std::make_shared<Module::CommandAddPerson>(&model, pc1));
	pc1.setFirstname("Added First Name 9");
	cdctrl.executeCmd(std::make_shared<Module::CommandAddPerson>(&model, pc1));
	pc1.setFirstname("Added First Name 10");
	cdctrl.executeCmd(std::make_shared<Module::CommandAddPerson>(&model, pc1));
	pc1.setFirstname("Added First Name 11");
	cdctrl.executeCmd(std::make_shared<Module::CommandAddPerson>(&model, pc1));
	pc1.setFirstname("Added First Name 12");
	cdctrl.executeCmd(std::make_shared<Module::CommandAddPerson>(&model, pc1));
	CHECK(cdctrl.changesActive() == true);

    cdctrl = Module::CommandController(); // we erase previous cmds.
	CHECK(cdctrl.changesActive() == false);

    // Find the new person added by retrieve where criteria and remove them to cleanup for tests
    std::vector<Module::Person> result = dbStorage.doRead<Module::Person>({{"Surname", "Added Surname"}}, {});
    // Clean up the add items with doRemoveItem's
	for (Module::Person person: result) {
		dbStorage.doRemoveItem<Module::Person>(person);
	}


}

//
//-------------------------------------------------------
// CommandControllerConstruction test group
//-------------------------------------------------------
TEST_GROUP(CommandControllerConstruction)
{
    void setup() {

    }

};

TEST(CommandControllerConstruction, Constructor)
{
	Module::CommandController cdctrl;
	CHECK(cdctrl.changesActive() == false);

}

TEST(CommandControllerConstruction, CopyConstructor)
{
	Module::CommandController cdctrl;
	CHECK(cdctrl.changesActive() == false);
	Module::CommandController cdctrl2(cdctrl);
	CHECK(cdctrl2.changesActive() == false);

}

TEST(CommandControllerConstruction, AssignConstructor)
{
	Module::CommandController cdctrl;
	CHECK(cdctrl.changesActive() == false);
	Module::CommandController cdctrl2;
	cdctrl2 = cdctrl;
	CHECK(cdctrl2.changesActive() == false);

}

TEST(CommandControllerConstruction, Deconstructor)
{
	Module::CommandController* cdctrl = new Module::CommandController();
	CHECK(cdctrl->changesActive() == false);
	delete cdctrl;
}
