
#include "module/validate/ParseValidationRule.h"
#include <sstream>
#include <string>
#include <iostream>
//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"
//
//-------------------------------------------------------
// ParseValidationRuleConstruction test group
//-------------------------------------------------------

TEST_GROUP(ParseValidationRuleConstruction) {
	const char *xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
                      "<module-validation>\n"
                      "<object name=\"Module::Person\">\n"
                      "  <property-validations>\n"
                      "    <property name=\"Firstname\">\n"
                      "      <rule required=\"yes\"/>\n"
                      "      <rule maxlength=\"40\"/>\n"
                      "    </property>\n"
                      "    <property name=\"Surname\">\n"
                      "      <rule required=\"yes\"/>\n"
                      "      <rule maxlength=\"40\"/>\n"
                      "    </property>\n"
                      "  </property-validations>\n"
                      "  <constraints>\n"
                      "    <constraint property=\"Firstname\"/>\n"
                      "  </constraints>\n"
                      "</object>\n"
                      "</module-validation>\n";

};

TEST(ParseValidationRuleConstruction, ConstructorWithXml) {

    Validate::ParseValidationRule parseRule("Module::Person", xml);
    LONGS_EQUAL(0, parseRule.getRules().size());
    STRCMP_EQUAL("", parseRule.getXmlErrors().c_str());

}


//
//-------------------------------------------------------
// ParseValidationRuleAccessor test group
//-------------------------------------------------------

TEST_GROUP(ParseValidationRuleAccessor) {
	const char *xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
                      "<module-validation>\n"
                      "<object name=\"Module::Person\">\n"
                      "  <property-validations>\n"
                      "    <property name=\"Firstname\">\n"
                      "      <rule required=\"yes\"/>\n"
                      "      <rule maxlength=\"40\"/>\n"
                      "    </property>\n"
                      "    <property name=\"Surname\">\n"
                      "      <rule required=\"yes\"/>\n"
                      "      <rule maxlength=\"40\"/>\n"
                      "    </property>\n"
                      "  </property-validations>\n"
                      "  <constraints>\n"
                      "    <constraint property=\"Firstname\"/>\n"
                      "  </constraints>\n"
                      "</object>\n"
                      "</module-validation>\n";

};

TEST(ParseValidationRuleAccessor, ReadRulesAccessor) {

	Validate::ParseValidationRule parseRule("Module::Person", xml);
    STRCMP_EQUAL("", parseRule.getXmlErrors().c_str());
    LONGS_EQUAL(0, parseRule.getRules().size());

    parseRule.ReadRules();
    STRCMP_EQUAL("", parseRule.getXmlErrors().c_str());
    LONGS_EQUAL(4, parseRule.getRules().size());


    Validate::ParseValidationRule pr2("Module::Person", "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    		            "<modle-dbstorage>\n"
    		            "</module-dbstorage>\n");
    STRCMP_EQUAL("", pr2.getXmlErrors().c_str());
    LONGS_EQUAL(0, pr2.getRules().size());
    pr2.ReadRules();
    STRCMP_EQUAL(" mismatched tag at line 3", pr2.getXmlErrors().c_str());
    LONGS_EQUAL(0, pr2.getRules().size());


    Validate::ParseValidationRule pr3("Module::Person", "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    		            "<module-dbstorage>\n"
            "  <property-validations>\n"
            "    <property name=\"Firstname\">\n"
            "      <rule required=\"yes\"/>\n"
            "      <rule maxlength=\"40\"/>\n"
            "    </property>\n"
            "    <property name=\"Surname\">\n"
            "      <rule required=\"yes\"/>\n"
            "      <rule maxlength=\"40\"/>\n"
            "    </property>\n"
            "  </property-validations>\n"
		            "</module-dbstorage>\n");
    STRCMP_EQUAL("", pr3.getXmlErrors().c_str());
    LONGS_EQUAL(0, pr3.getRules().size());
    pr3.ReadRules();
    STRCMP_EQUAL("", pr3.getXmlErrors().c_str());
    LONGS_EQUAL(0, pr3.getRules().size());


    Validate::ParseValidationRule pr4("Module::Person", "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    		            "<module-dbstorage>\n"
            "<object name=\"Module::Person\">\n"
            "  <property-validations>\n"
            "    <property nme=\"Firstname\">\n"
            "      <rule required=\"yes\"/>\n"
            "      <rule maxlength=\"40\"/>\n"
            "    </property>\n"
            "    <property name=\"Surname\">\n"
            "      <rule required=\"yes\"/>\n"
            "      <rule axlength=\"40\"/>\n"
            "    </property>\n"
            "  </property-validations>\n"
            "  <constraints>\n"
            "    <constraint property=\"Firstname\"/>\n"
            "  </constraints>\n"
            "</object>\n"
		            "</module-dbstorage>\n");
    STRCMP_EQUAL("", pr4.getXmlErrors().c_str());
    LONGS_EQUAL(0, pr4.getRules().size());
    pr4.ReadRules();
    STRCMP_EQUAL(" Object: Module::Person - Property name attribute not found at line 5 Object: Module::Person - Rule attributes not recognized  at line 11", pr4.getXmlErrors().c_str());
    LONGS_EQUAL(4, pr4.getRules().size());


    Validate::ParseValidationRule pr5("Module::Person", "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    		            "<module-dbstorage>\n"
            "<object name=\"Mule::Person\">\n"
            "  <property-validations>\n"
            "    <property name=\"Firstname\">\n"
            "      <rule required=\"yes\"/>\n"
            "      <rule maxlength=\"40\"/>\n"
            "    </property>\n"
            "    <property name=\"Surname\">\n"
            "      <rule required=\"yes\"/>\n"
            "      <rule maxlength=\"40\"/>\n"
            "    </property>\n"
            "  </property-validations>\n"
            "  <constraints>\n"
            "    <constraint property=\"Firstname\"/>\n"
            "  </constraints>\n"
            "</object>\n"
		            "</module-dbstorage>\n");
    STRCMP_EQUAL("", pr5.getXmlErrors().c_str());
    LONGS_EQUAL(0, pr5.getRules().size());
    pr5.ReadRules();
    STRCMP_EQUAL("", pr5.getXmlErrors().c_str());
    LONGS_EQUAL(0, pr5.getRules().size());

}


TEST(ParseValidationRuleAccessor, GetXmlErrorsAccessor) {

	Validate::ParseValidationRule pr("Module::Person", "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    		            "<modle-dbstorage>\n"
    		            "</module-dbstorage>\n");
    STRCMP_EQUAL("", pr.getXmlErrors().c_str());
    pr.ReadRules();
    STRCMP_EQUAL(" mismatched tag at line 3", pr.getXmlErrors().c_str());

}


TEST(ParseValidationRuleAccessor, GetRulesAccessor) {


	Validate::ParseValidationRule pr("Module::Person", xml);
    STRCMP_EQUAL("", pr.getXmlErrors().c_str());
    LONGS_EQUAL(0, pr.getRules().size());
    pr.ReadRules();
    STRCMP_EQUAL("", pr.getXmlErrors().c_str());
    LONGS_EQUAL(4, pr.getRules().size());
    STRCMP_EQUAL("Firstname", pr.getRules().at(0).getPropertyName().c_str());
    LONGS_EQUAL(0, pr.getRules().at(0).getRuleType());
    STRCMP_EQUAL("yes", pr.getRules().at(0).getRuleValue().c_str());
    STRCMP_EQUAL("Firstname", pr.getRules().at(1).getPropertyName().c_str());
    LONGS_EQUAL(1, pr.getRules().at(1).getRuleType());
    STRCMP_EQUAL("40", pr.getRules().at(1).getRuleValue().c_str());
    STRCMP_EQUAL("Surname", pr.getRules().at(2).getPropertyName().c_str());
    LONGS_EQUAL(0, pr.getRules().at(2).getRuleType());
    STRCMP_EQUAL("yes", pr.getRules().at(2).getRuleValue().c_str());
    STRCMP_EQUAL("Surname", pr.getRules().at(3).getPropertyName().c_str());
    LONGS_EQUAL(1, pr.getRules().at(3).getRuleType());
    STRCMP_EQUAL("40", pr.getRules().at(3).getRuleValue().c_str());


    Validate::ParseValidationRule pr4("Module::Person", "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
    		            "<module-dbstorage>\n"
            "<object name=\"Module::Person\">\n"
            "  <property-validations>\n"
            "    <property nme=\"Firstname\">\n"
            "      <rule required=\"yes\"/>\n"
            "      <rule maxlength=\"40\"/>\n"
            "    </property>\n"
            "    <property name=\"Surname\">\n"
            "      <rule required=\"yes\"/>\n"
            "      <rule axlength=\"40\"/>\n"
            "    </property>\n"
            "  </property-validations>\n"
            "  <constraints>\n"
            "    <constraint property=\"Firstname\"/>\n"
            "  </constraints>\n"
            "</object>\n"
		            "</module-dbstorage>\n");
    STRCMP_EQUAL("", pr4.getXmlErrors().c_str());
    LONGS_EQUAL(0, pr4.getRules().size());
    pr4.ReadRules();
    STRCMP_EQUAL(" Object: Module::Person - Property name attribute not found at line 5 Object: Module::Person - Rule attributes not recognized  at line 11", pr4.getXmlErrors().c_str());
    LONGS_EQUAL(4, pr4.getRules().size());
    STRCMP_EQUAL("[missing]", pr4.getRules().at(0).getPropertyName().c_str());
    LONGS_EQUAL(0, pr4.getRules().at(0).getRuleType());
    STRCMP_EQUAL("yes", pr4.getRules().at(0).getRuleValue().c_str());
    STRCMP_EQUAL("[missing]", pr4.getRules().at(1).getPropertyName().c_str());
    LONGS_EQUAL(1, pr4.getRules().at(1).getRuleType());
    STRCMP_EQUAL("40", pr4.getRules().at(1).getRuleValue().c_str());
    STRCMP_EQUAL("Surname", pr4.getRules().at(2).getPropertyName().c_str());
    LONGS_EQUAL(0, pr4.getRules().at(2).getRuleType());
    STRCMP_EQUAL("yes", pr4.getRules().at(2).getRuleValue().c_str());
    STRCMP_EQUAL("Surname", pr4.getRules().at(3).getPropertyName().c_str());
    LONGS_EQUAL(0, pr4.getRules().at(3).getRuleType());
    STRCMP_EQUAL("yes", pr4.getRules().at(3).getRuleValue().c_str());

}


TEST(ParseValidationRuleAccessor, UseRulesAccessor) {


	Validate::ParseValidationRule pr("Module::Person", xml);
    STRCMP_EQUAL("", pr.getXmlErrors().c_str());
    LONGS_EQUAL(0, pr.getRules().size());
    pr.ReadRules();
    STRCMP_EQUAL("", pr.getXmlErrors().c_str());
    LONGS_EQUAL(4, pr.getRules().size());

    Validate::ValidationRule* r;
    if (!pr.useRules().empty()) {
    	r = &(pr.useRules().front());
        STRCMP_EQUAL("Firstname", r->getPropertyName().c_str());
        LONGS_EQUAL(0, r->getRuleType());
        STRCMP_EQUAL("yes", r->getRuleValue().c_str());
        pr.useRules().pop();
    }
    if (!pr.useRules().empty()) {
    	r = &(pr.useRules().front());
        STRCMP_EQUAL("Firstname", r->getPropertyName().c_str());
        LONGS_EQUAL(1, r->getRuleType());
        STRCMP_EQUAL("40", r->getRuleValue().c_str());
        pr.useRules().pop();
    }
    pr.useRules().pop();
    pr.useRules().pop();
    CHECK(pr.useRules().empty() == true );

}

//
//-------------------------------------------------------
// ValidationRuleConstruction test group
//-------------------------------------------------------

TEST_GROUP(ValidationRuleConstruction) {
};

TEST(ValidationRuleConstruction, Constructor) {
	Validate::ValidationRule dc{"Firstname", Validate::RuleType::Required, "yes", "Required field."};
    STRCMP_EQUAL("Firstname", dc.getPropertyName().c_str());
    LONGS_EQUAL(0, dc.getRuleType());
    STRCMP_EQUAL("yes", dc.getRuleValue().c_str());
    STRCMP_EQUAL("Required field.", dc.getMessage().c_str());

}

TEST(ValidationRuleConstruction, CopyConstructor) {
	Validate::ValidationRule vr{"Firstname", Validate::RuleType::Required, "yes", "Required field."};
    STRCMP_EQUAL("Firstname", vr.getPropertyName().c_str());
    LONGS_EQUAL(0, vr.getRuleType());
    STRCMP_EQUAL("yes", vr.getRuleValue().c_str());
    STRCMP_EQUAL("Required field.", vr.getMessage().c_str());

	Validate::ValidationRule vr2(vr);
    STRCMP_EQUAL("Firstname", vr2.getPropertyName().c_str());
    LONGS_EQUAL(0, vr2.getRuleType());
    STRCMP_EQUAL("yes", vr2.getRuleValue().c_str());
    STRCMP_EQUAL("Required field.", vr2.getMessage().c_str());

	Validate::ValidationRule vr3(Validate::ValidationRule("Firstname", Validate::RuleType::Required, "yes", "Required field."));
    STRCMP_EQUAL("Firstname", vr3.getPropertyName().c_str());
    LONGS_EQUAL(0, vr3.getRuleType());
    STRCMP_EQUAL("yes", vr3.getRuleValue().c_str());
    STRCMP_EQUAL("Required field.", vr3.getMessage().c_str());

}

TEST(ValidationRuleConstruction, AssignmentConstructor) {
	Validate::ValidationRule vr{"Firstname", Validate::RuleType::Required, "yes", "Required field."};
    STRCMP_EQUAL("Firstname", vr.getPropertyName().c_str());
    LONGS_EQUAL(0, vr.getRuleType());
    STRCMP_EQUAL("yes", vr.getRuleValue().c_str());
    STRCMP_EQUAL("Required field.", vr.getMessage().c_str());

    Validate::ValidationRule vr2 = vr;
    STRCMP_EQUAL("Firstname", vr2.getPropertyName().c_str());
    LONGS_EQUAL(0, vr2.getRuleType());
    STRCMP_EQUAL("yes", vr2.getRuleValue().c_str());
    STRCMP_EQUAL("Required field.", vr2.getMessage().c_str());

	Validate::ValidationRule &vr3 = vr;
    STRCMP_EQUAL("Firstname", vr3.getPropertyName().c_str());
    LONGS_EQUAL(0, vr3.getRuleType());
    STRCMP_EQUAL("yes", vr3.getRuleValue().c_str());
    STRCMP_EQUAL("Required field.", vr3.getMessage().c_str());

    vr3.setItemValue("New text");
    vr2 = vr3;
    STRCMP_EQUAL("New text", vr.getItemValue().c_str());
    STRCMP_EQUAL("New text", vr2.getItemValue().c_str());
    STRCMP_EQUAL("New text", vr3.getItemValue().c_str());

}

//
//-------------------------------------------------------
// ValidationRuleAccessor test group
//-------------------------------------------------------

TEST_GROUP(ValidationRuleAccessor) {
};


TEST(ValidationRuleAccessor, ItemValueAccessor) {
	Validate::ValidationRule vr{"Firstname", Validate::RuleType::Required, "yes", "Required field."};
    STRCMP_EQUAL("Firstname", vr.getPropertyName().c_str());
    LONGS_EQUAL(0, vr.getRuleType());
    STRCMP_EQUAL("yes", vr.getRuleValue().c_str());
    STRCMP_EQUAL("Required field.", vr.getMessage().c_str());

    vr.setItemValue("This is data to check with the rule.");
    STRCMP_EQUAL("This is data to check with the rule.", vr.getItemValue().c_str());
}


TEST(ValidationRuleAccessor, EqualOperator) {
	Validate::ValidationRule vr{"Firstname", Validate::RuleType::Required, "yes", "Required field."};
    STRCMP_EQUAL("Firstname", vr.getPropertyName().c_str());
    LONGS_EQUAL(0, vr.getRuleType());
    STRCMP_EQUAL("yes", vr.getRuleValue().c_str());
    STRCMP_EQUAL("Required field.", vr.getMessage().c_str());
    Validate::ValidationRule vr2{vr};
    CHECK(vr == vr2);
}

TEST(ValidationRuleAccessor, NotEqualOperator) {
	Validate::ValidationRule vr{"Firstname", Validate::RuleType::Required, "yes", "Required field."};
    STRCMP_EQUAL("Firstname", vr.getPropertyName().c_str());
    LONGS_EQUAL(0, vr.getRuleType());
    STRCMP_EQUAL("yes", vr.getRuleValue().c_str());
    STRCMP_EQUAL("Required field.", vr.getMessage().c_str());
    Validate::ValidationRule vr2{vr};
    Validate::ValidationRule vr3{"Surname", Validate::RuleType::Required, "yes", "Required field."};

    CHECK_EQUAL(false, vr != vr2); // these are equal
    CHECK(vr != vr3);

}
