/*
 * PersonTest.cpp
 *
 *  Created on: 17/06/2017
 *      Author: rodney
 */

#include "module/Person.h"
#include "module/Entity.h"

//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

//
//-------------------------------------------------------
// PersonAccessor test group
//-------------------------------------------------------
TEST_GROUP(PersonAccessor)
{
	Module::Person p{1,"Surname", "First name"};
    void setup() { }

};

TEST(PersonAccessor, IDAccessor)
{
	LONGS_EQUAL(1, p.getPersonID());
	p.setPersonID(2);
	LONGS_EQUAL(2, p.getPersonID());
}

TEST(PersonAccessor, FirstnameAccessor)
{
	STRCMP_EQUAL("First name", p.getFirstname().c_str());
	p.setFirstname("Changed");
	STRCMP_EQUAL("Changed", p.getFirstname().c_str());
}

TEST(PersonAccessor, SurnameAccessor)
{
	STRCMP_EQUAL("Surname", p.getSurname().c_str());
	p.setSurname("Changed");
	STRCMP_EQUAL("Changed", p.getSurname().c_str());
}

TEST(PersonAccessor, EqualOperator) {
    Module::Person p2{p};
    CHECK(p == p2);
}

TEST(PersonAccessor, NotEqualOperator) {
    Module::Person p2{p};
    Module::Person p3{1, "firebird", "local"};
    CHECK_EQUAL(false, p2 != p); // these are equal
    CHECK(p2 != p3);

}

//
//-------------------------------------------------------
// PersonEntityMapAccessor test group
//-------------------------------------------------------

TEST_GROUP(PersonEntityMapAccessor) {
    Storage::EntityMap<Module::Person> personEntityMap{};
    void setup() {

    }

};

TEST(PersonEntityMapAccessor, CreateEntityAccessor) {
	Module::Person p = personEntityMap.createEntity();
	LONGS_EQUAL(0, p.getPersonID());
	STRCMP_EQUAL("", p.getFirstname().c_str());
	STRCMP_EQUAL("", p.getSurname().c_str());

}

TEST(PersonEntityMapAccessor, GetFieldAccessor) {

	std::string resultValue{""};
	int resultIntValue{0};

	Module::Person p{};
	p.setPersonID(10);
	p.setFirstname("Warrior");
	p.setSurname("Surname");
	resultValue = "";
	resultIntValue = 0;
	personEntityMap.getField(p, "PersonID", resultIntValue);
	LONGS_EQUAL(10, resultIntValue);
	personEntityMap.getField(p, "Firstname", resultValue);
	STRCMP_EQUAL("Warrior", resultValue.c_str());
	personEntityMap.getField(p, "Surname", resultValue);
	STRCMP_EQUAL("Surname", resultValue.c_str());

	resultValue = "";
	resultIntValue = 0;
	personEntityMap.getField(p, "id", resultIntValue); // Wrong field name
	LONGS_EQUAL(-1, resultIntValue);
	personEntityMap.getField(p, "Site", resultValue); // Wrong field name
	STRCMP_EQUAL("EntityMap: Unknown field type found:- \"Site\".", resultValue.c_str());

	resultValue = "";
	resultIntValue = 0;
	personEntityMap.getField(p, "PersonID", resultValue); // Wrong result type
	STRCMP_EQUAL("EntityMap: Field is not STRING type:- \"PersonID\".", resultValue.c_str());
	personEntityMap.getField(p, "Statute", resultIntValue); // Wrong result type
	LONGS_EQUAL(-1, resultIntValue);

}

TEST(PersonEntityMapAccessor, SetFieldAccessor) {

	std::string value{"Test value1"};

	Module::Person p{1, "Test value1", "Test value2"};
	personEntityMap.setField(p, "PersonID", 10);
	LONGS_EQUAL(10, p.getPersonID());
	personEntityMap.setField(p, "Surname", "Knight");
	STRCMP_EQUAL("Knight", p.getSurname().c_str());

	try {
		personEntityMap.setField(p, "Site", "Knight");
	} catch (std::exception &e) {
		STRCMP_EQUAL("EntityMap setField (string): Field Site is mismatched for entity Person", e.what()); // Should be no chnage when unknown field used

	}
}

TEST(PersonEntityMapAccessor, GetMapFieldAccessor) {

	personEntityMap.getMapField("Module::Person");
	CHECK(personEntityMap.field_avaliable() == true );

	personEntityMap.getMapField("Module::person"); // Case sensitive is required
	CHECK(personEntityMap.field_avaliable() == false );


}

TEST(PersonEntityMapAccessor, FieldAvailableAndDoFrontAccessors) {

	std::string value{"Test value1"};
	std::string resultValue{""};

	personEntityMap.getMapField("Module::Person");
	CHECK(personEntityMap.field_avaliable() == true );
	Module::Person p{};
	p.setPersonID(10);
	p.setSurname("Warrior");

	CHECK(personEntityMap.field_avaliable() == true );
	Storage::FieldDetail fdh1 = personEntityMap.do_front();
	STRCMP_EQUAL("[PersonID INTEGER STR: INT:0 ]", fdh1.detailed().c_str());

	CHECK(personEntityMap.field_avaliable() == true );
	Storage::FieldDetail fdh2 = personEntityMap.do_front();
	STRCMP_EQUAL("[Firstname STRING STR: INT:0 ]", fdh2.detailed().c_str());

	CHECK(personEntityMap.field_avaliable() == true );
	Storage::FieldDetail fdh3 = personEntityMap.do_front();
	STRCMP_EQUAL("[Surname STRING STR: INT:0 ]", fdh3.detailed().c_str());

	CHECK(personEntityMap.field_avaliable() == true );
	Storage::FieldDetail fdh4 = personEntityMap.do_front();
	STRCMP_EQUAL("[ChestID INTEGER STR: INT:0 ]", fdh4.detailed().c_str());

	CHECK(personEntityMap.field_avaliable() == false );
	Storage::FieldDetail fdh5 = personEntityMap.do_front(); // trying to get unavailable next field
	STRCMP_EQUAL("[field unavailable STRING STR: INT:0 ]", fdh5.detailed().c_str());

}


//
//-------------------------------------------------------
// PersonConstruction test group
//-------------------------------------------------------
TEST_GROUP(PersonConstruction)
{
    void setup() { }

};

TEST(PersonConstruction, Constructor)
{
	Module::Person p{1,"Surname", "First name"};
	STRCMP_EQUAL("First name", p.getFirstname().c_str());

}

TEST(PersonConstruction, CopyConstructor)
{
	Module::Person p{1,"Surname", "First name"};
	STRCMP_EQUAL("First name", p.getFirstname().c_str());
	Module::Person p2(p);
	STRCMP_EQUAL("First name", p2.getFirstname().c_str());

}

TEST(PersonConstruction, AssignConstructor)
{
	Module::Person p{1,"Surname", "First name"};
	STRCMP_EQUAL("First name", p.getFirstname().c_str());
	Module::Person p2;
	p2 = p;
	STRCMP_EQUAL("First name", p2.getFirstname().c_str());

}

TEST(PersonConstruction, Deconstructor)
{
	Module::Person* pptr = new Module::Person();
	pptr->setFirstname("First name");
	STRCMP_EQUAL("First name", pptr->getFirstname().c_str());
	delete pptr;
}


