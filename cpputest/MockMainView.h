/*
 * MockMainView.h
 */

#ifndef D_MOCKMAINVIEW_H_
#define D_MOCKMAINVIEW_H_

#include "module/Controller.h"
#include "module/AbstractViewFactory.h"
#include "module/View.h"
#include "module/ViewList.h"
#include "module/ViewBindList.h"
#include "module/ViewItem.h"
#include "module/Person.h"
#include "module/Message.h"
#include "module/AbstractPresenter.h"
#include "module/MainWindowInterface.h"
#include "module/PersonWindowInterface.h"
#include "module/DBLoginWindowInterface.h"
#include "module/Person.h"
#include "module/MainPresenter.h"
#include "module/PersonPresenter.h"
#include "module/DBLoginPresenter.h"
#include <memory>
#include <cstring>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <memory>

#include "CppUTestExt/MockSupport.h"

//
//****************************************************************************
// View List of template
//****************************************************************************
//

template<class DataType>
class MockViewList: public Module::ViewList<DataType> {

	int m_pos{1};
public:
	MockViewList(std::vector<DataType> *container) :
			m_container(container) {
	}
	virtual ~MockViewList() {
	}
	virtual void add(DataType additem) {
		mock().actualCall("add").onObject(this);
		m_container->push_back(additem);
	}
	virtual void clear() {
		mock().actualCall("clear").onObject(this);
		m_container->clear();
	}
	virtual unsigned int size() {
		return (m_container->size());
	}
	virtual void setPosition(int value) {
		m_pos = value;
	}
	virtual int getPosition() {
		return (m_pos);
	}
	virtual DataType &getSelectItem() {
		return (m_container->at(m_pos-1));
	}
	// Define the iterator typedefs
	typedef typename std::vector<DataType>::iterator container_iterator;
	virtual container_iterator begin() {
		return m_container->begin();
	}
	;
	virtual container_iterator end() {
		return m_container->end();
	}
	;

protected:
	std::vector<DataType> *m_container;

};

//
//****************************************************************************
// View List of Person
//****************************************************************************
//

class MockBindViewList: public Module::ViewBindList<Module::Person, int> {
	Module::Person item[1];
	int index = 0;
	unsigned int position; ///< One-based positioning of current item for function getSelectItem

public:

	/// Constructor
	MockBindViewList() :
			item { Module::Person() }, index(0), position(1) {
	}
	;
	/// Deconstructor
	~MockBindViewList() {
	}
	;

	typedef Storage::Bind_iterator<Module::Person> iterator;
	virtual iterator begin() {
		return iterator(nullptr, 0);
	}
	;
	virtual iterator end() {
		mock().actualCall("end").onObject(this);
		iterator* iter = (iterator*) mock().getData("FoundIterEnd").getObjectPointer();
		return (*iter);
	}
	;
	unsigned int size() {
		return index;
	}
	;

	virtual void add(Module::Person) {
		++index;
		mock().actualCall("add").onObject(this);
	}
	virtual void remove(int){
		--index;
		mock().actualCall("remove").onObject(this);

	}
	virtual iterator findById(int) {
		mock().actualCall("findById").onObject(this);
		iterator* iter = (iterator*) mock().getData("FoundIter").getObjectPointer();
		return (*iter);
	}
	virtual void clear() {
		mock().actualCall("clear").onObject(this);
	}
	virtual void setPosition(int value) {
		if ((value < 1) || (value > index)) {
			return; //throw ( Control::BindSourceException ( "Position is out of bounds of container size." ) );
		}
		position = value;
	}
	virtual int getPosition() {
		return mock().actualCall("getPosition").onObject(this).returnIntValue();
	}
	virtual Module::Person getSelectItem() {
		mock().actualCall("getSelectItem").onObject(this);
		Module::Person* newp = (Module::Person*) mock().getData(
				"SelectedPerson").getObjectPointer();
		item[0] = *newp;
		return (item[0]);
	}
	int max_size();
	bool empty();

};

//
//****************************************************************************
// View Binded List as template
//****************************************************************************
//

template<class T>
class MockBindList: public Module::ViewBindList<T, int> {
	T item[1];
	int index = 0;
	unsigned int position; ///< One-based positioning of current item for function getSelectItem

public:

	/// Constructor
	MockBindList() :
			item { T() }, index(0), position(1) {
	}
	;
	/// Deconstructor
	virtual ~MockBindList() {
	}
	;
	typedef Storage::Bind_iterator<T> iterator;
	virtual iterator begin() {
		return iterator(nullptr, 0);
	}
	;
	virtual iterator end() {
		return iterator(nullptr, 0);
	}
	;

	virtual void add(T) {
		++index;
		mock().actualCall("add").onObject(this);
	}
	virtual void remove(int){

	}
	virtual iterator findById(int) {
		return iterator(nullptr, 0);
	}
	virtual void clear() {
		mock().actualCall("clear").onObject(this);
	}
	virtual void setPosition(int value) {
		if ((value < 1) || (value > index)) {
			return; //throw ( Control::BindSourceException ( "Position is out of bounds of container size." ) );
		}
		position = value;
	}
	virtual int getPosition() {
		return mock().actualCall("getPosition").onObject(this).returnIntValue();
	}
	virtual T getSelectItem() {
		mock().actualCall("getSelectedItem").onObject(this);
		T* newp = (T*) mock().getData("SelectedT").getObjectPointer();
		item[0] = *newp;
		return (item[0]);
	}
	unsigned int size() {
		return index;
	}
	;
	int max_size();
	bool empty();

};

//
//****************************************************************************
// View Item as template
//****************************************************************************
//
template<typename DataType>
class MockViewItem: public Module::ViewItem<DataType> {
	DataType m_item;
public:
	virtual ~MockViewItem() {};
	virtual void setItem(const DataType &item) {
		m_item = item;
		mock().actualCall("setItem").onObject(this);
	}
	virtual DataType &getItem() {
		mock().actualCall("getItem").onObject(this);
		return (m_item);
	}

};

//
//****************************************************************************
// ViewItem of UserRole
//****************************************************************************
//

template<>
class MockViewItem<Module::UserRole> : public Module::ViewItem<Module::UserRole> {
	Module::UserRole m_item;
public:
	virtual void setItem(const Module::UserRole &item) {
		m_item = item;
		mock().actualCall("setItem").onObject(this);
	}
	virtual Module::UserRole &getItem() {
		Module::UserRole * value = (Module::UserRole*) mock().actualCall(
				"getItem").onObject(this).returnPointerValue();
		return (*value);
	}

};

//
//****************************************************************************
// View Item of Person
//****************************************************************************
//

template<>
class MockViewItem<Module::Person> : public Module::ViewItem<Module::Person> {
	Module::Person m_item;
public:
	virtual void setItem(const Module::Person &item) {
		m_item = item;
		mock().actualCall("setItem").onObject(this).withParameterOfType(
				"Person", "item", &item);
	}
	virtual Module::Person &getItem() {
		mock().actualCall("getItem").onObject(this);
		Module::Person* newp =
				(Module::Person*) mock().getData("EditPerson").getObjectPointer();
		m_item = *newp;
		return m_item;
	}

};

//
//****************************************************************************
// ViewItem of String
//****************************************************************************
//

template<>
class MockViewItem<std::string> : public Module::ViewItem<std::string> {
	std::string m_item;
public:
	virtual void setItem(const std::string &item) {
		m_item = item;
		mock().actualCall("setItem").onObject(this).withStringParameter("item",
				item.c_str());
	}
	virtual std::string &getItem() {
		std::string * value =
				(std::string*) mock().actualCall("getItem").onObject(this).returnPointerValue();
		return (*value);
	}

};

//
//****************************************************************************
// Person Edit View
//****************************************************************************
//

class MockPersonView: public Module::PersonWindowInterface {
public:
	MockPersonView() :
			validationList(&validationSource) {
	}
	;

	Module::ViewItem<Module::Person> &getPerson() {
		mock().actualCall("getPerson").onObject(this);
		return personItem;
	}
	;
	Module::ViewList<Validate::ValidationError> &getValidationError() {
		mock().actualCall("getValidationError").onObject(this);
		return validationList;
	}
	;
	Module::ViewItem<std::string> &getStatus() {
		mock().actualCall("getStatus").onObject(this);
		return statusItem;
	}
	;
	virtual void readEditControl() {
		mock().actualCall("readEditControl").onObject(this);
		//personSource.doUpdate();
	}
	;
	virtual Module::ViewItem<Module::ViewEditState> &getState() {
		mock().actualCall("getState").onObject(this);
		return viewEditState;
	}
	;
	// View defintions
	void showView(const Module::Message &) {
		mock().actualCall("showView").onObject(this);
	}
	;
	void closeView() {
		mock().actualCall("closeView").onObject(this);
	}
	;
    void worker_notify(){
    }

	// public function
	int createView(std::unique_ptr<Module::PersonPresenter> prsntr) {
		m_presenter = std::move(prsntr);
		m_presenter->initialize();
		return (0);
	}
	;

	std::vector<Module::Person> personSource;
	std::vector<std::string> textSource;
	std::vector<Module::ViewEditState> viewEditStateSource;
	std::vector<Validate::ValidationError> validationSource;
	MockViewItem<Module::Person> personItem;
	MockViewList<Validate::ValidationError> validationList;
	MockViewItem<std::string> statusItem;
	MockViewItem<Module::ViewEditState> viewEditState;
	std::unique_ptr<Module::PersonPresenter> m_presenter;
};

//
//****************************************************************************
// Login View
//****************************************************************************
//

class MockDBLoginView: public Module::DBLoginWindowInterface {
	//: m_validationList ( &m_validationSource )
public:
	MockDBLoginView() {
	}
	;

	Module::ViewItem<std::string> &getUsername() {
		mock().actualCall("getUsername").onObject(this);
		return m_username;
	}
	;
	Module::ViewItem<std::string> &getPassword() {
		mock().actualCall("getPassword").onObject(this);
		return m_password;
	}
	;
	Module::ViewItem<Module::UserRole> &getRole() {
		mock().actualCall("getRole").onObject(this);
		return m_userrole;
	}
	;
	Module::ViewItem<Module::LoggedState> &getLoggedState() {
		mock().actualCall("getLoggedState").onObject(this);
		return m_logged;
	}
	;
	Module::ViewList<Validate::ValidationError> &getValidationError() {
		mock().actualCall("getValidationError").onObject(this);
		return m_validationList;
	}
	;
	virtual void readEditControl() {
		mock().actualCall("readEditControl").onObject(this);
	}
	;

	// View defintions
	void showView(const Module::Message &) {
		mock().actualCall("showView").onObject(this);
	}
	;
	void closeView() {
		mock().actualCall("closeView").onObject(this);
	}
	;
    void worker_notify(){
    }

	// public function
	int createView(std::unique_ptr<Module::DBLoginPresenter> prsntr) {
		m_presenter = std::move(prsntr);
		m_presenter->initialize();
		return (0);
	}
	;

	std::vector<Validate::ValidationError> m_validationSource;
	MockViewList<Validate::ValidationError> m_validationList {
			&m_validationSource };
	MockViewItem<std::string> m_username;
	MockViewItem<std::string> m_password;
	MockViewItem<Module::UserRole> m_userrole;
	MockViewItem<Module::LoggedState> m_logged;
	std::unique_ptr<Module::DBLoginPresenter> m_presenter;
};

//
//****************************************************************************
// MainWindow View
//****************************************************************************
//

class MockMainView: public Module::MainWindowInterface {
public:
	MockMainView() :
			Module::MainWindowInterface(false), personList(&personListSource) {
	}
	;

	// MainInterface
	Module::ViewBindList<Module::Person> &getPersonList() {
		mock().actualCall("getPersonList").onObject(this);
		return personBindList; // give presenter the container to fill and init components
	}
	;
	Module::ViewBindList<Module::Person> &getDBPersonList() {
		mock().actualCall("getDBPersonList").onObject(this);
		return personDBBindList; // give presenter the container to fill and init components
	}
	;
	Module::ViewItem<std::string> &getStatus() {
		mock().actualCall("getStatus").onObject(this);
		return statusItem;
	}
	;
	Module::ViewBindList<std::string> &getSystemList() {
		return listSystem;
	}
	;
	Module::ViewBindList<std::string> &getCategoryList() {
		return listCategory;
	}
	;
	virtual void readEditControl() {
		mock().actualCall("readEditControl").onObject(this);
	}
	;
	virtual void showError(std::string message) {
		std::cout << "MockMainView showError message is : " << message << "\n";
		mock().actualCall("showError").onObject(this);
	}
	// View defintions
	void showView(const Module::Message &) {
		mock().actualCall("showView").onObject(this);
	}
	void closeView() {
	}
    void worker_notify(){
    }

	// public function
	int createView(std::unique_ptr<Module::MainPresenter> prsntr) {
		m_presenter = std::move(prsntr);
		m_presenter->initialize();
		return (0);
	}
	;
	// These are public mock for unit test.
	std::unique_ptr<Module::MainPresenter> m_presenter;
	std::vector<Module::Person> personListSource;
	MockViewList<Module::Person> personList;
	MockBindViewList personBindList;
	MockBindViewList personDBBindList;
	MockBindList<std::string> listSystem;
	MockBindList<std::string> listCategory;
	MockViewItem<Module::Person> personItem;
	MockViewItem<std::string> statusItem;
};

//
//****************************************************************************
// Controller View Factory
//****************************************************************************
//

class MockControllerViewFactory: public Module::AbstractViewFactory {
public:
	MockControllerViewFactory() {
	}
	;
	virtual void createInstance(Module::Controller *, Module::ViewIdentity, Module::ViewIdentity) {
		mock().actualCall("createInstance").onObject(this);

	}
	;
	virtual void destroyInstance(Module::ViewIdentity viewIdentity) {
		//.withParameterOfType ( "Person", "item", &item )
		mock().actualCall("destroyInstance").onObject(this).withParameterOfType(
				"ViewIdentity", "identity", &viewIdentity);

	}
	;
	Module::MainWindowInterface *mainInterface() {
		return (Module::MainWindowInterface *) mock().actualCall(
				"mainInterface").onObject(this).returnPointerValue();
	}
	;
	Module::PersonWindowInterface *personInterface() {
		return (Module::PersonWindowInterface *) mock().actualCall(
				"personInterface").onObject(this).returnPointerValue();
	}
	;
	Module::DBLoginWindowInterface *loginInterface() {
		return (Module::DBLoginWindowInterface *) mock().actualCall(
				"loginInterface").onObject(this).returnPointerValue();
	}
	;
};

//
//****************************************************************************
// Persom comparator for Mock
//****************************************************************************
//
class PersonComparator: public MockNamedValueComparator {
public:
	virtual bool isEqual(const void *object1, const void *object2) {
		Module::Person *lhs = (Module::Person *) object1;
		Module::Person *rhs = (Module::Person *) object2;
		return (lhs->getSurname().compare(rhs->getSurname()) == 0 &&
				lhs->getFirstname().compare(rhs->getFirstname()) == 0);
	}
	virtual SimpleString valueToString(const void *object) {
		std::stringstream what;
		Module::Person *lhs = (Module::Person *) object;
//		what << lhs->getPersonID() << " " << lhs->getFirstname() << " "
//				<< lhs->getSurname();
		what << lhs->getFirstname() << " "	<< lhs->getSurname();
		return what.str().c_str(); // StringFrom(object);
	}
};

//
//****************************************************************************
// ViewIdentity comparator for Mock
//****************************************************************************
//
class ViewIdentityComparator: public MockNamedValueComparator {
public:
	virtual bool isEqual(const void *object1, const void *object2) {
		Module::ViewIdentity *lhs = (Module::ViewIdentity *) object1;
		Module::ViewIdentity *rhs = (Module::ViewIdentity *) object2;
		return (*lhs == *rhs);
	}
	virtual SimpleString valueToString(const void *object) {
		std::stringstream what;
		Module::ViewIdentity *lhs = (Module::ViewIdentity *) object;
		switch (*lhs) {
		case Module::ViewIdentity::LoginUser:
			what << "ViewLoginUser";
			break;
		case Module::ViewIdentity::MainWindow:
			what << "ViewMainWindow";
			break;
		case Module::ViewIdentity::PersonEdit:
			what << "ViewPersonEdit";
			break;
		default:
			what << "ViewOther";
			break;
		}
		return what.str().c_str();
	}
};

#endif /* D_MOCKMAINVIEW_H_ */
