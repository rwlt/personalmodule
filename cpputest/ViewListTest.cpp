/*
 * ViewListTest.cpp
 *
 *  Created on: 28/07/2017
 *      Author: rodney
 */

#include "module/ViewList.h"
#include "module/ViewBindList.h"
#include "module/Person.h"
#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>

//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

//
//****************************************************************************
// View List of Person
//****************************************************************************
//

class ViewListTest: public Module::ViewBindList<Module::Person, int> {
	Module::Person item[1];
	int index = 0;
	unsigned int position; ///< One-based positioning of current item for function getSelectItem
	std::vector<Module::Person>* m_list;

public:
	///@{
	typedef __PTRDIFF_TYPE__ difference_type;
	typedef const Module::Person & const_reference;
	typedef size_t size_type;
	typedef Module::Person value_type;
	typedef Module::Person & reference;
	typedef Module::Person * pointer;
	///@}

	typedef Storage::Bind_iterator<Module::Person> iterator;

	/// Constructor
	ViewListTest(std::vector<Module::Person>* list) :
			item { Module::Person() },
			index(0),
			position(1),
			m_list { list },
			m_bind { m_list } {
	}
	;
	/// Deconstructor
	~ViewListTest() {
	}
	;


	/// add
	/**
	 * \param item add Module::Person item to ViewList
	 */
	virtual void add(Module::Person item) {
		++index;
		m_bind.doBind(item);
	}

	/// update
	/**
	 * The item updated into the list view component view model
	 * \tparam KeyType
	 * \param id  value of id for DataType Module::Person in this case
	 * \return integer 1 based position in the container
	 */
	virtual void update(int id, const Module::Person& item) {
		int updateIndex = 0;
		for (Module::Person fitem : *this) {
			if (fitem.getPersonID() == id) {
				m_bind.doBindUpdate(item, updateIndex);
				return;
			}
			++updateIndex;
		}
	}

	/// remove
	/**
	 * The item removed from the list view component view model
	 * \tparam KeyType
	 * \param id  value of id for DataType Module::Person in this case
	 * \return integer 1 based position in the container
	 */
	void remove(int id) {
		int removeIndex = 0;
		for (Module::Person fitem : *this) {
			if (fitem.getPersonID() == id) {
				m_bind.doBindClear(fitem, removeIndex);
				--index;
				return;
			}
			++removeIndex;
		}
	}

	/// findById
	/**
	 * findById the Position (1 based position)
	 * \param id  value of id for DataType Module::Person in this case
	 * \return integer 1 based position in the container
	 */
	iterator findById(int id) {
		iterator iter;
		for (iter = begin(); iter != end(); ++iter) {
			if ((*iter).getPersonID() == id) {
				return iter;
			}
		}
		return end();
	}

	virtual void clear() {
	}
	virtual void setPosition(int value) {
		if ((value < 1) || (value > index)) {
			return; //throw ( Control::BindSourceException ( "Position is out of bounds of container size." ) );
		}
		position = value;
	}
	virtual int getPosition() {
		return position;
	}
	virtual Module::Person getSelectItem() {
		return (item[0]);
	}

	/// begin iterator
	virtual iterator begin() {
		return iterator(item, 0, &m_bind);
	}
	;
	/// end iterator
	virtual iterator end() {
		return iterator(item, index, &m_bind);
	}
	;

	unsigned int size() {
		return index;
	}
	;
	int max_size();
	bool empty();

	class PersonStoreBind: public Storage::Bind<Module::Person> {
	public:
		/// Constructor
		/**
		 * \param dest pointer to bind source
		 */
		PersonStoreBind(std::vector<Module::Person>* dest) :
				destList(dest) {
		}
		;
		virtual ~PersonStoreBind() {
		}
		;
		/// doBind - when an item is add or set with data
		/**
		 * \param item const reference to Storage::RowItem()
		 * \param notify const reference to Control::BindNotify()
		 */
		void doBind(const Module::Person &item) {
			destList->push_back(item); // We add the items as new when source binds to dest
		}
		;
		/// doBind - when an item is add or set with data
		/**
		 * \param item const reference to Storage::RowItem()
		 * \param notify const reference to Control::BindNotify()
		 */
		void doBindUpdate(const Module::Person &item, int index) {
			destList->at(index) = item; // We add the items as new when source binds to dest
		}
		;
		/// doBind - when an item is add or set with data
		/**
		 * \param item const reference to Storage::RowItem()
		 * \param notify const reference to Control::BindNotify()
		 */
		void doBindClear(const Module::Person &item, int) {
			//int int_to_remove = n;
			//vec.erase(std::remove(vec.begin(), vec.end(), int_to_remove), vec.end());
			std::cout << "Size: " << destList->size() << "\n";
			destList->erase(std::remove(destList->begin(), destList->end(), item), destList->end()); // We add the items as new when source binds to dest
			std::cout << "Size: " << destList->size() << "\n";
		}
		;
		/// doUpdate - update an item from dest to source
		/**
		 * \param item const reference to Storage::RowItem()
		 * \param notify const reference to Control::BindNotify()
		 */
		void doUpdate(Module::Person &item, int index) {
			std::cout << "doUpdate Size: " << destList->size() << "\n";
			item = destList->at(index);
		}
		;

		/// destination view list
		std::vector<Module::Person> *destList;

	};

	PersonStoreBind m_bind;

};

//
//-------------------------------------------------------
// ViewListAccessor Fixture test group
//-------------------------------------------------------
//
TEST_GROUP(ViewListAccessor) {
	void setup() {
	}

};

TEST(ViewListAccessor, FindIDAccessor) {
	std::stringstream name; // For test comparing result of iter's
	std::vector<Module::Person> list;
	std::vector<Module::Person> list2send;
	list2send.push_back(Module::Person { 16, "Smith", "Fred" });
	list2send.push_back(Module::Person { 12, "Fred", "Fred" });
	list2send.push_back(Module::Person { 6, "Green", "Fred" });
	list2send.push_back(Module::Person { 4, "Black", "Fred" });
	list2send.push_back(Module::Person { 17, "NewSmith", "NewFred" });

	ViewListTest viewListPerson(&list);
	viewListPerson.add(Module::Person { 16, "Smith", "Fred" });
	viewListPerson.add(Module::Person { 12, "Fred", "Fred" });
	viewListPerson.add(Module::Person { 6, "Green", "Fred" });
	viewListPerson.add(Module::Person { 4, "Black", "Fred" });
	LONGS_EQUAL(4, viewListPerson.size());

	Storage::Bind_iterator<Module::Person> founditer = viewListPerson.findById(12);
	STRCMP_EQUAL("Fred", (*founditer).getFirstname().c_str());
	STRCMP_EQUAL("Fred", (*founditer).getSurname().c_str());
	Module::Person newp(12, "Blogs", "Joe");
	founditer = newp;
	STRCMP_EQUAL("Joe", (*founditer).getFirstname().c_str());
	STRCMP_EQUAL("Blogs", (*founditer).getSurname().c_str());
	//viewListPerson.remove(12);

	// Overwrite with list2send
	for ( auto p : list2send) {
		founditer = viewListPerson.findById(p.getPersonID());
		if (founditer != viewListPerson.end()) {
			founditer = p;
		} else {
			viewListPerson.add(p);
		}
	}

	Module::ViewBindList<Module::Person> &viewList = viewListPerson;
	name.str(std::string());
	try {
	std::for_each(viewList.begin(), viewList.end(),
			[&name](const Module::Person & p) {
				name << p.getPersonID() << " " << p.getFirstname();
				if (p.getSurname() != "") {
					if (p.getFirstname() != "") {
						name << " ";
					}
					name << p.getSurname();
				}
				name << " ";
			});
		STRCMP_EQUAL("16 Fred Smith 12 Fred Fred 6 Fred Green 4 Fred Black 17 NewFred NewSmith ",
			name.str().c_str());
	} catch (std::exception &e) {
		std::cout << "what " << e.what() << "\n";
	}
}

TEST(ViewListAccessor, ListIteratorAccessor) {
	std::stringstream name; // For test comparing result of iter's
	std::vector<Module::Person> list;
	ViewListTest viewListPerson(&list);
	viewListPerson.add(Module::Person { 1, "Fred", "Fred" });
	viewListPerson.add(Module::Person { 2, "Fred", "Fred" });
	viewListPerson.add(Module::Person { 3, "Fred", "Fred" });
	viewListPerson.add(Module::Person { 4, "Fred", "Fred" });
	LONGS_EQUAL(4, viewListPerson.size());

	Module::ViewBindList<Module::Person> &basePersonList = viewListPerson;

	name.str(std::string());
	std::for_each(basePersonList.begin(), basePersonList.end(),
			[&name](const Module::Person & p) {
				name << p.getPersonID() << " " << p.getFirstname();
				if (p.getSurname() != "") {
					if (p.getFirstname() != "") {
						name << " ";
					}
					name << p.getSurname();
				}
				name << " ";
			});
	STRCMP_EQUAL("1 Fred Fred 2 Fred Fred 3 Fred Fred 4 Fred Fred ",
			name.str().c_str());
}
