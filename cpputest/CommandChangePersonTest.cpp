/*
 * CommandChangePersonTest.cpp
 *
 *  Created on: 17/06/2017
 *      Author: rodney
 */

/*
 * CommandTest.cpp
 *
 *  Created on: 17/06/2017
 *      Author: rodney
 */

#include "module/Command.h"
#include "module/CommandAddPerson.h"
#include "module/CommandChangePerson.h"
#include "module/Entity.h"
#include <memory>
//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

//
//-------------------------------------------------------
// CommandChangePersonAccessor test group
//-------------------------------------------------------
TEST_GROUP(CommandChangePersonAccessor)
{
    Storage::DBStorage dbStorage{"Module DBStorage 1.0 - September 6, 2016",
    	"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
        "<module-dbstorage>\n"
        "  <database>\n"
        "   <file role=\"live\" driver=\"firebird\" name=\"/home/rodney/databases/personal.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
        "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
        "  </database>\n"
        "<table entity=\"Module::Person\" name=\"Person\" key=\"PersonID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"PersonID\" type=\"autogenint\"/>\n"
        "        <field name=\"Firstname\" type=\"string\"/>\n"
        "        <field name=\"Surname\" type=\"string\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
		"<table entity=\"Module::Chest\" name=\"Chest\" key=\"ChestID\">\n"
		"  <database-statements>\n"
		"      <statement type=\"select\">\n"
		"        <field name=\"ChestID\" type=\"integer\"/>\n"
		"        <field name=\"Name\" type=\"string\"/>\n"
		"        <field name=\"PersonID\" type=\"integer\"/>\n"
		"      </statement>\n"
		"  </database-statements>\n"
		"</table>\n"
        "</module-dbstorage>\n"};
    Storage::DBStorage dbStorage2{"Module DBStorage 1.0 - September 6, 2016",
    	"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
        "<module-dbstorage>\n"
        "  <database>\n"
        "   <file role=\"live\" driver=\"firebird\" name=\"/home/rodney/databases/personal.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
        "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
        "  </database>\n"
        "<table entity=\"Module::Person\" name=\"Person\" key=\"PersonID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"PersonID\" type=\"autogenint\"/>\n"
        "        <field name=\"Firstname\" type=\"string\"/>\n"
        "        <field name=\"Surname\" type=\"string\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
		"<table entity=\"Module::Chest\" name=\"Chest\" key=\"ChestID\">\n"
		"  <database-statements>\n"
		"      <statement type=\"select\">\n"
		"        <field name=\"ChestID\" type=\"integer\"/>\n"
		"        <field name=\"Name\" type=\"string\"/>\n"
		"        <field name=\"PersonID\" type=\"integer\"/>\n"
		"      </statement>\n"
		"  </database-statements>\n"
		"</table>\n"
        "</module-dbstorage>\n"};
    std::string UserName{"TEST"};
    std::string UserPassword{"pass"};
    Module::PersonalModel model;
    Module::Person p{0, "Change Surname", "Change First name"};
    Module::Person p_changed{};
    void setup() {
    	model.loginUser(UserName, UserPassword);
    	dbStorage.loginUser(UserName, UserPassword);
    	dbStorage2.loginUser(UserName, UserPassword);
     }

};

TEST(CommandChangePersonAccessor, RedoAccessor)
{
   	Module::CommandAddPerson cmd{&model, p};
	cmd.redo();
  	Module::Person addedPerson = cmd.getPerson();
	p = cmd.getPerson();
	p_changed = p;
	CHECK(0 < p.getPersonID()); // AddItem will return ID generated if we set it zero and field is autogen
	STRCMP_EQUAL("Change First name", p.getFirstname().c_str());
	STRCMP_EQUAL("Change Surname", p.getSurname().c_str());

	p_changed.setFirstname("Updated item");

	CHECK(true == model.loggedUser());
	Module::CommandChangePerson cmd_change{&model, p, p_changed};
	try {
		cmd_change.redo();
	} catch (std::exception& e) {
		STRCMP_EQUAL("", e.what());
		LONGS_EQUAL(0,1);
	}

	// Find the person item by retrieve where criteria
    // Clean up the item with doRemoveItem
    auto result = dbStorage.doRead<Module::Person>({{"Surname", "Change Surname"}}, {});
    LONGS_EQUAL(1, result.size());
    STRCMP_EQUAL("Updated item", result.at(0).getFirstname().c_str());
    STRCMP_EQUAL("Change Surname", result.at(0).getSurname().c_str());

	try {
		cmd_change.undo();
	} catch (std::exception& e) {
		LONGS_EQUAL(0,1);
	}
    std::vector<Module::Person> result2 = dbStorage2.doRead<Module::Person>({{"Surname", "Change Surname"}}, {});
    LONGS_EQUAL(1, result2.size());
    //Updated item
    STRCMP_EQUAL("Change First name", result2.at(0).getFirstname().c_str());
    STRCMP_EQUAL("Change Surname", result2.at(0).getSurname().c_str());
	for (auto person: result2) {
		dbStorage2.doRemoveItem<Module::Person>(person);
	}

}


//
//-------------------------------------------------------
// CommandChangePersonConstruction test group
//-------------------------------------------------------
TEST_GROUP(CommandChangePersonConstruction)
{
    Storage::DBStorage dbStorage{"Module DBStorage 1.0 - September 6, 2016",
    	"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
        "<module-dbstorage>\n"
        "  <database>\n"
        "   <file role=\"live\" driver=\"firebird\" name=\"/home/rodney/databases/personal.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
        "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
        "  </database>\n"
        "<table entity=\"Module::Person\" name=\"Person\" key=\"PersonID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"PersonID\" type=\"autogenint\"/>\n"
        "        <field name=\"Firstname\" type=\"string\"/>\n"
        "        <field name=\"Surname\" type=\"string\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
		"<table entity=\"Module::Chest\" name=\"Chest\" key=\"ChestID\">\n"
		"  <database-statements>\n"
		"      <statement type=\"select\">\n"
		"        <field name=\"ChestID\" type=\"integer\"/>\n"
		"        <field name=\"Name\" type=\"string\"/>\n"
		"        <field name=\"PersonID\" type=\"integer\"/>\n"
		"      </statement>\n"
		"  </database-statements>\n"
		"</table>\n"
        "</module-dbstorage>\n"};
    std::string UserName{"TEST"};
    std::string UserPassword{"pass"};
    Module::Person p{0, "Change Surname", "Change First name"};
    Module::Person p_changed{0, "Change Surname", "Change First name"};
    Module::PersonalModel model;
    void setup() {
    	model.loginUser(UserName, UserPassword);
    	dbStorage.loginUser(UserName, UserPassword);
    }
};

TEST(CommandChangePersonConstruction, Constructor)
{

	Module::CommandChangePerson cmd{&model, p, p_changed};
	STRCMP_EQUAL("Change First name", p.getFirstname().c_str());

}

TEST(CommandChangePersonConstruction, CopyConstructor)
{
	Module::CommandChangePerson cmd{&model, p, p_changed};
	Module::CommandChangePerson cmd2(cmd);
	STRCMP_EQUAL("Change First name", p.getFirstname().c_str());

}

TEST(CommandChangePersonConstruction, AssignConstructor)
{
	Module::CommandChangePerson cmd{&model, p, p_changed};
	Module::CommandChangePerson cmd2 = cmd;
	STRCMP_EQUAL("Change First name", p.getFirstname().c_str());

}

TEST(CommandChangePersonConstruction, Deconstructor)
{
	Module::CommandChangePerson* cmd = new Module::CommandChangePerson(&model, p, p_changed);
	CHECK(cmd != 0);
	delete cmd;
}



