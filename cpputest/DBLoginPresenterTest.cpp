
#include "MockMainView.h"

//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"
#include "CppUTestExt/MockSupport.h"

//
//-------------------------------------------------------
// Data test group
//-------------------------------------------------------
//
TEST_GROUP(DBLoginPresenterAccessor)
{
    MockControllerViewFactory factory;
    std::unique_ptr<Module::Controller> controller{std::unique_ptr<Module::Controller>(new Module::Controller(&factory))};
    std::unique_ptr<MockMainView> mainView{std::unique_ptr<MockMainView>(new MockMainView())};
    std::unique_ptr<MockDBLoginView> loginView{std::unique_ptr<MockDBLoginView>(new MockDBLoginView())};
    std::unique_ptr<Module::MainPresenter> mainPresenter{std::unique_ptr<Module::MainPresenter>(new Module::MainPresenter(mainView.get(), controller.get()))};
    std::unique_ptr<Module::DBLoginPresenter> presenter{std::unique_ptr<Module::DBLoginPresenter>(new Module::DBLoginPresenter(loginView.get(), controller.get()))};
    PersonComparator comparatorPerson;
    ViewIdentityComparator comparatorViewIdentity;
    Module::DBLoginWindowInterface *pILoginView{loginView.get()};
    Module::MainWindowInterface *pIMainView{mainView.get()};

    /// Setup so DBLoginPresenter is running under mocked DBLogin view and controller
    // Mainpresenter setup is status and a person list cleared.
    void setup() {
        mock().installComparator("Person", comparatorPerson);
        mock().installComparator("ViewIdentity", comparatorViewIdentity);

        mock().expectOneCall("getStatus").onObject(mainView.get());
        mock().expectOneCall("getPersonList").onObject(mainView.get());
        mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
        mock().expectNCalls(1, "setItem").onObject(&mainView->statusItem).withStringParameter("item", "Ready");
        mainPresenter->initialize();
        presenter->initialize();
    }
    void teardown() {
        mock().clear();
        mock().removeAllComparatorsAndCopiers();
    }

};

TEST(DBLoginPresenterAccessor, LoginUser)
{
	// Get the setup scenario done
    // Main presenter then mock to show the DBLogin View and that will cause the Username and password
    // fields to be setup. Also login role and is logged states
    // Then we can perform the DB presenter checks under that scenario.
    mock().expectOneCall("readEditControl").onObject(mainView.get());
    mock().expectNCalls(1, "createInstance").onObject(&factory);
    mock().expectNCalls(2, "loginInterface").onObject(&factory).andReturnValue(pILoginView);
    //The controller will call loginuser presenter loadMessage to set up the view
    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_username).withStringParameter("item", "");
    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_password).withStringParameter("item", "");
    mock().expectNCalls(1, "getRole").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_userrole);
    mock().expectNCalls(1, "getLoggedState").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_logged);

    mock().expectOneCall("getValidationError").onObject(loginView.get());
    mock().expectOneCall("clear").onObject(&loginView->m_validationList);
    mock().expectOneCall("showView").onObject(loginView.get());
    mainPresenter->loginUser();

    mock().checkExpectations();


    // Login a known user
    std::string username("JOE");
    std::string password("password");
    Module::UserRole roletype = Module::UserRole::GUEST;

    mock().expectOneCall("readEditControl").onObject(loginView.get());
    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_username).andReturnValue(&username);
    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_password).andReturnValue(&password);
    mock().expectNCalls(1, "getRole").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_userrole).andReturnValue(&roletype);

    // Validation happens on entered data so clear and prepare for validation errors.
    mock().expectOneCall("getValidationError").onObject(loginView.get());
    mock().expectOneCall("clear").onObject(&loginView->m_validationList);
    // Known user and then the ctrl get user login details and close login view
    mock().expectNCalls(1, "getLoggedState").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_logged);
    mock().expectNCalls(1, "loginInterface").onObject(&factory).andReturnValue(pILoginView);
    mock().expectOneCall("closeView").onObject(loginView.get());

    mock().expectNCalls(2, "mainInterface").onObject(&factory).andReturnValue(pIMainView);
	mock().expectOneCall("getPersonList").onObject(mainView.get());
	mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
	mock().expectNCalls(1, "add").onObject(&mainView->personBindList);
    mock().expectNCalls(2, "showView").onObject(mainView.get());
    Module::ViewIdentity loginViewId = Module::ViewIdentity::LoginUser;
    mock().expectNCalls(1, "destroyInstance").onObject(&factory).withParameterOfType("ViewIdentity", "identity", &loginViewId);
    presenter->loginUser();
    mock().checkExpectations();

}

TEST(DBLoginPresenterAccessor, LogoutUser)
{
	// Get the setup scenario done
    // Main presenter then mock to show the DBLogin View and that will cause the Username and password
    // fields to be setup. Also login role and is logged states
    // Then we can perform the DB presenter checks under that scenario.
    mock().expectOneCall("readEditControl").onObject(mainView.get());
    mock().expectNCalls(1, "createInstance").onObject(&factory);
    mock().expectNCalls(2, "loginInterface").onObject(&factory).andReturnValue(pILoginView);
    //The controller will call loginuser presenter loadMessage to set up the view
    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_username).withStringParameter("item", "");
    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_password).withStringParameter("item", "");
    mock().expectNCalls(1, "getRole").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_userrole);

    mock().expectNCalls(1, "getLoggedState").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_logged);

    mock().expectOneCall("getValidationError").onObject(loginView.get());
    mock().expectOneCall("clear").onObject(&loginView->m_validationList);
    mock().expectOneCall("showView").onObject(loginView.get());
    mainPresenter->loginUser();
    mock().checkExpectations();


    // Login user
    std::string username("JOE");
    std::string password("password");
    Module::UserRole roletype = Module::UserRole::GUEST;

    mock().expectOneCall("readEditControl").onObject(loginView.get());
    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_username).andReturnValue(&username);
    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_password).andReturnValue(&password);
    mock().expectNCalls(1, "getRole").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_userrole).andReturnValue(&roletype);

    // Validation happens on entered data so clear and prepare for validation errors.
    mock().expectOneCall("getValidationError").onObject(loginView.get());
    mock().expectOneCall("clear").onObject(&loginView->m_validationList);
    // Known user and then the ctrl get user login details and close login view
    mock().expectNCalls(1, "getLoggedState").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_logged);
    mock().expectNCalls(1, "loginInterface").onObject(&factory).andReturnValue(pILoginView);
    mock().expectOneCall("closeView").onObject(loginView.get());

    mock().expectNCalls(2, "mainInterface").onObject(&factory).andReturnValue(pIMainView);
	mock().expectOneCall("getPersonList").onObject(mainView.get());
	mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
	mock().expectNCalls(1, "add").onObject(&mainView->personBindList);
    mock().expectNCalls(2, "showView").onObject(mainView.get());
    Module::ViewIdentity loginViewId = Module::ViewIdentity::LoginUser;
    mock().expectNCalls(1, "destroyInstance").onObject(&factory).withParameterOfType("ViewIdentity", "identity", &loginViewId);
    presenter->loginUser();
    mock().checkExpectations();


    //
    // Reshow a login view
    //
    // fields to be setup. Also login role and is logged states
    // Then we can perform the DB presenter checks under that scenario.
    mock().expectOneCall("readEditControl").onObject(mainView.get());
    mock().expectNCalls(1, "createInstance").onObject(&factory);
    mock().expectNCalls(2, "loginInterface").onObject(&factory).andReturnValue(pILoginView);
    //The controller will call loginuser presenter loadMessage to set up the view
    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_username).withStringParameter("item", "");
    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_password).withStringParameter("item", "");
    mock().expectNCalls(1, "getRole").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_userrole);

    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_username).withStringParameter("item", "JOE");
    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_password).withStringParameter("item", "password");
    mock().expectNCalls(1, "getLoggedState").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_logged);

    mock().expectOneCall("getValidationError").onObject(loginView.get());
    mock().expectOneCall("clear").onObject(&loginView->m_validationList);
    mock().expectOneCall("showView").onObject(loginView.get());
    mainPresenter->loginUser();
    mock().checkExpectations();
    mock().clear();

    mock().expectNCalls(1, "getLoggedState").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_logged);
    mock().expectNCalls(1, "loginInterface").onObject(&factory).andReturnValue(pILoginView);
    mock().expectOneCall("closeView").onObject(loginView.get());

    mock().expectNCalls(2, "mainInterface").onObject(&factory).andReturnValue(pIMainView);
    mock().expectNCalls(2, "showView").onObject(mainView.get());
	mock().expectOneCall("getPersonList").onObject(mainView.get());
	mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
    loginViewId = Module::ViewIdentity::LoginUser;
    mock().expectNCalls(1, "destroyInstance").onObject(&factory).withParameterOfType("ViewIdentity", "identity", &loginViewId);
    presenter->logoutUser();
    mock().checkExpectations();
    mock().clear();

    // Login an unknown user
//    username = "BRAIN";
//    password = "password";
//    roletype = Module::UserRole::GUEST;
//
//    mock().expectOneCall("readEditControl").onObject(loginView.get());
//    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
//    mock().expectOneCall("getItem").onObject(&loginView->m_username).andReturnValue(&username);
//    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
//    mock().expectOneCall("getItem").onObject(&loginView->m_password).andReturnValue(&password);
//    mock().expectNCalls(1, "getRole").onObject(loginView.get());
//    mock().expectOneCall("getItem").onObject(&loginView->m_userrole).andReturnValue(&roletype);
//
//    // Validation happens on entered data so clear and prepare for validation errors.
//    mock().expectOneCall("getValidationError").onObject(loginView.get());
//    mock().expectOneCall("clear").onObject(&loginView->m_validationList);
//    // An known user will add error to views validationerrors
//    mock().expectOneCall("getValidationError").onObject(loginView.get());
//    mock().expectOneCall("add").onObject(&loginView->m_validationList);
//
//    presenter->loginUser();
//    //controller->loginUser(pILoginView, Module::ViewIdentity::LoginUser);
//    mock().checkExpectations();

}
