/*
 * ValidationErrorTest.cpp
 *
 *  Created on: 18/06/2017
 *      Author: rodney
 */


#include "module/validate/ValidationError.h"
#include <sstream>
#include <string>
#include <iostream>
//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

//
//-------------------------------------------------------
// ValidationErrorConstruction test group
//-------------------------------------------------------

TEST_GROUP(ValidationErrorConstruction) {
};

TEST(ValidationErrorConstruction, Constructor) {
	Validate::ValidationError ve{"Firstname", "Required field."};
    STRCMP_EQUAL("Firstname", ve.getFieldName().c_str());
    STRCMP_EQUAL("Required field.", ve.getErrorMessage().c_str());
    LONGS_EQUAL(2, ve.getRuleType());

}

TEST(ValidationErrorConstruction, CopyConstructor) {
	Validate::ValidationError ve{"Firstname", "Required field."};
    STRCMP_EQUAL("Firstname", ve.getFieldName().c_str());
    STRCMP_EQUAL("Required field.", ve.getErrorMessage().c_str());
    LONGS_EQUAL(2, ve.getRuleType());

	Validate::ValidationError ve2(ve);
    STRCMP_EQUAL("Firstname", ve2.getFieldName().c_str());
    STRCMP_EQUAL("Required field.", ve2.getErrorMessage().c_str());
    LONGS_EQUAL(2, ve2.getRuleType());

	Validate::ValidationError ve3(Validate::ValidationError("Firstname", "Required field."));
    STRCMP_EQUAL("Firstname", ve3.getFieldName().c_str());
    STRCMP_EQUAL("Required field.", ve3.getErrorMessage().c_str());
    LONGS_EQUAL(2, ve3.getRuleType());

}

TEST(ValidationErrorConstruction, AssignmentConstructor) {
	Validate::ValidationError ve{"Firstname", "Required field."};
    STRCMP_EQUAL("Firstname", ve.getFieldName().c_str());
    STRCMP_EQUAL("Required field.", ve.getErrorMessage().c_str());
    LONGS_EQUAL(2, ve.getRuleType());

    Validate::ValidationError ve2 = ve;
    STRCMP_EQUAL("Firstname", ve2.getFieldName().c_str());
    STRCMP_EQUAL("Required field.", ve2.getErrorMessage().c_str());
    LONGS_EQUAL(2, ve2.getRuleType());

	Validate::ValidationError &ve3 = ve;
    STRCMP_EQUAL("Firstname", ve3.getFieldName().c_str());
    STRCMP_EQUAL("Required field.", ve3.getErrorMessage().c_str());
    LONGS_EQUAL(2, ve3.getRuleType());

    ve3.setFieldName("New text");
    ve2 = ve3;
    STRCMP_EQUAL("New text", ve.getFieldName().c_str());
    STRCMP_EQUAL("New text", ve2.getFieldName().c_str());
    STRCMP_EQUAL("New text", ve3.getFieldName().c_str());

}


TEST(ValidationErrorConstruction, Deconstructor)
{
	Validate::ValidationError * veptr = new Validate::ValidationError{"Firstname", "Required field."};
    STRCMP_EQUAL("Firstname", veptr->getFieldName().c_str());
    STRCMP_EQUAL("Required field.", veptr->getErrorMessage().c_str());
    LONGS_EQUAL(2, veptr->getRuleType());
	delete veptr;
}

//
//-------------------------------------------------------
// ValidationErrorAccessor test group
//-------------------------------------------------------

TEST_GROUP(ValidationErrorAccessor) {
};


TEST(ValidationErrorAccessor, FieldNameAccessor) {
	Validate::ValidationError ve{"Firstname", "Required field."};
    STRCMP_EQUAL("Firstname", ve.getFieldName().c_str());
    ve.setFieldName("This is data to check with the rule.");
    STRCMP_EQUAL("This is data to check with the rule.", ve.getFieldName().c_str());
}

TEST(ValidationErrorAccessor, UIFieldNameAccessor) {
	Validate::ValidationError ve{"Firstname", "Required field."};
    STRCMP_EQUAL("", ve.getUIFieldName().c_str());
    ve.setUIFieldName("This is data to check with the rule.");
    STRCMP_EQUAL("This is data to check with the rule.", ve.getUIFieldName().c_str());
}

TEST(ValidationErrorAccessor, ErrorTypeAccessor) {
	Validate::ValidationError ve{"Firstname", "Required field."};
    LONGS_EQUAL(2, ve.getRuleType());
    ve.setErrorType(Validate::ErrorType::Rule);
    LONGS_EQUAL(1, ve.getRuleType());
}

TEST(ValidationErrorAccessor, ErrorMessageAccessor) {
	Validate::ValidationError ve{"Firstname", "Required field."};
    STRCMP_EQUAL("Required field.", ve.getErrorMessage().c_str());
}

TEST(ValidationErrorAccessor, SortOrderAccessor) {
	Validate::ValidationError ve{"Firstname", "Required field."};
    LONGS_EQUAL(0, ve.getSortOrder());
    ve.setSortOrder(2);
    LONGS_EQUAL(2, ve.getSortOrder());
}

TEST(ValidationErrorAccessor, EqualOperator) {
	Validate::ValidationError ve{"Firstname", "Required field."};
    STRCMP_EQUAL("Firstname", ve.getFieldName().c_str());
    STRCMP_EQUAL("Required field.", ve.getErrorMessage().c_str());
    LONGS_EQUAL(2, ve.getRuleType());
    Validate::ValidationError ve2{ve};
    CHECK(ve == ve2);
}

TEST(ValidationErrorAccessor, NotEqualOperator) {
	Validate::ValidationError ve{"Firstname", "Required field."};
    STRCMP_EQUAL("Firstname", ve.getFieldName().c_str());
    STRCMP_EQUAL("Required field.", ve.getErrorMessage().c_str());
    LONGS_EQUAL(2, ve.getRuleType());
    Validate::ValidationError ve2{ve};
    Validate::ValidationError ve3{"Surname", "Required field."};

    CHECK_EQUAL(false, ve != ve2); // these are equal
    CHECK(ve != ve3);

}



