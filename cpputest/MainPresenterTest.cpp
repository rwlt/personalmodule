
#include "MockMainView.h"

//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"
#include "CppUTestExt/MockSupport.h"

//
//-------------------------------------------------------
// Data test group
//-------------------------------------------------------
TEST_GROUP(MainAccessor)
{
    MockControllerViewFactory factory;
    std::unique_ptr<Module::Controller> controller{std::unique_ptr<Module::Controller>(new Module::Controller(&factory))};
    std::unique_ptr<MockMainView> mainView{std::unique_ptr<MockMainView>(new MockMainView())};
    std::unique_ptr<MockDBLoginView> loginView{std::unique_ptr<MockDBLoginView>(new MockDBLoginView())};
    std::unique_ptr<Module::MainPresenter> presenter{std::unique_ptr<Module::MainPresenter>(new Module::MainPresenter(mainView.get(), controller.get()))};
    std::unique_ptr<Module::DBLoginPresenter> loginPresenter{std::unique_ptr<Module::DBLoginPresenter>(new Module::DBLoginPresenter(loginView.get(), controller.get()))};
    Module::MainWindowInterface *pIMainView = mainView.get();
    Module::DBLoginWindowInterface *pILoginView{loginView.get()};
    PersonComparator comparator;
    ViewIdentityComparator comparatorViewIdentity;
 
    void setup() {
        mock().installComparator("Person", comparator);
        mock().installComparator("ViewIdentity", comparatorViewIdentity);

        mock().expectOneCall("getStatus").onObject(mainView.get());
        mock().expectOneCall("getPersonList").onObject(mainView.get());
        mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
        mock().expectNCalls(1, "setItem").onObject(&mainView->statusItem).withStringParameter("item", "Ready");
        presenter->initialize();
        loginPresenter->initialize();
        mock().checkExpectations();
    }
    void teardown() {
        mock().clear();
        mock().removeAllComparatorsAndCopiers();
    }

};

TEST(MainAccessor, LoginUser)
{
	// Get the setup scenario done
    // Main presenter then mock to show the DBLogin View and that will cause the Username and password
    // fields to be setup. Also login role and is logged states
    // Then we can perform the PersonEdit presenter checks under that scenario.
    mock().expectOneCall("readEditControl").onObject(mainView.get());
    mock().expectNCalls(1, "createInstance").onObject(&factory);
    mock().expectNCalls(2, "loginInterface").onObject(&factory).andReturnValue(pILoginView);
    //The controller will call loginuser presenter loadMessage to set up the view
    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_username).withStringParameter("item", "");
    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_password).withStringParameter("item", "");
    mock().expectNCalls(1, "getRole").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_userrole);
    mock().expectNCalls(1, "getLoggedState").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_logged);
    mock().expectOneCall("getValidationError").onObject(loginView.get());
    mock().expectOneCall("clear").onObject(&loginView->m_validationList);
    mock().expectOneCall("showView").onObject(loginView.get());
    //controller->loginUser(pIMainView, Module::ViewIdentity::MainWindow);
    presenter->loginUser();
    mock().checkExpectations();

}

TEST(MainAccessor, RemovePerson)
{
	// Get the setup scenario done
    // Main presenter then mock to show the DBLogin View and that will cause the Username and password
    // fields to be setup. Also login role and is logged states
    // Then we can perform the PersonEdit presenter checks under that scenario.
    mock().expectOneCall("readEditControl").onObject(mainView.get());
    mock().expectNCalls(1, "createInstance").onObject(&factory);
    mock().expectNCalls(2, "loginInterface").onObject(&factory).andReturnValue(pILoginView);
    //The controller will call loginuser presenter loadMessage to set up the view
    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_username).withStringParameter("item", "");
    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_password).withStringParameter("item", "");
    mock().expectNCalls(1, "getRole").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_userrole);
    mock().expectNCalls(1, "getLoggedState").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_logged);
    mock().expectOneCall("getValidationError").onObject(loginView.get());
    mock().expectOneCall("clear").onObject(&loginView->m_validationList);
    mock().expectOneCall("showView").onObject(loginView.get());
    presenter->loginUser();
    //controller->loginUser(pIMainView, Module::ViewIdentity::MainWindow);
    mock().checkExpectations();
    mock().clear();

    //  Login a known user
    std::string username("TEST");
    std::string password("pass");
    Module::UserRole roletype = Module::UserRole::ADMIN;
    mock().expectOneCall("readEditControl").onObject(loginView.get());
    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_username).andReturnValue(&username);
    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_password).andReturnValue(&password);
    mock().expectNCalls(1, "getRole").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_userrole).andReturnValue(&roletype);
    // Validation happens on entered data so clear and prepare for validation errors.
    mock().expectOneCall("getValidationError").onObject(loginView.get());
    mock().expectOneCall("clear").onObject(&loginView->m_validationList);
    // Known user and then the ctrl get user login details and close login view
    mock().expectNCalls(1, "getLoggedState").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_logged);
    mock().expectNCalls(1, "loginInterface").onObject(&factory).andReturnValue(pILoginView);
    mock().expectOneCall("closeView").onObject(loginView.get());

    mock().expectNCalls(2, "mainInterface").onObject(&factory).andReturnValue(pIMainView);
	mock().expectOneCall("getPersonList").onObject(mainView.get());
	mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
	mock().expectNCalls(1, "add").onObject(&mainView->personBindList);
    mock().expectNCalls(2, "showView").onObject(mainView.get());
    Module::ViewIdentity loginViewId = Module::ViewIdentity::LoginUser;
    mock().expectNCalls(1, "destroyInstance").onObject(&factory).withParameterOfType("ViewIdentity", "identity", &loginViewId);
    loginPresenter->loginUser();
	mock().checkExpectations();
    mock().clear();


    // Now need a person presenter to setup the person to remove
    Module::Person removeitem{1, "Smithy", "Terry"};
    Module::Person p;
	mock().setDataObject("SelectedPerson", "Module::Person", &removeitem);
    Storage::Bind_iterator<Module::Person> foundIter(&removeitem, 1);
    Storage::Bind_iterator<Module::Person> foundIterEnd(&p, 2);
    mock().setDataObject("FoundIter", "Storage::Bind_iterator<Module::Person>", &foundIter);
    mock().setDataObject("FoundIterEnd", "Storage::Bind_iterator<Module::Person>", &foundIterEnd);

	mock().expectOneCall("getStatus").onObject(mainView.get());
    mock().expectNCalls(1, "setItem").onObject(&mainView->statusItem).withStringParameter("item", "");
    mock().expectOneCall("readEditControl").onObject(mainView.get());
	mock().expectOneCall("getPersonList").onObject(mainView.get());
	mock().expectNCalls(1, "getSelectItem").onObject(&mainView->personBindList);
    // Remove a person from the MainWindow view using main presenter
	mock().expectOneCall("getStatus").onObject(mainView.get());
    mock().expectNCalls(1, "setItem").onObject(&mainView->statusItem).withStringParameter("item", "Removed Person");
    mock().expectNCalls(1, "mainInterface").onObject(&factory).andReturnValue(pIMainView);
	mock().expectOneCall("getPersonList").onObject(mainView.get());
	mock().expectNCalls(1, "findById").onObject(&mainView->personBindList);
	mock().expectNCalls(1, "end").onObject(&mainView->personBindList);
	mock().expectNCalls(1, "remove").onObject(&mainView->personBindList);
    mock().expectNCalls(1, "showView").onObject(mainView.get());
    presenter->removePerson();
    mock().checkExpectations();

}

TEST(MainAccessor, WorkerStopAndGetData)
{
	// Get the setup scenario done
    // Main presenter then mock to show the DBLogin View and that will cause the Username and password
    // fields to be setup. Also login role and is logged states
    // Then we can perform the PersonEdit presenter checks under that scenario.
    mock().expectOneCall("readEditControl").onObject(mainView.get());
    mock().expectNCalls(1, "createInstance").onObject(&factory);
    mock().expectNCalls(2, "loginInterface").onObject(&factory).andReturnValue(pILoginView);
    //The controller will call loginuser presenter loadMessage to set up the view
    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_username).withStringParameter("item", "");
    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_password).withStringParameter("item", "");
    mock().expectNCalls(1, "getRole").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_userrole);
    mock().expectNCalls(1, "getLoggedState").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_logged);
    mock().expectOneCall("getValidationError").onObject(loginView.get());
    mock().expectOneCall("clear").onObject(&loginView->m_validationList);
    mock().expectOneCall("showView").onObject(loginView.get());
    presenter->loginUser();
    mock().checkExpectations();

    // Login a known user
    std::string username("TEST");
    std::string password("pass");
    Module::UserRole roletype = Module::UserRole::ADMIN;
    mock().expectOneCall("readEditControl").onObject(loginView.get());
    mock().expectNCalls(1, "getUsername").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_username).andReturnValue(&username);
    mock().expectNCalls(1, "getPassword").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_password).andReturnValue(&password);
    mock().expectNCalls(1, "getRole").onObject(loginView.get());
    mock().expectOneCall("getItem").onObject(&loginView->m_userrole).andReturnValue(&roletype);
    // Validation happens on entered data so clear and prepare for validation errors.
    mock().expectOneCall("getValidationError").onObject(loginView.get());
    mock().expectOneCall("clear").onObject(&loginView->m_validationList);
    // Known user and then the ctrl get user login details and close login view
    mock().expectNCalls(1, "getLoggedState").onObject(loginView.get());
    mock().expectOneCall("setItem").onObject(&loginView->m_logged);
    mock().expectNCalls(1, "loginInterface").onObject(&factory).andReturnValue(pILoginView);
    mock().expectOneCall("closeView").onObject(loginView.get());

    mock().expectNCalls(2, "mainInterface").onObject(&factory).andReturnValue(pIMainView);
	mock().expectOneCall("getPersonList").onObject(mainView.get());
	mock().expectNCalls(1, "clear").onObject(&mainView->personBindList);
	mock().expectNCalls(1, "add").onObject(&mainView->personBindList);
    mock().expectNCalls(2, "showView").onObject(mainView.get());
    Module::ViewIdentity loginViewId = Module::ViewIdentity::LoginUser;
    mock().expectNCalls(1, "destroyInstance").onObject(&factory).withParameterOfType("ViewIdentity", "identity", &loginViewId);
    loginPresenter->loginUser();
    //controller->loginUser(pILoginView, Module::ViewIdentity::LoginUser);
    mock().checkExpectations();

//    CHECK(true == presenter->worker_has_stopped());
//
//    double fraction_done;
//    std::string message;
//    presenter->get_data(&fraction_done, &message);
//    DOUBLES_EQUAL(1.0, fraction_done, 0.01); // means past work has fully completed and is stopped
//    STRCMP_CONTAINS("StartedSelecting from storage", message.c_str());
//
//    presenter->stop_work(); // Will stop any worker running;

    mock().checkExpectations();

}



