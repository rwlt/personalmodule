
#include <module/data/StatuteStoreBind.h>
#include "storage/FileStorage.h"
#include "storage/RowRecord.h"
//#include "control/List.h"
#include "module/Person.h"
#include "module/Hero.h"
#include "module/data/PersonStoreBind.h"
#include <iostream>
#include <cstring>

//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

////
////-------------------------------------------------------
//// Data test group
////-------------------------------------------------------
//TEST_GROUP(DataAccessor)
//{
//    std::vector<Module::Person> sourcePerson;
//    std::vector<Module::Statute> sourceHero;
//    Module::Data::PersonStoreBind personStoreBind{&sourcePerson};
//    Module::Data::StatuteStoreBind heroStoreBind{&sourceHero};
//    std::string storageDesc{"Module File 1.0 - July 16, 2016"};
//    std::string filename{"moduledata.bin"};
//    void setup() {
//
//    }
//
//};
//
//TEST(DataAccessor, ModuleWrite)
//{
//    Storage::FileStorage fileStorage(storageDesc.c_str());
//
//    // Add some data.. (This source1 is the storage of all data for table - it is empty now)
//    // This data will be saved if we run doUpdateStore of fileStorage
//    sourcePerson.push_back(Module::Person(1, "Anderson", "George"));
//    sourcePerson.push_back(Module::Person(2, "Smith", "George"));
//    sourcePerson.push_back(Module::Person(3, "Blogg", "Harry"));
//    LONGS_EQUAL(3, sourcePerson.size());
//
//    sourceHero.push_back(Module::Statute(1, "Knight"));
//    sourceHero.push_back(Module::Statute(2, "Soldier"));
//    sourceHero.push_back(Module::Statute(3, "Archer"));
//    sourceHero.push_back(Module::Statute(4, "Warrior"));
//    LONGS_EQUAL(4, sourceHero.size());
//
//    // Tell file storage to save all source DaTaType to file.
//    fileStorage.storeTable("Person", &personStoreBind,  2, sourcePerson.size());
//    fileStorage.storeTable("Hero", &heroStoreBind, 3, sourceHero.size());
//    LONGS_EQUAL(0, fileStorage.getStoreSource()->size());
//    LONGS_EQUAL(2, fileStorage.totalTables());
//    fileStorage.doUpdateStore(filename);
//    LONGS_EQUAL(0, fileStorage.getStoreSource()->size()); // is cleared after
//
//}
//
//TEST(DataAccessor, Iterators)
//{
//    std::vector<Module::Person> sourcePerson2;
//    LONGS_EQUAL(0, sourcePerson.size());
//    LONGS_EQUAL(0, sourcePerson2.size());
//
//    sourcePerson.push_back(Module::Person(1, "One","TwoThree"));
//    sourcePerson.push_back(Module::Person(2, "Six","Five"));
//    sourcePerson.push_back(Module::Person(3, "Nine","Seven"));
//    sourcePerson.push_back(Module::Person(3, "Twelve","Eleven"));
//    LONGS_EQUAL(4, sourcePerson.size());
//
//    std::vector<Module::Person>::iterator s_iter;
//    for (s_iter =  sourcePerson.begin(); s_iter !=  sourcePerson.end();
//            ++s_iter) {
//        sourcePerson2.push_back(*s_iter);
//    }
//
//    CHECK(sourcePerson2.at(0).getSurname().compare("One") == 0);
//    CHECK(sourcePerson2.at(1).getSurname().compare("Six")==0);
//    CHECK(sourcePerson2.at(2).getSurname().compare("Nine") == 0);
//    CHECK(sourcePerson2.at(3).getSurname().compare("Twelve")==0);
//
//}
//
//
//
