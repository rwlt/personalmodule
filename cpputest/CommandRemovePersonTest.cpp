/*
 * CommandRemovePersonTest.cpp
 *
 *  Created on: 22/06/2017
 *      Author: rodney
 */


#include "module/Command.h"
#include "module/CommandRemovePerson.h"
#include "module/CommandAddPerson.h"
#include "storage/DBStorage.h"
#include "module/Entity.h"
#include <memory>
#include <exception>
//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

//
//-------------------------------------------------------
// CommandRemovePersonAccessor test group
//-------------------------------------------------------
TEST_GROUP(CommandRemovePersonAccessor)
{
    Storage::DBStorage dbStorage{"Module DBStorage 1.0 - September 6, 2016",
    	"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
        "<module-dbstorage>\n"
        "  <database>\n"
        "   <file role=\"live\" driver=\"firebird\" name=\"/home/rodney/databases/personal.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
        "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
        "  </database>\n"
        "<table entity=\"Module::Person\" name=\"Person\" key=\"PersonID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"PersonID\" type=\"autogenint\"/>\n"
        "        <field name=\"Firstname\" type=\"string\"/>\n"
        "        <field name=\"Surname\" type=\"string\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
        "</module-dbstorage>\n"};
    Storage::DBStorage dbStorage2{"Module DBStorage 1.0 - September 6, 2016",
    	"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
        "<module-dbstorage>\n"
        "  <database>\n"
        "   <file role=\"live\" driver=\"firebird\" name=\"/home/rodney/databases/personal.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
        "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
        "  </database>\n"
        "<table entity=\"Module::Person\" name=\"Person\" key=\"PersonID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"PersonID\" type=\"autogenint\"/>\n"
        "        <field name=\"Firstname\" type=\"string\"/>\n"
        "        <field name=\"Surname\" type=\"string\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
        "</module-dbstorage>\n"};
    std::string UserName{"TEST"};
    std::string UserPassword{"pass"};
    Module::PersonalModel model;
    Module::Person p{0, "Remove Surname", "Remove First name"};
    void setup() {
    	model.loginUser(UserName, UserPassword);
    	dbStorage.loginUser(UserName, UserPassword);
    	dbStorage2.loginUser("JOE", "password");
    }

};

TEST(CommandRemovePersonAccessor, RedoUndoAccess)
{
	// User TEST can add persons.
	CHECK(true == dbStorage.isLoggedUser());
	Module::CommandAddPerson cmd_add{&model, p};
	cmd_add.redo();
	p.setPersonID(cmd_add.getPerson().getPersonID());
	CHECK(p.getPersonID() > 0);

	Module::CommandRemovePerson cmd{&model, p};
	try {
		cmd.redo(); // Remove person
	} catch (std::exception& e) {
		LONGS_EQUAL(0,1);
	}

	// User can redo again - although this wont normally happen in real code
	// All data kept same and an no database error as delete can not find record
	// as an Delete will happen with no ID or Person record found.
	try {
		cmd.redo();
	} catch (std::exception& e) {
		LONGS_EQUAL(0,1);
	}

	try {
		cmd.undo(); // Put person back with an addItem with same ID
	} catch (std::exception& e) {
		LONGS_EQUAL(0,1);
	}

	try {
		cmd.redo(); // Remove person
	} catch (std::exception& e) {
		LONGS_EQUAL(0,1);
	}


}

//
//-------------------------------------------------------
// CommandRemovePersonConstruction test group
//-------------------------------------------------------
TEST_GROUP(CommandRemovePersonConstruction)
{
    Storage::DBStorage dbStorage{"Module DBStorage 1.0 - September 6, 2016",
    	"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
        "<module-dbstorage>\n"
        "  <database>\n"
        "   <file role=\"live\" driver=\"firebird\" name=\"/home/rodney/databases/personal.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
        "   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
        "  </database>\n"
        "<table entity=\"Module::Person\" name=\"Person\" key=\"PersonID\">\n"
        "  <database-statements>\n"
        "      <statement type=\"select\">\n"
        "        <field name=\"PersonID\" type=\"autogenint\"/>\n"
        "        <field name=\"Firstname\" type=\"string\"/>\n"
        "        <field name=\"Surname\" type=\"string\"/>\n"
        "      </statement>\n"
        "  </database-statements>\n"
        "</table>\n"
        "</module-dbstorage>\n"};
    std::string UserName{"JOE"};
    std::string UserPassword{"password"};
    Module::Person p{20, "Add Surname", "Add First name"};
    Module::PersonalModel model;
    void setup() {
    	model.loginUser(UserName, UserPassword);
    	dbStorage.loginUser(UserName, UserPassword);
    }

};

TEST(CommandRemovePersonConstruction, Constructor)
{
	Module::CommandRemovePerson cmd{&model, p};
	STRCMP_EQUAL("Add First name", p.getFirstname().c_str());

}

TEST(CommandRemovePersonConstruction, CopyConstructor)
{
	Module::CommandRemovePerson cmd{&model, p};
	Module::CommandRemovePerson cmd2(cmd);
	STRCMP_EQUAL("Add First name", p.getFirstname().c_str());

}

TEST(CommandRemovePersonConstruction, AssignConstructor)
{
	Module::CommandRemovePerson cmd{&model, p};
	Module::CommandRemovePerson cmd2 = cmd;
	STRCMP_EQUAL("Add First name", p.getFirstname().c_str());

}

TEST(CommandRemovePersonConstruction, Deconstructor)
{
	Module::CommandRemovePerson* cmd = new Module::CommandRemovePerson(&model, p);
	CHECK(cmd != 0);
	delete cmd;
}







