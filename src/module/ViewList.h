#ifndef VIEWLIST_H_
#define VIEWLIST_H_

#include <vector>

namespace Module
{

/// ViewList<DataType>: Used to move data to the view
/**
 * \tparam DataType template on data type
 */
template<class DataType>
class ViewList
{
public:
    /// Virtual deconstructor
    virtual ~ViewList() {}
    /// Use Control BindSource iterator of DataType
    /**
     * The iterator will use BindSources iterator (vectors)
     * \sa Control::BindSource()
     */
    typedef typename std::vector<DataType>::iterator iterator;
    /// begin iterator
    virtual iterator begin() = 0;
    /// end iterator
    virtual iterator end() = 0;

    /// Add item to collection
    /**
     * The item is bind to the list view component view model
     * \param item DataType value of item
     */
    virtual void add ( DataType item ) = 0;
    /// clear the list
    virtual void clear() = 0;
    /// Size of the collection
    /**
     * \return int of count items
     */
    virtual unsigned int size() = 0;
    /// set position to use for selected item using 1 based numbering
    /**
     * \param value int of position
     */
    virtual void setPosition ( int value ) = 0;
    /// get position value
    /**
     * \return value int of position
     */
    virtual int getPosition() = 0;
    /// get selected item at position
    /**
     * \return DataType reference of item at position(selected)
     */
    virtual DataType &getSelectItem() = 0;

};
}
#endif /* VIEWLIST_H_ */
