/*
 * Controller.h
 */
#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "Command.h"
#include "Person.h"
#include "Entity.h"
#include "ViewIdentity.h"
#include <vector>
#include <thread>
#include <mutex>
#include <future>

namespace Module
{
/**
 *
 * \page controller Module PersonalModule Controller
 *
 * The logic of how to use PersonalModule an an application and which views are shown
 * is all controlled from this object. It also holds the applications main data source
 * which is using command redo/undo logic on its data.
 *
 */
class MainWindowInterface;
class PersonWindowInterface;
class DBLoginWindowInterface;
class AbstractViewFactory;
class Message;
class PersonalModel;

/// Controller
class Controller
{
    CommandController commandController;        // Command control for redo/undo
    AbstractViewFactory *abstractViewFactory;
    std::unique_ptr<PersonalModel> m_personalModel;

public:
    /// Constructor
    Controller (Module::AbstractViewFactory *abstractViewFactory );
    /// Deconstructor
    ~Controller();
    /// Load main window view
    void run();

private:
    /// Load view
    void load ( Module::ViewIdentity viewIdentity, Module::ViewIdentity parentViewIdentity =
                    Module::ViewIdentity::NullView );
    /// Close view
    void close ( Module::ViewIdentity viewIdentity );
    /// Show view
    void show ( Module::ViewIdentity viewIdentity, Module::Message message );
    /// populatePersonViewList
    void doPopulatePersonViewList();

    PersonalModel* personalModel() {
    	return m_personalModel.get();
    }

    friend class MainPresenter;
    friend class PersonPresenter;
    friend class DBLoginPresenter;
    friend class PersonalModule;
};

}
#endif  /* CONTROLLER_H */
