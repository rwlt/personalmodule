/*
 * Message.cpp
 */

#include "Message.h"

namespace Module
{

Message::Message()
{

}

Message::~Message()
{
}

Parameter::Args Message::argumments() const
{
    return (m_parameter.args);
}
const FieldArgs &Message::fieldArgumments() const
{
	return (m_parameter.fieldArgs);
}
ParameterKind Message::kind() const
{
    return (m_parameter.kind);
}
void  Message::setParameter(Parameter newParam)
{
	m_parameter = newParam;
}

} /* namespace Module */
