
#include "ParseValidationRule.h"
#include <sstream>
#include <algorithm>
#include <cstring>

namespace Validate
{


ParseValidationRule::ParseValidationRule(const std::string &datatype, const char *xml) :
    m_xml{xml}, validate_datatype{datatype}
{};

/// Virtual deconstructor
ParseValidationRule::~ParseValidationRule() {};

//
// Public ReadRules method
//
void ParseValidationRule::ReadRules()
{

    m_vec_rules.clear();
    m_xmlerror.str(std::string(""));
    m_cdatabuf[0] = '\0';
    m_offs = 0;
    m_cdatabuf_over = false;
    current_datatype = "";
    rule_property = "";
    pValidator = this;

    // create m_rules and m_vec_rules
    parseValidateRule(pValidator, m_xml);
}
//
// Xml Errors
//
std::string ParseValidationRule::getXmlErrors()
{
    return m_xmlerror.str();
}
//
// Private methods
//
void ParseValidationRule::queueValidationRule(const std::string &propertyname, RuleType ruletype,
        const std::string &rulevalue, const std::string &message)
{
    m_rules.push(ValidationRule(propertyname, ruletype, rulevalue, message));
    m_vec_rules.push_back(ValidationRule(propertyname, ruletype, rulevalue, message));
}

/// readAtribute
bool ParseValidationRule::readAttribute(const char *name, const char **atts, std::string &value)
{
    bool found = false;
    int i = 0;
    while (atts[i] != 0) {
        std::string attr = toUppercase(atts[i]);
        if (attr.compare(name) == 0) {
            i++;
            found = true;
            value = atts[i];
        }
        i++; // increment to move forward
    }
    return found;
}

//
// setup xmpat xml parser of the validation rule reader
//
void ParseValidationRule::parseValidateRule(ParseValidationRule *userData, const char *xml)
{
    XML_Parser xmlparser = XML_ParserCreate(NULL);
    userData->pXmlParser = &xmlparser;
    XML_SetUserData(xmlparser, userData);
    XML_SetStartElementHandler(xmlparser, ParseValidationRule::startElement);
    XML_SetEndElementHandler(xmlparser, ParseValidationRule::endElement);
    XML_SetCharacterDataHandler(xmlparser, ParseValidationRule::charData);
    // XML_SetCommentHandler ( xmlparser, cmntData );

    reset_char_data_buffer(userData);    //impotant to reset the char content data (CDATA appender logic)

    if (XML_Parse(xmlparser, xml, strlen(xml), XML_TRUE) == XML_STATUS_ERROR) {
        userData->m_xmlerror << " " << XML_ErrorString(XML_GetErrorCode(xmlparser)) << " at line " << XML_GetCurrentLineNumber(xmlparser);
    }
    XML_ParserFree(xmlparser);
}
//
//-------------------------------------------------------------------
// Static XML parse methods
//-------------------------------------------------------------------
// xml start element
void XMLCALL ParseValidationRule::startElement(void *userData, const char *name,
        const char **atts)
{
    ParseValidationRule *userDPtr = (ParseValidationRule *) userData;
    std::string element_name = toUppercase(name);
    process_char_data_buffer(userDPtr);
    reset_char_data_buffer(userDPtr);

    if (element_name.compare("OBJECT") == 0) {
        // Get object name attribute. <object name="Module::Person">... </object>
        std::string objectName;
        auto found  = userDPtr->readAttribute("NAME", atts, objectName);
        if (!found) {
            userDPtr->m_xmlerror << " Object name attribute not found"
                                 << " at line " << XML_GetCurrentLineNumber(* (userDPtr->pXmlParser));
            return;
        }

        userDPtr->current_datatype = objectName;
        if (objectName != userDPtr->validate_datatype) {
            return;
        }

    } else if (element_name.compare("PROPERTY") == 0 &&
               userDPtr->current_datatype == userDPtr->validate_datatype) {

        // Get property name attribute. <property name="firstname">
        std::string propertyName;
        auto found  = userDPtr->readAttribute("NAME", atts, propertyName);
        if (!found) {
            userDPtr->m_xmlerror <<  " Object: " << userDPtr->current_datatype << " - Property name attribute not found"
                                 << " at line " << XML_GetCurrentLineNumber(* (userDPtr->pXmlParser));
            userDPtr->rule_property = "[missing]";
            return;
        }
        userDPtr->rule_property = propertyName;

    } else if (element_name.compare("RULE") == 0 &&
               userDPtr->current_datatype == userDPtr->validate_datatype) {

        // Get rule attributes. <rule required="yes"/> and <rule maxlength="40"/>
        // For each use property name to build new rule.
        std::string ruleValue;
        auto found  = userDPtr->readAttribute("REQUIRED", atts, ruleValue);
        if (found) {
            userDPtr->rule_type = RuleType::Required;
            userDPtr->rule_value = ruleValue;
            return;

        }
        found = userDPtr->readAttribute("MAXLENGTH", atts, ruleValue);
        if (found) {
            userDPtr->rule_type = RuleType::MaxLength;
            userDPtr->rule_value = ruleValue;
            return;
        }

        // If get here then attributes not recognized.
        userDPtr->m_xmlerror <<  " Object: " << userDPtr->current_datatype << " - Rule attributes not recognized "
                             << " at line " << XML_GetCurrentLineNumber(* (userDPtr->pXmlParser));

    }
}
// xml end element
void XMLCALL ParseValidationRule::endElement(void *userData, const char *name)
{
    ParseValidationRule *userDPtr = (ParseValidationRule *) userData;
    std::string element_name = toUppercase(name);

    process_char_data_buffer(userDPtr);
    if (element_name.compare("RULE") == 0 &&
            userDPtr->current_datatype == userDPtr->validate_datatype) {

        // Get rule attributes. <rule required="yes"/> and <rule maxlength="40"/>
        // For each use property name to build new rule.
        std::string rule_message(userDPtr->m_cdatabuf);
        // Enough info found to create and save the rule.
        userDPtr->queueValidationRule(userDPtr->rule_property,
                                      userDPtr->rule_type,
                                      userDPtr->rule_value,
                                      trimWhitespaces(rule_message));

    }
    reset_char_data_buffer(userDPtr);
}

//
// trim white spaces
//
std::string ParseValidationRule::trimWhitespaces(std::string value)
{
    auto wsfront = std::find_if_not(value.begin(), value.end(), [](int c) {
        return std::isspace(c);
    });
    auto wsback = std::find_if_not(value.rbegin(), value.rend(), [](int c) {
        return std::isspace(c);
    }).base();
    return ((wsback <= wsfront) ? std::string() : std::string(wsfront, wsback));
}
//
// to Uppercase a name
//
std::string ParseValidationRule::toUppercase(const char *name)
{
    int counter = 0;
    char upd[40];
    char *p_upd = upd;
    while (name[counter]) {
        *p_upd = toupper(name[counter]);
        p_upd++;
        counter++;
    }
    *p_upd = '\0';
    return std::string(upd);
}
//
// reset the char Data inside an element
//
void ParseValidationRule::reset_char_data_buffer(ParseValidationRule *userDPtr)
{
    userDPtr->m_offs = 0;
    userDPtr->m_cdatabuf_over = false;
    //ValidatorBase::grab_next_value = false;
}
//
// char Data inside an element
//
void XMLCALL ParseValidationRule::charData(void *userData, const XML_Char *s, int len)
{
    ParseValidationRule *userDPtr = (ParseValidationRule *) userData;
    if (!(userDPtr->m_cdatabuf_over)) {
        if (len + userDPtr->m_offs >= sizeof(userDPtr->m_cdatabuf)) {
            userDPtr->m_cdatabuf_over = true;
        } else {
            memcpy(userDPtr->m_cdatabuf + userDPtr->m_offs, s, len);
            userDPtr->m_offs += len;
        }
    }
}

//
// if the element is the one we're after
// do any CDATA conversion the character data to an integer value
//
void ParseValidationRule::process_char_data_buffer(ParseValidationRule *userDPtr)
{
    if (userDPtr->m_offs > 0) {
        userDPtr->m_cdatabuf[(userDPtr->m_offs) ] = '\0';
    }
}


}
