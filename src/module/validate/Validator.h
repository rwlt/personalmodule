/*
 * Validator.h
 */

#ifndef D_VALIDATOR_H_
#define D_VALIDATOR_H_

#include "ValidationRule.h"
#include "ValidationError.h"
#include "ParseValidationRule.h"
#include <vector>
#include <queue>
#include <string>
#include <sstream>
#include "storage/EntityMap.h"

namespace Validate
{
/**
 * \page validation Module Validation
 *
 * The logic of how to validate data using rules and constraints.
 *
 * \sa Validate::ValidationRule() and Validate::ValidationError()
 */


/// Validation using Rules and Constraint on data objects and properties.
/**
 * \defgroup Validation  Module Validation
 * @{
 */

/// Validator Base
/**
 * \tparam T data type used for module datatype 
 * 
 */
template <class T>
class ValidatorBase
{
    T m_item;
    const char *m_xml;
    std::queue<ValidationRule> m_filled_rules;
    std::vector<ValidationRule> m_vec_rules;
    std::vector<ValidationError> m_validationErrors;

public:
    /// Constructor
    /**
     * \param item item to validate
     * \param xml const char xml
     */
    ValidatorBase ( T item, const char *xml ) : m_item ( item ), m_xml ( xml )  {};
    /// Virtual deconstructor
    ~ValidatorBase() {};
    /// Validate
    /**
     * \return vector of ValidationError
     */
    std::vector<ValidationError> Validate() {
        ProcessRules();

        while ( !m_filled_rules.empty() ) {
            auto rule = m_filled_rules.front();
            auto property = rule.getPropertyName();

            std::string value ( rule.getItemValue() );
            std::string message ( rule.getMessage() );
            switch ( rule.getRuleType() ) {
            case Validate::RuleType::Required:
                if ( message == "" ) {
                    message = "Field is required";
                }
                ValidateRequired ( property, message, value );
                break;
            case Validate::RuleType::MaxLength:
                if ( message == "" ) {
                    message = "Field is too long. maximum " + rule.getRuleValue() + " characters.";
                }
                try {
                    ValidateMaximumLength ( property, message, value, std::atoi ( rule.getRuleValue().c_str() ) );
                } catch ( std::logic_error &e ) {
                    getValidationErrors().emplace_back (
                        ValidationError ( property, "Rule maxlength has wrong number for \"" + property + "\". Can not validate.", ErrorType::Rule ) );
                }
                break;
            }
            m_filled_rules.pop();
        }
        // Return any errors
        return getValidationErrors();
    }

    /// Restrict
    /**
     * \return vector of ValidationError
     */
    std::vector<ValidationError> Restrict() {
        ProcessRules();
        //    ConstraintCheck ( p => p.TaskJobs, "<FieldName> are found, no removal allowed until publication has no task jobs.", m_person.TaskJobs );

        // Return any errors
        return getValidationErrors();
    }

    /// Get Rules
    /**
     * This returns a copy of the rule queue as a vector for inspection purposes
     * \return vector of ValidationRules
     */
    std::vector<ValidationRule> &getRules () {
        BuildRules();
        return m_vec_rules;
    }

    /// Get ValidationErrors
    /**
     * \return vector of ValidationError
     */
    std::vector<ValidationError> &getValidationErrors () {
        return m_validationErrors;
    }
    /// Set ValidationErrors
    /**
     * \param in_errors of ValidationError
     */
    void setValidationErrors ( std::vector<ValidationError> in_errors ) {
        m_validationErrors = in_errors;
    }


private:
    /// BuildRules
    /**
     * Build Validation rules from Validator with a T type
     */
    template<typename A = Storage::EntityMap<T>, typename M = Storage::EntityTraits<T>>
    void BuildRules () {
        //M moduleDataTrait;

        m_validationErrors.clear();
        m_vec_rules.clear();

        // Use ParseValidationRule class to read the rules
        ParseValidationRule parser(M::entitytype, m_xml);
        parser.ReadRules();
        std::string parseerror = parser.getXmlErrors();
        if ( parseerror.length() > 0 ) {
            getValidationErrors().emplace_back ( ValidationError ( M::entitytype, parseerror, ErrorType::XML ) );
        }
        m_vec_rules = parser.getRules();
    }
    /// BuildRules
    /**
     * Build Validation rules from Validator with a T type
     */
    template<typename A = Storage::EntityMap<T>, typename M = Storage::EntityTraits<T>>
    void ProcessRules () {
        A moduleMap;
        //M moduleDataTrait;

        m_validationErrors.clear();
        m_vec_rules.clear();
        
        // Use ParseValidationRule class to read the rules
        ParseValidationRule parser(M::entitytype, m_xml);
        parser.ReadRules();
        std::string parseerror = parser.getXmlErrors();
        if ( parseerror.length() > 0 ) {
            getValidationErrors().emplace_back ( ValidationError ( M::entitytype, parseerror, ErrorType::XML ) );
        }
        m_vec_rules = parser.getRules();

        // Rules are read for M::name datatrait. They are applied with T's data ready for Validate function.
        // Uses M trait to read T property names to add the values to check with rule.
        // Check that rules properties can be found in the ModuleMap and create not valid if not found.
        // Use the parser.ReadRules
        while ( !parser.useRules().empty() ) {
            auto rule = parser.useRules().front();
            auto property = rule.getPropertyName();

            std::string value ( "" );
            moduleMap.getMapField ( property);
            if ( !moduleMap.field_avaliable() ) {
                //Property found is not known make a valdation error to stop invalid entry
                switch ( rule.getRuleType() ) {
                case Validate::RuleType::Required:
                    getValidationErrors().emplace_back ( ValidationError ( property, "Required rule could not find that module property.", ErrorType::Rule ) );
                    break;
                case Validate::RuleType::MaxLength:
                    getValidationErrors().emplace_back ( ValidationError ( property, "Maxlength rule could not find that module property.", ErrorType::Rule ) );
                    break;
                }
            }

            while ( moduleMap.field_avaliable() ) {
            	Storage::FieldDetail fld = moduleMap.do_front();
                // TODO: We know it is string type
                //std::cout << "Validator fld " << fld.field_name << std::endl;
                moduleMap.getField ( m_item, fld.field_name, value );
                // Got T value at field now do queue the updated rule as filled.
                rule.setItemValue ( value );
                m_filled_rules.push ( rule );
            }
            parser.useRules().pop();
        }

    }

    // ValidateRequired (overload for integer)
    /* Validate check on integer required as a value above 0 (Normally for ID use)
    * \param fieldName Objects property name.
    * \param message Message to add to validation errors when invalid.
    * \param value Interger value to check (Specific ID and FK are 0. (ID_NullValue is 0 and means invalid for required)
    */
    /*  void ValidateIDRequired ( Expression<Func<T, int>> fieldName, string message, int value ) {
          string propertyName = CommonBase.GetPropertyName ( fieldName );
          if ( value == TMCommon.CommonBase.ID_NullValue ) {
              getValidationErrors.Add ( new ValidationError ( propertyName, message ) );
          }
      }*/

    // ValidateRequired (overload for integer)
    /*
    * Validate check on integer required as a value above 0 (Normally for ID use)
    * \param fieldName Objects property name.
    * \param message Message to add to validation errors when invalid.
    * \param value Interger value to check. (Int_NullValue is 0 and means invalid for required)
    */
    /*void ValidateRequired ( Expression<Func<T, int>> fieldName, string message, int value ) {
        string propertyName = CommonBase.GetPropertyName ( fieldName );
        if ( value == TMCommon.CommonBase.Int_NullValue ) {
            ValidationErrors.Add ( new ValidationError ( propertyName, message ) );
        }
    }*/

    /// ValidateRequired (overload for string)
    /**
     * Validate check on string value to have a required value and not as empty.
     * \param fieldname Objects property name.
     * \param message Message to add to validation errors when invalid.
     * \param value String value to check
     */
    void ValidateRequired ( const std::string &fieldname, const std::string &message, const std::string &value ) {
        if ( value == "" ) {
            getValidationErrors().emplace_back ( ValidationError ( fieldname, message, ErrorType::Validate ) );
        }
    }

    /// Validate Maximum Length (overload for string properties)
    /**
     * Validate check on string value to have a required value and not as empty.
     * \param fieldname Object property name
     * \param message Message to add to validation errors when invalid.
     * \param value String value to check
     * \param size Maximum length of charactors allowed.
     */
    void ValidateMaximumLength ( const std::string &fieldname, const std::string &message, const std::string &value, int size ) {
        if ( value.compare("") != 0 && (int)value.length() > size ) {
            getValidationErrors().emplace_back ( ValidationError ( fieldname, message, ErrorType::Validate ) );
        }
    }

    /// ConstraintCheck
    /**
     * Validate check on string value to have a required value and not as empty.
     * \param fieldName Objects property name.
    * \param message Message to add to validation errors when invalid.
    * \param value String value to check
    */
    /*    void ConstraintCheck<U> ( Expression<Func<T, IList<U>>> fieldName, string message, IList<U> value ) {
            string propertyName = CommonBase.GetPropertyName ( fieldName );
            if ( value.Count > 0 ) {
                ValidationErrors.Add ( new ValidationError ( propertyName, message ) );
            }
        }*/

    /** @} */ /* group Validation */

};


} /* namespace Validate */

#endif /* D_VALIDATOR_H_ */

