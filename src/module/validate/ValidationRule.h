/*
 * ValidationRule.h
 */

#ifndef D_VALIDATIONRULE_H_
#define D_VALIDATIONRULE_H_

#include <vector>
#include <string>
#include <sstream>

/// Validate namespace
/**
 * Namespace for validator for presenter
 */
namespace Validate
{
/// Rule type
enum class RuleType : char {
    Required,   ///< rule required
    MaxLength  ///< maximum length
};

/// Validation Rule
class ValidationRule
{

    std::string m_propertyname;
    RuleType m_ruletype;
    std::string m_rulevalue;
    std::string m_itemvalue;
    std::string m_message;

public:
    /// Constructor
    /**
     * \param propertyname const reference of string
     * \param ruletype enum of rule type
     * \param rulevalue string of value
     * \param message const reference to string
     */
    ValidationRule ( const std::string &propertyname, RuleType ruletype,
                     const  std::string &rulevalue, const std::string &message ) :
        m_propertyname ( propertyname ),
        m_ruletype ( ruletype ), m_rulevalue ( rulevalue ),
        m_itemvalue ( "" ), m_message ( message ) {
    }

    /// Virtual deconstructor
    virtual ~ValidationRule() {};

    /// get FieldName
    /** FieldName is required.  It tells us which entity field
     * value triggered the validation error.
     * \return string
     * */
    std::string getPropertyName() {
        return m_propertyname;
    }
    /// RuleType
    /**
     * \return enum rule type
     */
    RuleType getRuleType() {
        return m_ruletype;
    }

    /// Rule Value
    /**
     * \return string rule value
     */
    std::string getRuleValue() {
        return m_rulevalue;
    }

    /// Item Value
    /**
     * \return string rule value
     */
    std::string getItemValue() {
        return m_itemvalue;
    }

    /// set Item Value
    /**
     * \param value const reference to string
     */
    void setItemValue ( const std::string& value ) {
        m_itemvalue = value;
    }

    /// Message Value
    /**
     * \return string rule value
     */
    std::string getMessage() {
        return m_message;
    }
    
    /// Equality operator
    /**
     * \param lhs left hand side
     * \param rhs right hand side
     * \return true if equal
     */
    friend bool operator == ( const ValidationRule & lhs , const ValidationRule & rhs ) ;
    /// Inequality operator
    /**
     * \param lhs left hand side
     * \param rhs right hand side
     * \return true if not equal
     */
    friend bool operator != ( const ValidationRule & lhs , const ValidationRule & rhs ) ;


};

/// Equality operator
/**
 * \param lhs left hand side
 * \param rhs right hand side
 * \return true if equal
 */
inline bool operator == ( const ValidationRule & lhs , const ValidationRule & rhs )
{
    return ( lhs.m_propertyname == rhs.m_propertyname && lhs.m_ruletype == rhs.m_ruletype
             && lhs.m_rulevalue == rhs.m_rulevalue );
}
/// Equality operator
/**
 * \param lhs left hand side
 * \param rhs right hand side
 * \return true if not equal
 */
inline bool operator != ( const ValidationRule & lhs , const ValidationRule & rhs )
{
    return ! ( lhs == rhs );
}

} /* namespace Validate */

#endif /* D_VALIDATIONRULE_H_ */
