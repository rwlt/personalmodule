/*
 * ParseValidatorRule.h
 */

#ifndef D_PARSEVALIDATIONRULE_H_
#define D_PARSEVALIDATIONRULE_H_

#include <expat.h>
#include <expat_external.h>
#include <string>
#include <sstream>
#include <vector>
#include <queue>
#include "ValidationRule.h"

namespace Validate
{

/// Parse Validation Rules
/**
 */
class ParseValidationRule
{
    const char *m_xml{};
    std::stringstream m_xmlerror{}; ///< what was xml parser problem
    char m_cdatabuf[1024]{};        ///< xml cdata buffer to concatenate value
    size_t m_offs{0};                ///< char concatenate buffer accumulate offset
    bool m_cdatabuf_over{false};         ///< if cdata char buffer it too big, we use 1024 chars
    std::string validate_datatype{""};  ///< validate data type
    std::string current_datatype{""};   ///< xml parse current data type
    std::string rule_property{""};      ///< rules property
    RuleType rule_type{RuleType::Required};   ///< rule type
    std::string rule_value{""};         ///< rule type values
    ParseValidationRule *pValidator{};
    XML_Parser *pXmlParser{};
    std::queue<ValidationRule> m_rules{};
    std::queue<ValidationRule> m_filled_rules{};
    std::vector<ValidationRule> m_vec_rules{};

public:
    /// Constructor
    /**
     * \param datatype const reference to string of the datatype name
     * \param xml const char xml
     */
    ParseValidationRule ( const std::string &datatype, const char *xml );
    /// Virtual deconstructor
    ~ParseValidationRule();

    /// Public ReadRules method
    /**
     */
    void ReadRules ();
    /// Get Read rules xml errors if any
    /**
     * Empty if no error
     * \return string of xml errors
     */
    std::string getXmlErrors ();
    /// Get Rules
    /**
     * This returns a reference of the rule queue as a vector for inspection purposes
     * \return vector of ValidationRules
     */
    std::vector<ValidationRule> &getRules () {
        return m_vec_rules;
    }

    /// Use Rule queue
    /**
     * This returns a reference of the rules queue to use the queue
     * \return reference to ValidationRules
     */
    std::queue<ValidationRule> &useRules () {
        return m_rules;
    }


private:

    /// Queue a validation rule to the current build rules of T
    /** The rule is a template which later is filled with T values using the Module::ModuleDataTraits
     * It is put in a queue so the most important ones can be put in first
     * \param propertyname const string reference of T property
     * \param ruletype enum Rule type
     * \param rulevalue string for function
     * \param message const string
     * \sa Module::ModuleDataTraits()
     */
    void queueValidationRule ( const std::string &propertyname, RuleType ruletype,
                               const std::string &rulevalue, const std::string &message );
    /// readAtribute
    bool readAttribute ( const char *name, const char **atts, std::string &value );
    // setup xmpat xml parser of the validation rule reader
    static void parseValidateRule ( ParseValidationRule *userData, const char *xml );
    // Static XML parse methods
    // xml start element
    static void XMLCALL startElement ( void *userData, const char *name, const char **atts );
    // xml end element
    static void XMLCALL endElement ( void *userData, const char *name );
    // trim white spaces
    inline static std::string trimWhitespaces ( std::string value );
    // to Uppercase a name
    inline static std::string toUppercase ( const char *name );
    // reset the char Data inside an element
    static void reset_char_data_buffer ( ParseValidationRule *userDPtr );
    // char Data inside an element
    static void XMLCALL charData ( void *userData, const XML_Char *s, int len );
    // if the element is the one we're after
    // do any CDATA conversion the character data to an integer value
    static void process_char_data_buffer ( ParseValidationRule *userDPtr );

};

}

#endif /* D_PARSEVALIDATIONRULE_H_*/
