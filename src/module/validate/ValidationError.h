/*
 * ValidationError.h
 */

#ifndef D_VALIDATIONERROR_H_
#define D_VALIDATIONERROR_H_

#include "storage/EntityMap.h"
#include <unordered_map>
#include <vector>
#include <string>
#include <sstream>

/// Validate namespace
/**
 * Namespace for validator for presenter
 */
namespace Validate
{
/// Error type
enum class ErrorType : char {
    XML,       ///< xml error
    Rule,       ///< maximum length
    Validate
};

/// Validation error
class ValidationError
{

    std::string m_fieldName;
    std::string m_errorMessage;
    std::string m_uiFieldName{};
    ErrorType m_errortype{ErrorType::Validate};
    int m_sortOrder{0};

public:
    /// Constructor
    ValidationError () {};
    /**
     * \param fieldName const reference of string
     * \param errorMessage const reference of string
     * \param errorType error type of validation
     */
    ValidationError ( const std::string &fieldName, const std::string &errorMessage,
                      ErrorType errorType = ErrorType::Validate )
        : m_fieldName ( fieldName ),
		  m_errorMessage ( errorMessage ),
          m_errortype ( errorType ) {
    }

    /// Virtual deconstructor
    virtual ~ValidationError() {};

    /// get FieldName
    /** FieldName is required.  It tells us which entity field
     * value triggered the validation error.
     * \return string
     * */
    const std::string getFieldName() const {
        return m_fieldName;
    }

    ///set  FieldName
    /** FieldName is required.  It tells us which entity field
     * value triggered the validation error.
     * \param value const reference to string
     * */
    void setFieldName ( const std::string &value ) {
        m_fieldName = value;
    }

    /// UIFieldName
    /** The UI label will often be different than the field name
     * on the business object.  This field allows the UI to set
     * a name that is consistent with the UI label.  If set, this
     * is the name that will be used in ErrorMessage.
     * \return string
     */
    std::string getUIFieldName () {
        return m_uiFieldName;
    }

    /// UIFieldName
    /** The UI label will often be different than the field name
     * on the business object.  This field allows the UI to set
     * a name that is consistent with the UI label.  If set, this
     * is the name that will be used in ErrorMessage.
     * \param value const reference to value
     */
    void setUIFieldName ( const std::string &value ) {
        m_uiFieldName = value;
    }

    /// ErrorType
    /**
     * \return enum error type
     */
    ErrorType getRuleType() {
        return m_errortype;
    }

    /// set ErrorType
    /**
     * \param value error type
     * \return enum of error 
     */
    void setErrorType ( ErrorType value ) {
        m_errortype = value;
    }

    /// ErrorMessage
    /** Contains the FieldName marker instead of field names.
    * This allows us to replace with a UI-friendly name, the
    * UIFieldName.  If UIFieldName isn't set use FieldName.
    * \return string
    */
    const std::string getErrorMessage () const {
        return m_errorMessage;
        /*if ( m_uiFieldName == "" ) {
            return m_errorMessage.replace( "<FieldName>", m_fieldName );
        } else {
            return m_errorMessage.replace ( "<FieldName>", m_uiFieldName );
        }*/
    }

    /// SortOrder
    /**
     * \return int
     */
    int getSortOrder() {
        return m_sortOrder;
    }

    /// SortOrder
    /**
     * \param value int
     */
    void setSortOrder ( int value ) {
        m_sortOrder = value;
    }

    /// Equality operator
    /**
     * \param lhs left hand side
     * \param rhs right hand side
     * \return true if equal
     */
    friend bool operator == ( const ValidationError & lhs , const ValidationError & rhs ) ;
    /// Inequality operator
    /**
     * \param lhs left hand side
     * \param rhs right hand side
     * \return true if not equal
     */
    friend bool operator != ( const ValidationError & lhs , const ValidationError & rhs ) ;


};

/// Equality operator
/**
 * \param lhs left hand side
 * \param rhs right hand side
 * \return true if equal
 */
inline bool operator == ( const ValidationError & lhs , const ValidationError & rhs )
{
    return ( lhs.m_fieldName == rhs.m_fieldName && lhs.m_errorMessage == rhs.m_errorMessage
             && lhs.m_errortype == rhs.m_errortype );
}
/// Inequality operator
/**
 * \param lhs left hand side
 * \param rhs right hand side
 * \return true if not equal
 */
inline bool operator != ( const ValidationError & lhs , const ValidationError & rhs )
{
    return ! ( lhs == rhs );
}

} /* namespace Validate */


#endif /* D_VALIDATIONERROR_H_ */
