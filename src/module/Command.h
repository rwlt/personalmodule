/*
 * Command.h
 */

#ifndef MODULECOMMAND_H_
#define MODULECOMMAND_H_

#include <stack>
#include <memory>

namespace Module
{

/// Command abstraction
class Command
{
public:
    /// redo
    virtual void redo () = 0;
    /// undo
    virtual void undo () = 0;
    /// virtual deconstructor
    virtual ~Command();
};

/// commandStack_t
typedef std::stack<std::shared_ptr<Command> > commandStack_t;

/// CommandController
class CommandController
{
    commandStack_t mUndoStack;
    commandStack_t mRedoStack;
    
public:
    CommandController();
    virtual ~CommandController();
    
    /// execute a command to change data
    /**
     * \param command shared_ptr to command
     */
    void executeCmd ( std::shared_ptr<Command>  command );
    /// undo
    void undo();
    /// redo
    void redo();
    /// changes changes
    /**
     * \return true if changes active
     */
    bool changesActive();
};

} /* namespace Module */

#endif /* MODULECOMMAND_H_ */
