/*
 * Person.h
 */

#ifndef PERSON_H_
#define PERSON_H_

#include "storage/EntityMap.h"
#include "Chest.h"
#include <string>

namespace Module
{
/// Person
class Person
{
public:
    /// Constructor
	Person()
	{
	}
    /// Constructor
    /**
     * \param id int of ID
     * \param newSurname const reference to string
     * \param newFirstName const reference to string
     */
    Person( int id, const std::string &newSurname, const std::string &newFirstName)
        : m_PersonID(id), m_Surname(newSurname), m_Firstname(newFirstName)
    {
    }

    virtual ~Person() {}

	ENTITY_INT_ATTRIBUTE(PersonID)
	ENTITY_STRING_ATTRIBUTE(Surname)
	ENTITY_STRING_ATTRIBUTE(Firstname)
	ENTITY_INT_ATTRIBUTE(ChestID)

private:
	Chest chest{};
public:
	/// Get Chest
	const Chest getChest() const {	return chest; }
	/// Set Chest
	void setChest(Chest newvalue){ chest = newvalue; }

	/// Equality operator
     /**
      * \param lhs left hand side
      * \param rhs right hand side
      * \return true if equal
      */
     friend bool operator==(const Person &lhs, const Person &rhs);
     /// Inequality operator
     /**
      * \param lhs left hand side
      * \param rhs right hand side
      * \return true if not equal
      */
     friend bool operator!=(const Person &lhs, const Person &rhs);

private:
    friend class Hero;

};

/// operator ==
/**
 * \param lhs const reference to person
 * \param rhs const reference to person
 * \return true if equal
 */
inline bool operator == ( const Person &lhs, const Person &rhs )
{
    return (lhs.m_PersonID == rhs.m_PersonID &&
             lhs.m_Firstname.compare(rhs.m_Firstname) == 0 &&
             lhs.m_Surname.compare(rhs.m_Surname) == 0);
}

/// Equality operator
inline bool operator!=(const Person &lhs, const Person &rhs) {
    return !(lhs == rhs);
}

} /* namespace Module */

#endif /* PERSON_H_ */
