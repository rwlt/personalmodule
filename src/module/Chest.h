/*
 * Chest.h
 *
 *  Created on: 26/07/2017
 *      Author: rodney
 */

#ifndef SRC_MODULE_CHEST_H_
#define SRC_MODULE_CHEST_H_

#include "storage/EntityMap.h"
#include <string>

namespace Module
{


class ChestItems;
/// Chest
class Chest {
	ENTITY_INT_ATTRIBUTE(ChestID)
	ENTITY_INT_ATTRIBUTE(PersonID)
	ENTITY_STRING_ATTRIBUTE(Name)
private:
	std::vector<ChestItems> chestitems{};
public:
	/// Get Chest items
	const std::vector<ChestItems> &getChestItems() const {	return chestitems; }
	/// Add Chest items
	void addChestItem(const ChestItems &newvalue){ chestitems.push_back(newvalue); }

};

/// Inventory
class Inventory {
	ENTITY_INT_ATTRIBUTE(InventoryID)
	ENTITY_INT_ATTRIBUTE(Quota)
	ENTITY_STRING_ATTRIBUTE(Name)
};

/// ChestItems
class ChestItems {
	ENTITY_INT_ATTRIBUTE(ChestItemID)
	ENTITY_INT_ATTRIBUTE(InventoryID)
	ENTITY_INT_ATTRIBUTE(ChestID)

private:
	Inventory inventory{};
public:
	/// Get Inventory
	const Inventory getInventory() const { return inventory; }
	/// Set Inventory
	void setInventory(Inventory newvalue){ inventory = newvalue; }
};


} /* namespace Module */


#endif /* SRC_MODULE_CHEST_H_ */
