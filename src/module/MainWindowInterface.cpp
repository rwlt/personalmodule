/*
 * MainWindowInterface.cpp
 */

#include "MainWindowInterface.h"

namespace Module
{

MainWindowInterface::MainWindowInterface(bool emit_view ) : View(ViewIdentity::MainWindow, emit_view)
{
}

MainWindowInterface::~MainWindowInterface()
{
}

} /* namespace Module */
