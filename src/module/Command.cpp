/*
 * Command.cpp and CommandController
 */
#include "Command.h"

namespace Module {

Command::~Command() {}

CommandController::CommandController() {}
CommandController::~CommandController() {
	mRedoStack = commandStack_t();
	mUndoStack = commandStack_t();
}

void CommandController::executeCmd ( std::shared_ptr<Command>  command ) {
    mRedoStack = commandStack_t(); // clear the redo stack
    command->redo();
    mUndoStack.push ( command );
}

void CommandController::undo() {
    if ( mUndoStack.empty() == true ) {
        return;
    }
    mUndoStack.top()->undo();          // undo most recently executed command
    mRedoStack.push ( mUndoStack.top() ); // add undone command to undo stack
    mUndoStack.pop();                  // remove top entry from undo stack
}

void CommandController::redo() {
    if ( mRedoStack.empty() == true ) {
        return;
    }
   	mRedoStack.top()->redo();          // redo most recently executed command
    mUndoStack.push ( mRedoStack.top() ); // add undone command to redo stack
    mRedoStack.pop();                  // remove top entry from redo stack
}

bool CommandController::changesActive() {
    return (!mUndoStack.empty() || !mRedoStack.empty());
}

}
