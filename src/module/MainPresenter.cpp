/*
 * MainPresenter.cpp
 */

#include "MainPresenter.h"
#include "PersonWindowInterface.h"
#include "MainWindowInterface.h"
#include "PersonalModel.h"
#include "Controller.h"
#include "storage/DBStorage.h"
#include "CommandRemovePerson.h"
#include "Person.h"
#include "Message.h"
#include <iostream>
#include <memory>
#include <stdexcept>

namespace Module
{

///////////////////////////////////////////////////////////////////////////////
// Public constructor
///////////////////////////////////////////////////////////////////////////////
/*m_versionDesc("Module File 1.0 - July 16, 2016"),*/
MainPresenter::MainPresenter(MainWindowInterface *const viewInterface,  Module::Controller *controller) :
    Module::AbstractPresenter(controller),
    view(viewInterface), model(ctrl()->personalModel())
{
	model->addBroadcast([this] ( Module::Person& p, bool remove) {
		Module::ViewBindList<Module::Person> &list = view->getPersonList();
		Module::ViewBindList<Module::Person>::iterator founditer;
		founditer = list.findById(p.getPersonID());
		if (founditer != list.end()) {
			if (!remove) {
				std::cout << "View is Broadcast update " << p.getPersonID() << "\n";
				founditer = p;
			} else {
				std::cout << "View is Broadcast remove " << p.getPersonID() << "\n";
				list.remove(p.getPersonID());
			}
		} else {
			std::cout << "View is Broadcast add " << p.getPersonID() << "\n";
			list.add(p);
		}
		view->worker_notify(); // let the view update itself (not a new thread this notify)
    });

}

MainPresenter::~MainPresenter()
{
    std::lock_guard<std::mutex> lock(m_Mutex);
    if (m_has_stopped == false) {
        m_worker_thread.detach();
    } else  {
        if (m_worker_thread.joinable()) {
            m_worker_thread.join();
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
void MainPresenter::initialize()
{
    view->modulePresenter(this);
    Module::ViewItem<std::string> &viewStatus = view->getStatus();
    Module::ViewBindList<Module::Person> &viewPersonList = view->getPersonList();

    ctrl()->commandController = CommandController();

    viewPersonList.clear(); // Empty list to start
    viewStatus.setItem("Ready");

}
//
///////////////////////////////////////////////////////////////////////////////
// protected function
///////////////////////////////////////////////////////////////////////////////
//
void MainPresenter::loadMessage(const Module::Message &message)
{
    // Process the message to setup the view to show a spefic record of data
    // and to store what position is is at in the ctrl data source
    // Also info on a new item or to change an item
    switch (message.kind()) {
    case Module::ParameterKind::ONE_ARG:
    	if (message.argumments().oneArg.message == 2) { // notified list is changes
			if (worker_has_stopped()) {
				if (model->loggedUser()) {
					join_detach();
					start_work(view->m_emit_view);
					// If an emit signal view available ie a separate non blocking thread
					// return to the caller now, otherwise wait here for the db read to complete
					if (view->m_emit_view == false) {
						while (worker_has_stopped() == false) {
							// sleep a bit to let any of the new thread dbWorker messages to be processed on this
							// callers (could be a console blocked and any messages can be displayed there)
							std::this_thread::sleep_for(std::chrono::microseconds(150));
						}
						complete_dbworker();
					}
				} else {
					Module::ViewBindList<Person> &viewPersonList = view->getPersonList();
					viewPersonList.clear();
				}
			}
    	}
        // Setup view for edit an item
        break;
    case Module::ParameterKind::FIELD_ARG:
        // Setup view for edit a person using the message Field Arguments as requested values
    	for (Storage::FieldDetail fd : message.fieldArgumments()) {
    		if (fd.field_name.compare("PersonID") == 0) {
    			//m_originalPerson.setPersonID(fd.int_value);
    		}
    	}
        break;
    case Module::ParameterKind::TWO_ARG:
        // Setup view for edit an item
        break;
    default:
        // Otherwise setup for add an item
        break;
    }

}
//
///////////////////////////////////////////////////////////////////////////////
// Public method
///////////////////////////////////////////////////////////////////////////////
//
void MainPresenter::loginUser()
{
    if (view == nullptr) {
        return;
    }
   view->readEditControl(); //Make sure view has updated personitem source
 	// Check if it is a logged User and no active DB reading occurrence.
	Module::Message message;
	Module::Parameter param;
	param.kind = Module::ParameterKind::ONE_ARG;
	param.args.oneArg.message = 0;
	message.setParameter(param);// Default empty message is OK to do add a new person on PersonView
    ctrl()->load(ViewIdentity::LoginUser, ViewIdentity::MainWindow);
    ctrl()->show(ViewIdentity::LoginUser, message);
}

//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//

void MainPresenter::addNewPerson()
{
//	if (!worker_has_stopped()) {
//		view->getStatus().setItem("Busy");
//		return;
//    }
    if (!model->loggedUser()) {
		view->getStatus().setItem("Login required.");
		return;
    }
	Module::Message message;
	Module::Parameter param;
	view->getStatus().setItem("");// Refresh status
	view->readEditControl(); //Make sure view is updated first then open the PersonEdit view
	param.kind = Module::ParameterKind::ONE_ARG;
	param.args.oneArg.message = 0;
	message.setParameter(param);// Default empty message is OK to do add a new person on PersonView
	ctrl()->load(ViewIdentity::PersonEdit, ViewIdentity::MainWindow);
	ctrl()->show(ViewIdentity::PersonEdit, message);
}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
void MainPresenter::changePerson()
{
	// Check if it is a logged User and no active DB reading occurrence.
//    if (!worker_has_stopped()) {
//		view->getStatus().setItem("Busy");
//		return;
//    }
    if (!model->loggedUser()) {
		view->getStatus().setItem("Login required.");
		return;
    }
	Module::Message message;
	Module::Parameter paramArgs;
	view->getStatus().setItem("");// Refresh status
	view->readEditControl(); //Make sure view has updated personitem source
	Module::Person person = view->getPersonList().getSelectItem();
	paramArgs.kind = Module::ParameterKind::FIELD_ARG;
	paramArgs.fieldArgs = {
			{"PersonID" , person.getPersonID()},
			{"Firstname" , person.getFirstname()},
			{"Surname" , person.getSurname()} };
	message.setParameter(paramArgs);
	ctrl()->load(ViewIdentity::PersonEdit, ViewIdentity::MainWindow);
	ctrl()->show(ViewIdentity::PersonEdit, message);
}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
void MainPresenter::removePerson()
{
	// Check if it is a logged User and no active DB reading occurrence.
//    if (!worker_has_stopped()) {
//		view->getStatus().setItem("Busy");
//		return;
//    }
    if (!model->loggedUser()) {
		view->getStatus().setItem("Login required.");
		return;
    }
	Module::Message message;
	Module::Parameter param;
	param.kind = Module::ParameterKind::ONE_ARG;
	param.args.oneArg.message = 0;
	message.setParameter(param);// Default empty message is OK to do add a new person on PersonView
	view->getStatus().setItem("");// Refresh status
	view->readEditControl(); //Make sure view has updated personitem source
    auto removePerson = view->getPersonList().getSelectItem();
	try {
		ctrl()->commandController.executeCmd(std::make_shared<CommandRemovePerson>(model, removePerson));
		view->getStatus().setItem("Removed Person");
		ctrl()->show(Module::ViewIdentity::MainWindow, message);
	} catch (std::exception& e) {
		view->getStatus().setItem(e.what());
	}

}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
void MainPresenter::undo()
{
	Module::Message message;
	Module::Parameter param;
	param.kind = Module::ParameterKind::ONE_ARG;
	param.args.oneArg.message = 0;
	message.setParameter(param);// Default empty message is OK to do add a new person on PersonView
	ctrl()->commandController.undo();
	view->getStatus().setItem("Undo complete");
	ctrl()->show(Module::ViewIdentity::MainWindow, message);
}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
void MainPresenter::redo()
{
	Module::Message message;
	Module::Parameter param;
	param.kind = Module::ParameterKind::ONE_ARG;
	param.args.oneArg.message = 0;
	message.setParameter(param);// Default empty message is OK to do add a new person on PersonView
	ctrl()->commandController.redo();
	view->getStatus().setItem("Redo complete");
	ctrl()->show(Module::ViewIdentity::MainWindow, message);
}
//
///////////////////////////////////////////////////////////////////////////////
// public functions
///////////////////////////////////////////////////////////////////////////////
//
void MainPresenter::start_work(bool emit_view)
{
    std::lock_guard<std::mutex> lock(m_Mutex);
    if (m_has_stopped == true) {
        m_worker_thread = std::thread(&MainPresenter::do_work, this, emit_view);
        m_has_stopped = false;
        m_shall_stop = false;
    }
}
//
///////////////////////////////////////////////////////////////////////////////
// public functions
///////////////////////////////////////////////////////////////////////////////
//
void MainPresenter::stop_work() {
   std::lock_guard<std::mutex> lock(m_Mutex);
   m_shall_stop = true;
}

//
///////////////////////////////////////////////////////////////////////////////
// public functions
///////////////////////////////////////////////////////////////////////////////
//
void MainPresenter::join_detach()
{
    if (m_worker_thread.joinable()) {
        std::cout << "jionable...\n";
        m_worker_thread.join();
    }
}

//
///////////////////////////////////////////////////////////////////////////////
// public functions
///////////////////////////////////////////////////////////////////////////////
//
bool MainPresenter::worker_has_stopped()
{
    std::lock_guard<std::mutex> lock(m_Mutex);
    return m_has_stopped;
}
//
///////////////////////////////////////////////////////////////////////////////
// public functions
///////////////////////////////////////////////////////////////////////////////
//
void MainPresenter::get_data(double *fraction_done, std::string *message)
{
    std::lock_guard<std::mutex> lock(m_Mutex);

    if (fraction_done) {
        *fraction_done = m_fraction_done;
    }

    if (message) {
        *message = m_message;
    }
}
//
///////////////////////////////////////////////////////////////////////////////
// public functions
///////////////////////////////////////////////////////////////////////////////
//
void MainPresenter::complete_dbworker()
{
    if (worker_has_stopped()) {
        if (model->loggedUser()) {
            std::lock_guard<std::mutex> lock(m_Mutex);
            std::cout << "DB work populating caller view\n";
            Module::ViewBindList<Person> &viewPersonList = view->getPersonList();
            if (!m_dberrors.empty()) {
            	std::stringstream message;
            	for (const std::string line : m_dberrors) {
            	   message << line << " ";
            	}
            	view->showError(message.str());
            }
            if (m_personList.size() > 0) {
                viewPersonList.clear();
                for (const Module::Person & person : m_personList) {
                    viewPersonList.add(person);
                }
                m_personList.clear(); // clear it now it gets populated on doWork DB initial read only
            }
        }
        join_detach();
    }
}
//
///////////////////////////////////////////////////////////////////////////////
// public functions
///////////////////////////////////////////////////////////////////////////////
//
void MainPresenter::worker_notify()
{
	view->worker_notify();
}

//
///////////////////////////////////////////////////////////////////////////////
// private functions
///////////////////////////////////////////////////////////////////////////////
//
void MainPresenter::do_work(bool emit_view)
{
    {
        std::lock_guard<std::mutex> lock(m_Mutex);
        m_has_stopped = false;
        m_fraction_done = 0.0;
        m_message = "";
    } // The mutex is unlocked here by lock's destructor.

    std::cout << "Reading DB" << std::endl;
    m_personList.clear();
//	Storage::DBRetrieveMap where;
//	Storage::DBRetrieveMap params;
	std::future<std::vector<Module::Person>> result(
			std::async(
					std::launch::async, &Module::PersonalModel::doRead<Module::Person>, model));
	double fraction_done;
	std::string message{""};
	std::cout << "fraction done ";
	do {
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
		model->get_data(&fraction_done, &message);
		std::cout << fraction_done << " " << message;

		{
			std::lock_guard<std::mutex> lock(m_Mutex);
			m_fraction_done = fraction_done;
			m_message = message;
		}

		if (emit_view == true ) {
			std::cout << "emit view\n";
			worker_notify();
		}
		if (m_shall_stop) {
			model->stop_work(); // tell async thread to stop earlier
			m_message += "Stopped";
			break;
		}

	} while (fraction_done < 1.0);
	m_personList = result.get(); // wait her for async to finish

    {
        std::lock_guard<std::mutex> lock(m_Mutex);
        m_shall_stop = false;
        m_has_stopped = true;
//        if (db.unableRead()) {
//        	m_dberrors = db.getErrors();
//            std::cout << " unable read" << std::endl;
//        }
    }

    std::cout << "Finished DB Read\n";
    if (emit_view == true ) {
        std::cout << "emit view\n";
        worker_notify();
    } else {
        std::cout << "Not emit view\n";

    }
}

} /* namespace Module */

