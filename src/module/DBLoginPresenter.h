/*
 * DBLoginPresenter.h
 */

#ifndef DBPERSONPRESENTER_H_
#define DBPERSONPRESENTER_H_

#include "AbstractPresenter.h"
#include "storage/DBStorage.h"

namespace Module
{

class DBLoginWindowInterface;
class PersonalModel;
class Controller;

/// DBLogin presenter
class DBLoginPresenter: public AbstractPresenter
{
    DBLoginWindowInterface *const view;
    PersonalModel *const model;

public:
    /// Constructor
    /**
     * \param viewInterface const pointer of view interface
     * \param controller pointer of module controller
     */
    DBLoginPresenter ( DBLoginWindowInterface *const viewInterface,  Module::Controller *controller );
    /// Virtual deconstructor
    virtual ~DBLoginPresenter();
    /// initialize the presenter and set the view
    /**
     * To edit and to be able to remove the person, its module controller source model
     * index is required.
     */
    void initialize();
    /// Login user
    void loginUser();
    /// Logout user
    void logoutUser();
    
protected:
    /// Load the message to setup for presenter
    /**
     * To edit and to be able to remove the person, its module controller source model
     * index is required.
     * A message does this by sending basic information on what to do with module source data
     * such as loading an item from a specific position in the data source
     * \param message const reference to message
     * \sa Module::Message()
     */
    virtual void loadMessage ( const Module::Message &message );

};

} /* namespace Module */

#endif /* DBPERSONPRESENTER_H_ */
