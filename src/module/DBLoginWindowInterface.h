/*
 * DBLoginWindowInterface.h
 */

#ifndef DBLOGINWINDOWINTERFACE_H_
#define DBLOGINWINDOWINTERFACE_H_

#include "View.h"
#include "ViewList.h"
#include "ViewBindList.h"
#include "ViewItem.h"
#include "validate/ValidationError.h"
#include "Entity.h"

namespace Module
{

/// View interface which should be implemented to use the DBLogin presenter
/**
 * \sa Module::DBLoginPresenter()
 */
class DBLoginWindowInterface: public View
{
public:
    /// Constructor
    DBLoginWindowInterface() : View ( ViewIdentity::LoginUser ) {};
    /// Virtual deconstructor
    virtual ~DBLoginWindowInterface() {};
    /// Interface method to get a Username
    /**
     * \return ViewItem<std::string>
     */
    virtual ViewItem<std::string> &getUsername() = 0;
    /// Interface method to get a Username
    /**
     * \return ViewItem<std::string>
     */
    virtual ViewItem<std::string> &getPassword() = 0;
    /// WHat role is user
    /**
     * \return ViewItem of User role enum
     * \sa Module::UserRole
     */
    virtual ViewItem<Module::UserRole> &getRole () = 0;
    /// get state of presenter with logged user or not
    /**
     * \return ViewItem of logged state enum
     * \sa Module:::LoggedState
     */
    virtual ViewItem<Module::LoggedState> &getLoggedState () = 0;
    /// Interface method to get the view validation errors ViewList
    /**
     * \return ViewList<Validate::ValidationError>
     */
    virtual ViewList<Validate::ValidationError> &getValidationError() = 0;
    /// Interface method to read the edit control username password and role
    virtual void readEditControl() = 0;
};

} /* namespace Module */

#endif /* DBLOGINWINDOWINTERFACE_H_ */
