/*
 * PersonPresenter.cpp
 */

#include "PersonPresenter.h"
#include <iostream>
#include <sstream>
#include <memory>
#include <algorithm>
#include "storage/DBStorage.h"
#include "CommandAddPerson.h"
#include "CommandRemovePerson.h"
#include "CommandChangePerson.h"
#include "Controller.h"
#include "PersonWindowInterface.h"
#include "Person.h"
#include "PersonValidate.h"
#include "PersonalModel.h"
#include "Message.h"

namespace Module
{

///////////////////////////////////////////////////////////////////////////////
// Public constructor
///////////////////////////////////////////////////////////////////////////////
PersonPresenter::PersonPresenter(PersonWindowInterface *const viewInterface, Module::Controller *controller) :
    Module::AbstractPresenter(controller),
    view(viewInterface), model(ctrl()->personalModel())
{
}

PersonPresenter::~PersonPresenter()
{
}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
void PersonPresenter::initialize()
{
    view->modulePresenter(this);
    Module::ViewItem<std::string> &viewStatus = view->getStatus();
    Module::ViewItem<Module::Person> &viewPerson = view->getPerson();

    viewPerson.setItem(Module::Person());
    view->getValidationError().clear();
    viewStatus.setItem("Ready");

}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
void PersonPresenter::loadMessage(const Module::Message &message)
{
    // Process the message to setup the view to show a spefic record of data
    // and to store what position is is at in the ctrl data source
    // Also info on a new item or to change an item
    //std::cout << "PersonPresenter Message from ctrl " << message.argumments().twoArgs.valueA << " " << message.argumments().twoArgs.valueB              << std::endl;
    switch (message.kind()) {
    case Module::ParameterKind::FIELD_ARG:
        // Setup view for edit a person using the message Field Arguments as requested values
    	for (Storage::FieldDetail fd : message.fieldArgumments()) {
    		if (fd.field_name.compare("PersonID") == 0) {
    			m_originalPerson.setPersonID(fd.int_value);
    		} else if (fd.field_name.compare("Firstname") == 0) {
    			m_originalPerson.setFirstname(fd.str_value);
    		} else if (fd.field_name.compare("Surname") == 0) {
    			m_originalPerson.setSurname(fd.str_value);
    		}
    	}
        view->getPerson().setItem(m_originalPerson);
        view->getState().setItem(Module::ViewEditState::Change);
        break;
    default:
        // Otherwise setup for add an item
        view->getPerson().setItem(Module::Person());
        view->getState().setItem(Module::ViewEditState::AddNew);
        break;
    }
    view->getValidationError().clear();
    view->getStatus().setItem("Ready");
}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
void PersonPresenter::addNewPerson()
{
	view->readEditControl();
	auto editPerson = view->getPerson().getItem();
	view->getValidationError().clear();
	auto errors = model->validatePerson(editPerson);
	if (!errors.empty()) {
		for (auto ve : errors) {
			view->getValidationError().add(ve);
		}
	} else {
		try {
			auto cmd = std::make_shared<CommandAddPerson>(model, editPerson);
			ctrl()->commandController.executeCmd(cmd);
			view->getPerson().setItem(cmd->getPerson());
			view->getStatus().setItem("Added Person");
			ctrl()->close(Module::ViewIdentity::PersonEdit);
		} catch (ModuleException& e) {
			view->getStatus().setItem(e.what());

		}
	}
}

//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
void PersonPresenter::changePerson()
{
    view->readEditControl(); //Make sure view has updated personitem source
    auto changePerson = view->getPerson().getItem();
    view->getValidationError().clear();
	auto errors = model->validatePerson(changePerson);
	if (!errors.empty()) {
		for (auto ve : errors) {
			view->getValidationError().add(ve);
		}
	} else {
		try {
			auto cmd = std::make_shared<CommandChangePerson>(model, m_originalPerson, changePerson);
			ctrl()->commandController.executeCmd(cmd);
			view->getStatus().setItem("Changed Person");
			ctrl()->close(Module::ViewIdentity::PersonEdit);
		} catch (std::exception& e) {
			view->getStatus().setItem(e.what());
		}
    }
}

//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
void PersonPresenter::removePerson()
{
	try {
		ctrl()->commandController.executeCmd(std::make_shared<CommandRemovePerson>(model, m_originalPerson));
		view->getStatus().setItem("Removed Person");
        view->getState().setItem(Module::ViewEditState::Removed);
		ctrl()->close(Module::ViewIdentity::PersonEdit);
	} catch (std::exception& e) {
		view->getStatus().setItem(e.what());
	}
}


} /* namespace Module */

