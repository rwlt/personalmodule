/*
 * CommandAddPerson.h
 */

#ifndef COMMANDADDPERSON_H_
#define COMMANDADDPERSON_H_

#include "Command.h"
#include "PersonalModel.h"

namespace Module
{

/// Command Add Person
class CommandAddPerson : public Command
{
    PersonalModel* const m_model;
    Person m_person;

public:
    /// Constructor
    /**
     * \param model PersonalModel pointer
     * \param person person object
     */
    CommandAddPerson ( PersonalModel* const model, Person person ) :
         m_model(model), m_person(person) {}

    /// redo
    void redo()       {
        if (m_model->loggedUser()) {
			auto result = m_model->addNewPerson(m_person);
			m_person = result;
        }
    }

    /// undo
    void undo()       {
        if (m_model->loggedUser()) {
			m_model->removePerson(m_person);
        }
    }
    /// getPerson
    /**
     * Person added to DB
     * \return Module::Person const
     */
    const Module::Person &getPerson() const {
    	return m_person;
    }
};


} /* namespace Module */

#endif /* COMMANDADDPERSON_H_ */

