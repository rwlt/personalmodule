#ifndef VIEWITEM_H_
#define VIEWITEM_H_

namespace Module
{
/// ViewItem<DataType>: Used to move an item to the view
template<typename DataType>
class ViewItem
{
public:
    /// Virtual deconstructor
    virtual ~ViewItem() { }
    /// Set item
    /**
     * The item is bind to the view component view model
     * \param item DataType value of item
     */
    virtual void setItem ( const DataType &item ) = 0;
    /// Get item
    /**
     * The item is bind to the view component view model
     * \return reference DataType value of item
     */
    virtual DataType &getItem() = 0;

};

}
#endif /* VIEWITEM_H_ */
