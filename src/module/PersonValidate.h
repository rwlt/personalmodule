/*
 * PersonValidate.h
 */

#ifndef D_PERSONVALIDATE_H_
#define D_PERSONVALIDATE_H_

#include "validate/Validator.h"
#include "validate/ValidationError.h"
#include "Person.h"
#include <vector>
#include <string>

namespace Validate
{
/// Person Validate 
/** Person validation 
 * \ingroup Validation
 * 
 */
class PersonValidate : public ValidatorBase<Module::Person>
{
    Module::Person m_person;

public:
    /// Constructor with a module data object
    /**
     * This validator object manages the logic of person validation
     * In future to able be extended with xml configuration file to declare the validation rules.
     * \param person const reference to person
     * \param xml const char xml
     */
    PersonValidate ( const Module::Person &person, const char * xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
                     "<module-validation>\n"
                     "<object name=\"Module::Person\">\n"
                     "  <property-validations>\n"
                     "    <property name=\"Firstname\">\n"
                     "      <rule required=\"yes\"/>\n"
                     "      <rule maxlength=\"40\"/>\n"
                     "    </property>\n"
                     "    <property name=\"Surname\">\n"
                     "      <rule required=\"yes\"></rule>\n"
                     "      <rule maxlength=\"40\"></rule>\n"
                     "    </property>\n"
                     "  </property-validations>\n"
                     "  <constraints>\n"
                     "    <constraint property=\"Firstname\"/>\n"
                     "  </constraints>\n"
                     "</object>\n"
                     "</module-validation>\n" ) :
                    	     ValidatorBase(person, xml), m_person(person) {};
    /// Virtual deconstructor
    virtual ~PersonValidate() {};


};

} /* namespace Validate */

#endif /* D_PERSONVALIDATE_H_ */
