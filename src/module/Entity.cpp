/*
 * Entity.cpp
 *
 *  Created on: 15/07/2017
 *      Author: rodney
 */


#include "Entity.h"

STORENTMAP_INIT(Person, PersonID, Module::Person)
STORENTMAP_INIT(Hero, HeroID, Module::Hero)
STORENTMAP_INIT(Statute, StatuteID, Module::Statute)
STORENTMAP_INIT(Chest, ChestID, Module::Chest)
STORENTMAP_INIT(ChestItems, ChestItemID, Module::ChestItems)
STORENTMAP_INIT(Inventory, InventoryID, Module::Inventory)

STORENTMAP_INIT(ValidationError, ID, Validate::ValidationError)





