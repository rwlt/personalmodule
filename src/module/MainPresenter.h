/*
 * MainPresenter.h
 */

#ifndef MAINPRESENTER_H_
#define MAINPRESENTER_H_

#include "AbstractPresenter.h"
#include "Person.h"
#include <string>
#include <vector>
#include <thread>
#include <mutex>
#include <future>

namespace Module
{


class MainWindowInterface;
class PersonalModel;
class Controller;

/// Main presenter
class MainPresenter: public AbstractPresenter
{
    MainWindowInterface *const view;
    PersonalModel *const model;
    std::vector<Module::Person> m_personList{};
    std::vector<std::string> m_dberrors{};

    std::thread m_worker_thread{};
    mutable std::mutex m_Mutex{};
    bool m_shall_stop{false};
    bool m_has_stopped{true};
    // Data used by both GUI thread and worker thread.
    double m_fraction_done{0.0};
    std::string m_message{""};

public:
    /// Constructor
    /**
     * \param viewInterface const pointer of view interface
     * \param controller pointer of module controller
     */
    MainPresenter ( MainWindowInterface *const viewInterface,  Module::Controller *controller );
    /// Virtual deconstructor
    virtual ~MainPresenter();
    /// initialize the presenter and set the view
    void initialize();

    /// Login user view
    void loginUser();
    /// Add New Person view
    void addNewPerson();
    /// Change Person view
    void changePerson();
    /// Remove a person
    void removePerson();
    /// Undo
    void undo();
    /// Redo
    void redo();
    // Worker has stopped
    /**
     * \return bool
     */
    bool worker_has_stopped();
    /// Get data from worker when worker_notify is called to View
    /**
     * \param fraction_done pointer to double
     * \param message string
     */
    void get_data ( double *fraction_done, std::string *message );
    /// Flags a process to stop
    void stop_work();
    /// complete db worker
    void complete_dbworker();

protected:
    /// Load the message to setup for presenter
    /**
     * To edit and to be able to remove the person, its module controller source model
     * index is required.
     * A message does this by sending basic information on what to do with module source data
     * such as loading an item from a specific position in the data source
     * \param message const reference to message
     * \sa Module::Message()
     */
    virtual void loadMessage ( const Module::Message &message ) override;

private:
    MainPresenter ( const MainPresenter & );            //Use default copy c'tor and rvalue
    MainPresenter &operator= ( const MainPresenter & ); //Use default = operator and rvalue
    /// start work
    void start_work (bool emit_view);
    /// worker notify
    void worker_notify();
    /// join completed work (or detach)
    void join_detach();
    // Thread function.
    void do_work (bool emit_view);

};

} /* namespace Module */

#endif /* MAINPRESENTER_H_ */
