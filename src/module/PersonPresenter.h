/*
 * PersonPresenter.h
 */

#ifndef PERSONPRESENTER_H_
#define PERSONPRESENTER_H_

#include "AbstractPresenter.h"
#include "Person.h"

namespace Module
{

class PersonWindowInterface;
class PersonalModel;
class Controller;

/// Person presenter
class PersonPresenter: public AbstractPresenter
{
    PersonWindowInterface *const view;
    PersonalModel *const model;
	Module::Person m_originalPerson{};

public:
    /// Constructor
    /**
     * \param viewInterface const pointer of view interface
     * \param controller pointer of module controller
     */
    PersonPresenter ( PersonWindowInterface *const viewInterface,  Module::Controller *controller );
    /// Virtual deconstructor
    virtual ~PersonPresenter();
    //    PersonPresenter ( const PersonPresenter & );            Use default copy c'tor and rvalue
    //    PersonPresenter &operator= ( const PersonPresenter & ); Use default = operator and rvalue
    /// initialize the presenter and set the view
    /**
     * To edit and to be able to remove the person, its module controller source model
     * index is required.
     */
    void initialize();

    /// Add new person
    void addNewPerson();
    /// Change person
    void changePerson();
    /// Remove person
    void removePerson();

protected:
    /// Load the message to setup for presenter
    /**
     * To edit and to be able to remove the person, its module controller source model
     * index is required.
     * A message does this by sending basic information on what to do with module source data
     * such as loading an item from a specific position in the data source
     * \param message const reference to message
     * \sa Module::Message()
     */
    virtual void loadMessage ( const Module::Message &message );
    
};

} /* namespace Module */

#endif /* PERSONPRESENTER_H_ */
