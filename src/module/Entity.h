/*
 * Entity.h
 */

#ifndef MODULE_ENTITY_H_
#define MODULE_ENTITY_H_

#include "storage/EntityMap.h"
#include "storage/DBStorage.h"
#include "Chest.h"
#include "Person.h"
#include "Hero.h"
#include "validate/ValidationError.h"
#include <string>


namespace Storage {


/// EntityMap ChestItems
STORENTMAP_DECLARE_BEGIN(ChestItems, Module::ChestItems)
STOREENTMAP_FIELDS_BEGIN
STOREENTMAP_FIELDS_DECLARE(ChestItemID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(InventoryID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(ChestID, FieldType::INTEGER)
STOREENTMAP_FIELDS_END
STOREENTMAP_RELATIONS_BEGIN
STORERELMAP_RELATIONS_DECLARE(Inventory, RelationType::ONETOONE)
STOREENTMAP_RELATIONS_END
STOREENTMAP_FIELDS_GET_BEGIN
GET_INT_ACCESSOR(ChestItemID, getChestItemID)
GET_INT_ACCESSOR(InventoryID, getInventoryID)
GET_INT_ACCESSOR(ChestID, getChestID)
STOREENTMAP_FIELDS_GET_END
STOREENTMAP_FIELDS_SET_BEGIN
SET_INT_ACCESSOR(ChestItemID, setChestItemID)
SET_INT_ACCESSOR(InventoryID, setInventoryID)
SET_INT_ACCESSOR(ChestID, setChestID)
STOREENTMAP_FIELDS_SET_END
STOREENTMAP_RELATIONS_DETAIL_BEGIN
RELATION_ONE2ONE(Inventory, FieldType::INTEGER, InventoryID, FieldType::INTEGER, InventoryID, InventoryID, InventoryID)
STOREENTMAP_RELATIONS_DETAIL_END
STOREENTMAP_RELATIONS_GET_EMPTY
STOREENTMAP_RELATIONS_SET_BEGIN
SET_ONE2ONE_ACCESSOR(Inventory, Module::Inventory, setInventory)
STOREENTMAP_RELATIONS_SET_END
STORENTMAP_DECLARE_END

/// EntityMap Inventory
STORENTMAP_DECLARE_BEGIN(Inventory, Module::Inventory)
STOREENTMAP_FIELDS_BEGIN
STOREENTMAP_FIELDS_DECLARE(InventoryID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(Quota, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(Name, FieldType::STRING)
STOREENTMAP_FIELDS_END
STOREENTMAP_RELATIONS_BEGIN
STOREENTMAP_RELATIONS_END
STOREENTMAP_FIELDS_GET_BEGIN
GET_INT_ACCESSOR(InventoryID, getInventoryID)
GET_INT_ACCESSOR(Quota, getQuota)
GET_STRING_ACCESSOR(Name, getName)
STOREENTMAP_FIELDS_GET_END
STOREENTMAP_FIELDS_SET_BEGIN
SET_INT_ACCESSOR(InventoryID, setInventoryID)
SET_INT_ACCESSOR(Quota, setQuota)
SET_STRING_ACCESSOR(Name, setName)
STOREENTMAP_FIELDS_SET_END
STOREENTMAP_RELATIONS_DETAIL_EMPTY
STOREENTMAP_RELATIONS_GET_EMPTY
STOREENTMAP_RELATIONS_SET_EMPTY
STORENTMAP_DECLARE_END

/// EntityMap Chest
STORENTMAP_DECLARE_BEGIN(Chest, Module::Chest)
STOREENTMAP_FIELDS_BEGIN
STOREENTMAP_FIELDS_DECLARE(ChestID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(PersonID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(Name, FieldType::STRING)
STOREENTMAP_FIELDS_END
STOREENTMAP_RELATIONS_BEGIN
STORERELMAP_RELATIONS_DECLARE(ChestItems, RelationType::ONETOMANY)
STOREENTMAP_RELATIONS_END
STOREENTMAP_FIELDS_GET_BEGIN
GET_INT_ACCESSOR(ChestID, getChestID)
GET_INT_ACCESSOR(PersonID, getPersonID)
GET_STRING_ACCESSOR(ID, getName)
STOREENTMAP_FIELDS_GET_END
STOREENTMAP_FIELDS_SET_BEGIN
SET_INT_ACCESSOR(ChestID, setChestID)
SET_INT_ACCESSOR(PersonID, setPersonID)
SET_STRING_ACCESSOR(Name, setName)
STOREENTMAP_FIELDS_SET_END
STOREENTMAP_RELATIONS_DETAIL_BEGIN
RELATION_ONE2MANY(ChestItems, FieldType::INTEGER, ChestID, FieldType::INTEGER, ChestID, ChestItemID, ChestID)
STOREENTMAP_RELATIONS_DETAIL_END
STOREENTMAP_RELATIONS_GET_EMPTY
STOREENTMAP_RELATIONS_SET_BEGIN
SET_ONE2MANY_ACCESSOR(ChestItems, Module::ChestItems, addChestItem)
STOREENTMAP_RELATIONS_SET_END
STORENTMAP_DECLARE_END


/// Map type definition of queue field value methods
/**
 * Type def for entity person
 */
/// EntityMap Person
STORENTMAP_DECLARE_BEGIN(Person, Module::Person)
STOREENTMAP_FIELDS_BEGIN
STOREENTMAP_FIELDS_DECLARE(PersonID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(Firstname, FieldType::STRING)
STOREENTMAP_FIELDS_DECLARE(Surname, FieldType::STRING)
STOREENTMAP_FIELDS_DECLARE(ChestID, FieldType::INTEGER)
STOREENTMAP_FIELDS_END
STOREENTMAP_RELATIONS_BEGIN
STORERELMAP_RELATIONS_DECLARE(Chest, RelationType::ONETOONE)
STOREENTMAP_RELATIONS_END
STOREENTMAP_FIELDS_GET_BEGIN
GET_INT_ACCESSOR(PersonID, getPersonID)
GET_STRING_ACCESSOR(Firstname, getFirstname)
GET_STRING_ACCESSOR(Surname, getSurname)
STOREENTMAP_FIELDS_GET_END
STOREENTMAP_FIELDS_SET_BEGIN
SET_INT_ACCESSOR(PersonID, setPersonID)
SET_STRING_ACCESSOR(Firstname, setFirstname)
SET_STRING_ACCESSOR(Surname, setSurname)
SET_INT_ACCESSOR(ChestID, setChestID)
STOREENTMAP_FIELDS_SET_END
STOREENTMAP_RELATIONS_DETAIL_BEGIN
RELATION_ONE2ONE(Chest, FieldType::INTEGER, PersonID, FieldType::INTEGER, ChestID, ChestID, PersonID)
STOREENTMAP_RELATIONS_DETAIL_END
STOREENTMAP_RELATIONS_GET_EMPTY
STOREENTMAP_RELATIONS_SET_BEGIN
SET_ONE2ONE_ACCESSOR(Chest, Module::Chest, setChest)
STOREENTMAP_RELATIONS_SET_END
STORENTMAP_DECLARE_END

/// Map type definition of queue field value methods
/**
 * Type def for entity hero
 */
STORENTMAP_DECLARE_BEGIN(Hero, Module::Hero)

STOREENTMAP_FIELDS_BEGIN
STOREENTMAP_FIELDS_DECLARE(HeroID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(StatuteID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(PersonID, FieldType::INTEGER)
STOREENTMAP_FIELDS_END

STOREENTMAP_RELATIONS_BEGIN
STOREENTMAP_RELATIONS_END

STOREENTMAP_FIELDS_GET_BEGIN
GET_INT_ACCESSOR(HeroID, getHeroID)
GET_INT_ACCESSOR(StatuteID, getStatuteID)
GET_INT_ACCESSOR(PersonID, getPersonID)
STOREENTMAP_FIELDS_GET_END

STOREENTMAP_FIELDS_SET_BEGIN
SET_INT_ACCESSOR(HeroID, setHeroID)
SET_INT_ACCESSOR(StatuteID, setStatuteID)
SET_INT_ACCESSOR(PersonID, setPersonID)
STOREENTMAP_FIELDS_SET_END

STOREENTMAP_RELATIONS_DETAIL_EMPTY
STOREENTMAP_RELATIONS_GET_EMPTY
STOREENTMAP_RELATIONS_SET_EMPTY

STORENTMAP_DECLARE_END


/// EntityMap Statue
STORENTMAP_DECLARE_BEGIN(Statute, Module::Statute)
STOREENTMAP_FIELDS_BEGIN
STOREENTMAP_FIELDS_DECLARE(ID, FieldType::INTEGER)
STOREENTMAP_FIELDS_DECLARE(Title, FieldType::STRING)
STOREENTMAP_FIELDS_END
STOREENTMAP_RELATIONS_BEGIN
STOREENTMAP_RELATIONS_END
STOREENTMAP_FIELDS_GET_BEGIN
GET_INT_ACCESSOR(ID, getID)
GET_STRING_ACCESSOR(Title, getTitle)
STOREENTMAP_FIELDS_GET_END
STOREENTMAP_FIELDS_SET_BEGIN
SET_INT_ACCESSOR(ID, setID)
SET_STRING_ACCESSOR(Title, setTitle)
STOREENTMAP_FIELDS_SET_END
STOREENTMAP_RELATIONS_DETAIL_EMPTY
STOREENTMAP_RELATIONS_GET_EMPTY
STOREENTMAP_RELATIONS_SET_EMPTY
STORENTMAP_DECLARE_END


/// Entity Map ValidationError
STORENTMAP_DECLARE_BEGIN(ValidationError, Validate::ValidationError)
STOREENTMAP_FIELDS_BEGIN
STOREENTMAP_FIELDS_DECLARE(fieldname, FieldType::STRING)
STOREENTMAP_FIELDS_DECLARE(errormessage, FieldType::STRING)
STOREENTMAP_FIELDS_END
STOREENTMAP_RELATIONS_BEGIN
STOREENTMAP_RELATIONS_END
STOREENTMAP_FIELDS_GET_BEGIN
GET_STRING_ACCESSOR(fieldname, getFieldName)
GET_STRING_ACCESSOR(errormessage, getErrorMessage)
STOREENTMAP_FIELDS_GET_END
STOREENTMAP_FIELDS_SET_BEGIN
SET_STRING_ACCESSOR(fieldname, setUIFieldName)
STOREENTMAP_FIELDS_SET_END
STOREENTMAP_RELATIONS_DETAIL_EMPTY
STOREENTMAP_RELATIONS_GET_EMPTY
STOREENTMAP_RELATIONS_SET_EMPTY
STORENTMAP_DECLARE_END


} /* namespace Storage */


#endif /* MODULE_ENTITY_H_ */
