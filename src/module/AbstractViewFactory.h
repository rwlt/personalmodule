/*
 * AbstractViewFactory.h
 */

#ifndef ABSTRACTVIEWFACTORY_H_
#define ABSTRACTVIEWFACTORY_H_

#include "ViewIdentity.h"

namespace Module
{
    
class Controller;
class MainWindowInterface;
class PersonWindowInterface;
class DBLoginWindowInterface;

/// Abstract View Factory for creating Views for module presenters
class AbstractViewFactory
{
public:
    /// Constructor
    AbstractViewFactory();
    /// Virtual deconstructor
    virtual ~AbstractViewFactory();
    /// Create instance of the Module Presenter View
    /**
     * Create the View interfaces used by the presenter to be able to load the data from presenter.
     *
     * The main view will always have parent view as empty so the main view is known. Can only be one
     * main view from a Module presenter.
     *
     * \param controller Module controller pointer reference
     * \param viewIdentity Module presenter view identityInstance
     * \param parentViewIdentity A parent view identity (default is empty parent)
     */
    virtual void createInstance ( Module::Controller* controller,
        Module::ViewIdentity viewIdentity,
        Module::ViewIdentity parentViewIdentity = Module::ViewIdentity::NullView ) = 0;
    /// Destroy instance of a Module Presenter View
    /**
     * \param viewIdentity Module presenter view identityInstance
     */
    virtual void destroyInstance ( Module::ViewIdentity viewIdentity ) = 0;
    /// Retrieve instance of the Module Presenter View interface
    /**
     * \return pointer to Module::MainWindowInterface()
     */
    virtual MainWindowInterface *mainInterface () = 0;
    /// Retrieve instance of the Module Presenter View interface
    /**
     * \return pointer to Module::PersonWindowInterface()
     */
    virtual PersonWindowInterface *personInterface () = 0;
    /// Retrieve instance of the Module DB Login View interface
    /**
     * \return pointer to Module::DBLoginWindowInterface()
     */
    virtual DBLoginWindowInterface *loginInterface () = 0;
    
};

}
#endif /* ABSTRACTVIEWFACTORY_H_ */
