/*
 * AbstractPresenter.cpp
 */

#include "AbstractPresenter.h"

namespace Module
{

AbstractPresenter::AbstractPresenter(Module::Controller *controller) : m_ctrl(controller)
{
}

AbstractPresenter::~AbstractPresenter()
{
}

} /* namespace Module */
