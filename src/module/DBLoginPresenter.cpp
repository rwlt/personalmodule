/*
 * DBLoginPresenter.cpp
 */

#include "DBLoginPresenter.h"
#include <iostream>
#include <sstream>
#include <memory>
#include <algorithm>
#include "Controller.h"
#include "DBLoginWindowInterface.h"
#include "PersonalModel.h"
#include "ModuleException.h"
#include "Message.h"

namespace Module {

    ///////////////////////////////////////////////////////////////////////////////
    // Public constructor
    ///////////////////////////////////////////////////////////////////////////////

    DBLoginPresenter::DBLoginPresenter(DBLoginWindowInterface * const viewInterface, Module::Controller *controller) :
           Module::AbstractPresenter(controller),
		   view(viewInterface), model(ctrl()->personalModel())
    {
    }

    DBLoginPresenter::~DBLoginPresenter() {
    }
    //
    ///////////////////////////////////////////////////////////////////////////////
    // public function
    ///////////////////////////////////////////////////////////////////////////////
    //

    void DBLoginPresenter::initialize() {
        view->modulePresenter(this);
    }
    //
    ///////////////////////////////////////////////////////////////////////////////
    // public function
    ///////////////////////////////////////////////////////////////////////////////
    //

    void DBLoginPresenter::loadMessage(const Module::Message &message) {
        switch (message.kind()) {
            case Module::ParameterKind::TWO_ARG:
                break;
            default:
                // If model has login details then the user needs to be logged out to enter
                // new user - we setup the view for this logic
                view->getUsername().setItem("");
                view->getPassword().setItem("");
                view->getRole().setItem(Module::UserRole::GUEST);
                if (model->loggedUser()) {
                    Storage::DBStorage dbStorage(model->getDBVersion(), model->getDBConfigXml().c_str());
                    dbStorage.loginUser(model->getUserName(), model->getPassword(), model->getUserRole());
                    if (dbStorage.isLoggedUser()) {
                        view->getUsername().setItem(model->getUserName());
                        view->getPassword().setItem(model->getPassword());
                        if (model->getUserRole() == "RDB$ADMIN") { // Most times it will be GUEST
                            view->getRole().setItem(Module::UserRole::ADMIN);
                        }
                        view->getLoggedState().setItem(Module::LoggedState::LOGGED);
                    } else {
                        view->getLoggedState().setItem(Module::LoggedState::NOTLOGGED);
                    }
                } else {
                    view->getLoggedState().setItem(Module::LoggedState::NOTLOGGED);
                }
                view->getValidationError().clear();
                break;
        }
    }
    //
    ///////////////////////////////////////////////////////////////////////////////
    // Public method
    ///////////////////////////////////////////////////////////////////////////////
    //
    void DBLoginPresenter::loginUser()
    {
        if (view == nullptr) {
            return;
        }
       view->readEditControl(); //Make sure view has updated personitem source
       auto username = view->getUsername().getItem();
       auto password = view->getPassword().getItem();
       auto roletype = view->getRole().getItem();
       view->getValidationError().clear();
       try {
           // Try the login
		   auto logresult = model->loginUser(username, password, roletype);
		   bool logged;
		   std::string message;
		   std::tie(logged, message) = logresult;
		   if (logged) {
			   view->getLoggedState().setItem(Module::LoggedState::LOGGED);
			   ctrl()->close(Module::ViewIdentity::LoginUser);
		   } else {
			   Validate::ValidationError log_error("username", message);
			   view->getValidationError().add(log_error);
		   }
       } catch (Module::ModuleException& e) {
		   Validate::ValidationError log_error("username", e.what());
		   view->getValidationError().add(log_error);
       }

    }

    //
    ///////////////////////////////////////////////////////////////////////////////
    // public function
    ///////////////////////////////////////////////////////////////////////////////
    //

    void DBLoginPresenter::logoutUser() {
 	    model->logoutUser();
        view->getLoggedState().setItem(Module::LoggedState::NOTLOGGED);
        ctrl()->close(Module::ViewIdentity::LoginUser);
    }


} /* namespace Module */

