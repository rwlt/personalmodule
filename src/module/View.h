/*
 * View.h
 */

#ifndef VIEW_H_
#define VIEW_H_

#include "ViewIdentity.h"

namespace Module
{

class Message;
class AbstractPresenter;

/// View abstraction class
/* The presenter uses a view interface and this is the base pure virtual
 * for all views used by the presenter models.
 */
class View
{
public:
    /// Contructor
    /**
     * \param identity give the view an identity 
     * \param emit_view should view get sent to dispatch thread emit
     * \sa Module::ViewIdentity()
     */
    View ( Module::ViewIdentity identity, bool emit_view = false );
    /// Virutal decontructor
    virtual ~View();
    /// Show the view 
    /**
     * \param message const reference to message
     * \sa Module::Message()
     */
    virtual void showView ( const Message &message ) = 0;
    /// Close the view 
    /**
     */
    virtual void closeView () = 0;
    /// get the identity of this view
    /**
     * \return the identiy of this view
     * \sa Module::ViewIdentity()
     */
    ViewIdentity moduleViewIdentity();
    /// Notify from a worker thread 
    /**
     */
    virtual void worker_notify() = 0;

private:

    void modulePresenter(AbstractPresenter* abstract_presenter);
    AbstractPresenter* m_modulePresenter{};
    const ViewIdentity m_viewIdentity;
    bool m_emit_view;

    friend class Controller;
    friend class MainPresenter;
    friend class PersonPresenter;
    friend class DBLoginPresenter;
    
};
}

#endif /* VIEW_H_ */
