/*
 * CommandChangePerson.h
 */

#ifndef COMMANDCHANGEPERSON_H_
#define COMMANDCHANGEPERSON_H_

#include "Command.h"
#include "PersonalModel.h"
#include "Person.h"
#include "storage/DBStorage.h"
#include "ModuleException.h"

namespace Module
{
/// Command Change Person
class CommandChangePerson : public Command
{
    PersonalModel* const m_model;
    Person m_orig_person;
    Person m_changed_person;

public:
    /// Constructor
    /**
     * \param model PersonaModel pointer
     * \param orig_person person before change
     * \param changed_person person changed
     */
    CommandChangePerson (   PersonalModel* const model, Person orig_person, Person changed_person ) :
    	m_model(model), m_orig_person ( orig_person ), m_changed_person ( changed_person ) {}

    /// redo
    void redo()       {
        if (m_model->loggedUser()) {
			auto result = m_model->changePerson(m_orig_person, m_changed_person);
			m_changed_person = result;
        }
    }
    /// undo
    void undo()       {
        if (m_model->loggedUser()) {
			auto result = m_model->changePerson(m_changed_person, m_orig_person);
			m_orig_person = result;
        }
    }

};


} /* namespace Module */

#endif /* COMMANDCHANGEPERSON_H_ */

