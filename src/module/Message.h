/*
 * Message.h
 */

#ifndef MESSAGE_H_
#define MESSAGE_H_

#include "storage/RowItem.h"
#include "ViewIdentity.h"
#include <vector>
#include <map>

namespace Module
{
/// Vector of FieldDetail
typedef std::vector<Storage::FieldDetail> FieldArgs;

/// Parameter kind
enum class ParameterKind : char {
    ONE_ARG = 0,    ///< One arg kind of parameter
    TWO_ARG = 1,    ///< Two arg kind of parameter
    THREE_ARG = 2,  ///< Three arg kind of parameter
    FIELD_ARG = 3   ///< Field map kind of parameter
};

/// One arg
struct OneArg {
    long message;    ///< long of message
};
/// Two arg
struct TwoArgs {
    unsigned int valueA; ///< value A
    unsigned int valueB; ///< value B
};
///// Three arg
//struct ThreeArgs {
//    unsigned int valueA; ///< value A
//    unsigned int valueB; ///< value B
//    unsigned int valueC; ///< value C
//};
/// Paramater
struct Parameter {
    ParameterKind kind{ParameterKind::ONE_ARG};      ///< kind of component message
    FieldArgs fieldArgs;         ///< Field args (No Args union is used for this parameter)
    /// Union of which args
    union Args {
        OneArg oneArg;        ///< command argument detail
        TwoArgs twoArgs;      ///< command argument detail
//        ThreeArgs threeArgs;  ///< command argument detail
    } args;                   ///< args value of parameter
};

/// Message
class Message
{
    Parameter m_parameter{};

public:
    /// Constructor
    Message();
    /// Virtual deconstructor
    virtual ~Message();
    /// Arguments used
    /**
     * \return parameter arguments component message
     */
    Parameter::Args argumments() const;
    /// Field Arguments used
    /**
     * \return parameter arguments component message
     */
    const FieldArgs &fieldArgumments() const;
    /// Kind of message
    /**
     * \return parameter kind of component message
     */
    ParameterKind  kind() const;
    /// Set Parameter
    /**
     * \param newParam paramter
     */
    void setParameter ( Parameter newParam );
};

} /* namespace Module */

#endif /* MESSAGE_H_ */
