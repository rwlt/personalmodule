/*
 * PersonalModel.cpp
 *
 *  Created on: 20/07/2017
 *      Author: rodney
 */

#include "PersonalModel.h"
#include "ModuleException.h"
#include "PersonValidate.h"
#include "Entity.h"

namespace Module {

//
///////////////////////////////////////////////////////////////////////////////
// Public method
///////////////////////////////////////////////////////////////////////////////
//
std::tuple<bool, std::string> PersonalModel::loginUser(const std::string username, const std::string password,
		const Module::UserRole &roletype) {
	if (m_hasLoggeduser) {
		throw(ModuleException("PersonalModule loginUser Has user logged in, use logout to login new user"));
	}
	bool logged { false };
	std::stringstream message;
	std::string role;
	switch (roletype) {
	case Module::UserRole::ADMIN:
		role = "RBD$ADMIN";
		break;
	default:
		role = "GUEST";
		break;
	}
	auto result = m_dbStorage.loginUser(username, password, role);
	if (m_dbStorage.isLoggedUser()) {
		m_UserName = username;
		m_UserPassword = password;
		m_UserRole = role;
		m_hasLoggeduser = true;
		logged = true;
		m_repoPerson.clear();// reset the repo for new user
	} else {
		Storage::DBCallError err = std::get<1>(result);
		message << " " << err.error;
	}
	return std::tuple<bool, std::string>(logged, message.str());

}

//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//

void PersonalModel::logoutUser() {
	// Clear the model user login detail
	setUserName("");
	setPassword("");
	setUserRole("");
	m_hasLoggeduser = false;
	m_repoPerson.clear();// reset the repo for new user

}

//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
std::vector<Validate::ValidationError> PersonalModel::validatePerson(const Module::Person &person) {
	Validate::PersonValidate person_validate(person);
	std::vector<Validate::ValidationError> validation_errors = person_validate.Validate();
	return validation_errors;
}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
Module::Person PersonalModel::addNewPerson(const Module::Person &addPerson) {
	if (!loggedUser()) {
		throw(ModuleException("PersonalModule addNewPerson no logged in user."));
	}
	auto errors = validatePerson(addPerson);
	if (!errors.empty()) {
		std::string message = validationMessage(errors);
		throw(ModuleException("PersonalModule addNewPerson " + message));
	}
    Module::Person addedPerson;
    if (m_repoPerson.isTrackingState()) {
    	// find if in tracked remove state
    	try {
			auto key = m_repoPerson.addItem(addPerson);
			addedPerson = m_repoPerson.getItem(key);
    	} catch (Repository::RepositoryException& re) {
    		throw(ModuleException("PersonalModel addNewPerson " + std::string(re.what())));
    	}
    } else {
    	addedPerson = m_dbStorage.doAddItem<Person>(addPerson);
    }
	for (auto func: this->m_broadcastPersonChange) {
		func(addedPerson, false);
	}
	return addedPerson;
}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
Module::Person PersonalModel::changePerson(const Module::Person &origPerson,
		const Module::Person &changePerson) {
	if (!loggedUser()) {
		throw(ModuleException("PersonalModule changePerson no logged in user."));
	}
	auto errors = validatePerson(changePerson);
	if (!errors.empty()) {
		std::string message = validationMessage(errors);
		throw(ModuleException("PersonalModule changePerson " + message));
	}
    Module::Person changedPerson;
    if (m_repoPerson.isTrackingState()) {
	    m_repoPerson.updateItem(changePerson.getPersonID(), changePerson);
		changedPerson = m_repoPerson.getItem(changePerson.getPersonID());
    } else {
    	changedPerson = m_dbStorage.doUpdateItem<Person>(origPerson, changePerson);
    }
	for (auto func: this->m_broadcastPersonChange) {
		func(changedPerson, false);
	}
	return changedPerson;
}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
void PersonalModel::removePerson(const Module::Person &removePerson) {
	Module::Person bcRemovePerson = removePerson;
	if (!loggedUser()) {
		throw(ModuleException("PersonalModule removePerson no logged in user."));
	}
    if (m_repoPerson.isTrackingState()) {
    	m_repoPerson.deleteItem(removePerson.getPersonID());
    } else {
    	m_dbStorage.doRemoveItem<Person>(removePerson);
    }
	for (auto func: this->m_broadcastPersonChange) {
		func(bcRemovePerson, true);
	}
}


//
/////////////////////////////////////////////////////////////////////////////
// private function
/////////////////////////////////////////////////////////////////////////////
//
std::string PersonalModel::validationMessage(const std::vector<Validate::ValidationError> &errors) {
	std::stringstream message;
	for (auto error : errors) {
		message << error.getFieldName() << " " << error.getErrorMessage() << "\n";
	}
	return message.str();
}


} /* namespace Module */
