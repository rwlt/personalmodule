/*
 * ModuleBind.h
 */

#ifndef MODULE_MODULEBIND_H_
#define MODULE_MODULEBIND_H_
#define CALL_T_MEMBER_FN(object,ptrToMember)  ((object)->*(ptrToMember))

#include "Person.h"

namespace Module
{

/// Module Bind base and clone
class ModuleBind
{
public:
    /// Constructor
    ModuleBind() {
    }
    /// Deconstructor
    virtual ~ModuleBind() {
    }
    /// draw
    virtual void draw() = 0;
    /// find and clone
    /**
     * \return ModuleBind static
     */
    static ModuleBind *findAndClone ( int );

protected:
    /// return type
    /**
     * \return int
     */
    virtual int returnType() = 0;
    /// clone
    /**
     * \return ModuleBind 
     */
    virtual ModuleBind *clone() = 0;
    /// As each subclass of ModuleBind is declared, it registers its prototype
    /**
     * \param image pointer to Module bind
     */
    static void addPrototype ( ModuleBind *image ) {
        _prototypes[_nextSlot++] = image;
    }
private:
    static ModuleBind *_prototypes[10];
    static int _nextSlot;

};
/// prototypes
ModuleBind *ModuleBind::_prototypes[];
/// int of next slot
int ModuleBind::_nextSlot;
/// Client calls this public static member function when it needs an instance
/// of an Image subclass
/**
 * \param type int value
 * \return pointer to ModuleBind
 */
ModuleBind *ModuleBind::findAndClone ( int type )
{
    for ( int i = 0; i < _nextSlot; i++ )
        if ( _prototypes[i]->returnType() == type ) {
            return ( _prototypes[i]->clone() );
        }
    return ( nullptr );
}

/// Person Bind
class PersonBind: public ModuleBind
{
public:
    ///@{
    typedef void ( Person::*SetFunctionPtr ) ( const std::string &value );
    typedef const std::string ( Person::*GetFunctionPtr ) ();
    ///@}
//	ModuleBind(GetFunctionPtr getFuncPtr, SetFunctionPtr setFuncPtr) :
//			getPtr(getFuncPtr), setPtr(setFuncPtr)

    /// return type
    /**
     * \return int of return type
     */
    int returnType() {
        return ( 1 );
    }
    /// draw
    void draw() {
        std::cout << "LandSatImage::draw " << _id << std::endl;
    }
    /// When clone() is called, call the one-argument ctor with a dummy arg
    /**
     * \return pointer to ModuleBind
     */
    ModuleBind *clone() {
        return ( new PersonBind ( 1 ) );
    }
protected:
    /// Constructor only only called from clone()
    /**
     * \param dummy int
     */
    PersonBind ( int dummy ) :_id ( _count++ ) {  }
    
private:
    static PersonBind _personBind;
    PersonBind() :_id ( 0 ) {
        addPrototype ( this );
    }

    int _id;
    static int _count;

    /// Private constructor
    PersonBind ( const PersonBind & );
    /// Private assignment
    PersonBind operator = ( const PersonBind & );

};

/// Register the subclass's prototype
PersonBind PersonBind::_personBind;
/// Initialize the "state" per instance mechanism
int PersonBind::_count = 1;

} /* namespace control */

#endif /* MODULE_MODULEBIND_H_ */
