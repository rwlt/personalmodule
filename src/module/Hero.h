/*
 * Hero.h
 */

#ifndef HERO_H_
#define HERO_H_

#include "storage/EntityMap.h"
#include <string>

namespace Module
{

/// Hero
class Hero
{
public:
    /// Constructor
    Hero() {};
    /// Deconstructor
    virtual ~Hero(){};

	ENTITY_INT_ATTRIBUTE(HeroID)
	ENTITY_INT_ATTRIBUTE(PersonID)
	ENTITY_INT_ATTRIBUTE(StatuteID)

    /// Equality operator
     /**
      * \param lhs left hand side
      * \param rhs right hand side
      * \return true if equal
      */
     friend bool operator==(const Hero &lhs, const Hero &rhs);
     /// Inequality operator
     /**
      * \param lhs left hand side
      * \param rhs right hand side
      * \return true if not equal
      */
     friend bool operator!=(const Hero &lhs, const Hero &rhs);

};

/// operator ==
/**
 * \param lhs const reference to Hero
 * \param rhs const reference to Hero
 * \return true if equal
 */
inline bool operator == ( const Hero &lhs, const Hero &rhs )
{
    return (lhs.m_HeroID == rhs.m_HeroID &&
            lhs.m_PersonID == rhs.m_PersonID &&
			lhs.m_StatuteID == rhs.m_StatuteID);
}

/// Equality operator
inline bool operator!=(const Hero &lhs, const Hero &rhs) {
    return !(lhs == rhs);
}

/// Statute
class Statute {
public:
	/// Constructor
	Statute()	{	}
	/// Constructor from person
	/**
	 * \param newid integer
	 * \param title const string
	 */
	Statute(int newid, const std::string &title):
	     m_ID(newid), m_Title(title)
	{
	}
	/// virtual deconstructor
	virtual ~Statute(){};

	ENTITY_INT_ATTRIBUTE(ID)
	ENTITY_STRING_ATTRIBUTE(Title)
};


} /* namespace Module */

#endif /* HERO_H_ */
