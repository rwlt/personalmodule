/*
 * MainWindowInterface.h
 */

#ifndef MAINWINDOWINTERFACE_H_
#define MAINWINDOWINTERFACE_H_

#include "View.h"
#include "ViewList.h"
#include "ViewBindList.h"
#include "ViewItem.h"

namespace Module
{

class Person;
class Hero;
class MainPresenter;

/// Main view interface which should be implemented to use the main presenter
/**
 * \sa Module::MainPresenter()
 */
class MainWindowInterface: public View
{
public:
    /// Constructor
    /**
     * @param emit_view
     */
    MainWindowInterface(bool emit_view);
    /// Virtual deconstructor
    virtual ~MainWindowInterface();
    /// Main view interface method to get person list
    /**
     * \return ViewBindList<Module::Person> 
     */
    virtual ViewBindList<Person> &getPersonList() = 0;
    /// Main view interface method to get presenter status
    /**
     * \return ViewItem<std::string> 
     */
    virtual ViewItem<std::string> &getStatus() = 0;
    /// Main view interface method to read the edit control
    virtual void readEditControl() = 0;
    /// Main view interface method
    /**
     * \param[in] message message from presenter
     */
    virtual void showError ( std::string message ) = 0;
    /// Notify from a dbworker thread 
    /**
     */
};

} /* namespace Module */

#endif /* MAINWINDOWINTERFACE_H_ */
