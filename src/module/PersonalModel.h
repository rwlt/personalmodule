/*
 * PersonalModel.h
 *
 *  Created on: 20/07/2017
 *      Author: rodney
 */

#ifndef SRC_MODULE_PERSONALMODEL_H_
#define SRC_MODULE_PERSONALMODEL_H_

#include "Person.h"
#include "Entity.h"
#include "storage/DBStorage.h"
#include "repository/Repository.h"
#include "ViewIdentity.h"
#include "validate/ValidationError.h"
#include <string>
#include <vector>
#include <thread>
#include <future>
#include <tuple>

namespace Module {

/// PersonalModel

class PersonalModel {

	std::string m_dbVersion { "Module DBStorage 1.0 - September 6, 2016" };
	std::string m_dbConfigXml {
			"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
					"<module-dbstorage>\n"
					"  <database>\n"
					"   <file role=\"live\" driver=\"firebird\" name=\"/home/rodney/databases/personal.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
					"   <file role=\"test\" driver=\"firebird\" name=\"/home/rodney/databases/utest.fdb\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
					"  </database>\n"
					"<table entity=\"Module::Person\" name=\"Person\" key=\"PersonID\">\n"
					"  <database-statements>\n"
					"      <statement type=\"select\">\n"
					"        <field name=\"PersonID\" type=\"autogenint\"/>\n"
					"        <field name=\"Firstname\" type=\"string\"/>\n"
					"        <field name=\"Surname\" type=\"string\"/>\n"
					"      </statement>\n"
					"  </database-statements>\n"
					"</table>\n"
					"<table entity=\"Module::Hero\" name=\"Hero\" key=\"HeroID\">\n"
					"  <database-statements>\n"
					"      <statement type=\"select\">\n"
					"        <field name=\"HeroID\" type=\"integer\"/>\n"
					"        <field name=\"PersonID\" type=\"integer\"/>\n"
					"        <field name=\"StatuteID\" type=\"integer\"/>\n"
					"      </statement>\n"
					"  </database-statements>\n"
					"</table>\n"
					"<table entity=\"Module::Statute\" name=\"Statute\" key=\"StatuteID\">\n"
					"  <database-statements>\n"
					"      <statement type=\"select\">\n"
					"        <field name=\"StatuteID\" type=\"integer\"/>\n"
					"        <field name=\"Title\" type=\"string\"/>\n"
					"        <field name=\"Quota\" type=\"integer\"/>\n"
					"      </statement>\n"
					"  </database-statements>\n"
					"</table>\n"
					"<table entity=\"Module::ChestItems\" name=\"ChestItems\" key=\"ChestItemID\">\n"
					"  <database-statements>\n"
					"      <statement type=\"select\">\n"
					"        <field name=\"ChestItemID\" type=\"integer\"/>\n"
					"        <field name=\"InventoryID\" type=\"integer\"/>\n"
					"        <field name=\"ChestID\" type=\"integer\"/>\n"
					"      </statement>\n"
					"  </database-statements>\n"
					"</table>\n"
					"<table entity=\"Module::Chest\" name=\"Chest\" key=\"ChestID\">\n"
					"  <database-statements>\n"
					"      <statement type=\"select\">\n"
					"        <field name=\"ChestID\" type=\"integer\"/>\n"
					"        <field name=\"Name\" type=\"string\"/>\n"
					"        <field name=\"PersonID\" type=\"integer\"/>\n"
					"      </statement>\n"
					"  </database-statements>\n"
					"</table>\n"
					"<table entity=\"Module::Inventory\" name=\"Inventory\" key=\"InventoryID\">\n"
					"  <database-statements>\n"
					"      <statement type=\"select\">\n"
					"        <field name=\"InventoryID\" type=\"integer\"/>\n"
					"        <field name=\"Quota\" type=\"integer\"/>\n"
					"        <field name=\"Name\" type=\"string\"/>\n"
					"      </statement>\n"
					"  </database-statements>\n"
					"</table>\n"
					"<procedure entity=\"Module::InventoryCount\" name=\"InventoryCount\" key=\"InventoryID\">\n"
					"  <database-statements>\n"
					"      <statement type=\"select\">\n"
					"        <parameters>\n"
					"          <parameter name=\"InventoryID\" type=\"integer\"/>\n"
					"        </parameters>\n"
					"        <fields>\n"
					"          <field name=\"InventoryID\" type=\"integer\"/>\n"
					"          <field name=\"Quota\" type=\"integer\"/>\n"
					"          <field name=\"Name\" type=\"string\"/>\n"
					"          <field name=\"CountItem\" type=\"integer\"/>\n"
					"        </fields>\n"
					"      </statement>\n"
					"  </database-statements>\n"
					"</procedure>\n"
					"</module-dbstorage>\n" };
	std::string m_UserName;
	std::string m_UserPassword { "" };
	std::string m_UserRole { "" };
	Storage::DBStorage m_dbStorage;
	Repository::Repository<Module::Person> m_repoPerson;
	bool m_hasLoggeduser { false };
	std::vector<std::function<void ( Module::Person&, bool)>> m_broadcastPersonChange;

public:
	PersonalModel() : m_UserName(""),
			m_dbStorage(m_dbVersion, m_dbConfigXml.c_str()),
			m_repoPerson(&m_dbStorage, "PersonID") {
	}
	PersonalModel(std::string version, const char *xml) :
			m_dbVersion(version), m_dbConfigXml(xml),
			m_dbStorage(version, xml),
			m_repoPerson(&m_dbStorage, "PersonID") {
	}
	virtual ~PersonalModel() {
	}

	/// Add Broadcast function
	void addBroadcast(std::function<void ( Module::Person&, bool)> func) {
		this->m_broadcastPersonChange.push_back(func);
	}

	/// loggedUser
	/**
	 * \return bool
	 */
	bool loggedUser() {
		return (m_hasLoggeduser);
	}
	///User Name set
	/**
	 * \param value
	 */
	void setUserName(const std::string &value) {
		m_UserName = value;
	}
	///User name get
	/**
	 * \return string
	 */
	std::string getUserName() {
		return m_UserName;
	}

	///User Name set
	/**
	 * \param value
	 */
	void setPassword(const std::string &value) {
		m_UserPassword = value;
	}
	///User name get
	/**
	 * \return string
	 */
	std::string getUserRole() {
		return m_UserRole;
	}

	///User Name set
	/**
	 * \param value
	 */
	void setUserRole(const std::string &value) {
		m_UserRole = value;
	}
	///User name get
	/**
	 * \return string
	 */
	std::string getPassword() {
		return m_UserPassword;
	}

	///DB Config xml
	/**
	 * \return string
	 */
	const std::string &getDBConfigXml() {
		return m_dbConfigXml;
	}

	///DB Version description
	/**
	 * \return string
	 */
	const std::string &getDBVersion() {
		return m_dbVersion;
	}

	/// Login user view
	/**
	 * \param user string
	 * \param password string
	 * \param role Enum UserRole
	 * \return tuple bool and string
	 * Return bool true if login succesful, empty string.
	 * Can return true if have a logged user with error string to logout user before log in new user.
	 * Error string holds the error message .
	 */
	std::tuple<bool, std::string> loginUser(const std::string user, const std::string password,
			const Module::UserRole &role = Module::UserRole::GUEST);
	/// Logout user
	void logoutUser();
	///Validate person
	/**
	 * \param person
	 * \return tuple bool and vector ValidationError
	 * Return bool true if valid, empty validation errors.
	 * Return bool false if not valid with person validation errors.
	 */
	std::vector<Validate::ValidationError> validatePerson(const Module::Person &person);
	/**
	 * \param addPerson string
	 * \return Module::Person
	 * Return when successful the added Person
	 * Throws ModuleException when no logged user or database error in dbStorage object
	 */
	Module::Person addNewPerson(const Module::Person &addPerson);
	/// Change person
	/**
	 * \param origPerson string
	 * \param changePerson string
	 * \return Module::Person
	 * Return when successful the added Person
	 * Throws ModuleException when no logged user or database error in dbStorage object
	 */
	Module::Person changePerson(const Module::Person &origPerson, const Module::Person &changePerson);
	/// Remove person
	void removePerson(const Module::Person &removePerson);

	/// Read the db storage
	/**
	 * Using a template method to retrieve DataType entities in a DBResult
	 * \tparam T entity type mainly
	 * \tparam A entity map
	 * \tparam M entity traits
	 * \return Control::List<T>
	 */
	template<class T, typename A = Storage::EntityMap<T>, typename M = Storage::EntityTraits<T>>
	std::vector<T> doRead() {
		std::vector<T> dest;
		if (loggedUser()) {
			Storage::DBRetrieveMap where;
			Storage::DBRetrieveMap params;
			std::future<std::vector<T>> result(std::async(std::launch::async, &Storage::DBStorage::doRead<T>, &m_dbStorage, where, params));
			dest = result.get(); // wait her for async to finish
		}
		return dest;
	}

	std::vector<Module::Person> doRead() {
		std::vector<Module::Person> dest;
		//std::this_thread::sleep_for(std::chrono::milliseconds(5000));
		if (loggedUser()) {
			// Some how need to find when to reset this repo on a new doRead();
			if (m_repoPerson.size() > 0) {
				return m_repoPerson.getItems();
			}
			m_repoPerson.clear();
			m_repoPerson.trackStateChanges(false);
			Storage::DBRetrieveMap where;
			Storage::DBRetrieveMap params;
			std::future<std::vector<Module::Person>> result(std::async(std::launch::async, &Storage::DBStorage::doRead<Module::Person>, &m_dbStorage, where, params));
			dest = result.get(); // wait her for async to finish
			Storage::EntityMap<Module::Person> tent;
			int key;
			for (Module::Person& t: dest) {
				tent.getField(t, m_repoPerson.getKeyField(), key);
				m_repoPerson.addItem(key, t);
			}
			m_repoPerson.trackStateChanges(true);
			return m_repoPerson.getItems();

		}
		return dest;
	}
	/// Get Data from a doRead
	/**
	 * \param fraction_done
	 * \param message
	 */
	void get_data(double *fraction_done, std::string *message) const {
		m_dbStorage.get_data(fraction_done, message);
	}
	/// Stop Work of a doRead
	void stop_work() {
		m_dbStorage.stop_work();
	}

private:
	std::string validationMessage(const std::vector<Validate::ValidationError> &errors);

};

} /* namespace Module */

#endif /* SRC_MODULE_PERSONALMODEL_H_ */
