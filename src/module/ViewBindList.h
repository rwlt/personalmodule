#ifndef VIEWBINDLIST_H_
#define VIEWBINDLIST_H_

#include "storage/Bind_iterator.h"

namespace Module
{

/// ViewList<DataType>: Used to move data to the view and using Control::Bind_iterator() to bring it back
/**
 * \tparam DataType template on data type
 */
template<class DataType, typename KeyType = int>
class ViewBindList
{
public:
    /// Virtual deconstructor
    virtual ~ViewBindList() {}

    /// Use a bind iterator of DataType
    /**
     * The iterator will do Bind on DataType doUpdate
     * \sa Control::Bind_iterator
     */
    typedef Storage::Bind_iterator<DataType> iterator;
    /// begin iterator
    virtual iterator begin() = 0;
    /// end iterator
    virtual iterator end() = 0;
    /// Add item to collection
    /**
     * The item is bind to the list view component view model
     * \param item DataType value of item
     */
    virtual void add ( DataType item ) = 0;
    /// clear the list
    virtual void clear() = 0;
	/// remove
	/**
	 * The item removed from the list view component view model
	 * \tparam KeyType
	 * \param id  value of id for DataType Module::Person in this case
	 * \return integer 1 based position in the container
	 */
	virtual void remove(KeyType id) = 0;
	/// findById
	/**
	 * \param id  value of id for DataType Module::Person in this case
	 * \return iterator in the container
	 */
	virtual iterator findById(KeyType id) = 0;
    /// Size of the collection
    /**
     * \return int of count items
     */
    virtual unsigned int size() = 0;
    /// set position to use for selected item using 1 based numbering
    /**
     * \param value int of position
     */
    virtual void setPosition ( int value ) = 0;
    /// get position value
    /**
     * \return value int of position
     */
    virtual int getPosition() = 0;
    /// get selected item at position
    /**
     * \return DataType reference of item at position(selected)
     */
    virtual DataType getSelectItem() = 0;

};
}
#endif /* VIEWBINDLIST_H_ */
