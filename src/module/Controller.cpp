/*
 * Controller.cpp
 */
#include "Controller.h"
#include <iostream>
#include <string>
#include <sstream>
#include "AbstractViewFactory.h"
#include "AbstractPresenter.h"
#include "MainWindowInterface.h"
#include "PersonWindowInterface.h"
#include "DBLoginWindowInterface.h"
#include "MainPresenter.h"
#include "PersonalModel.h"
#include "storage/DBStorage.h"
#include "CommandAddPerson.h"
#include "CommandChangePerson.h"
#include "CommandRemovePerson.h"
#include "Person.h"
#include "PersonValidate.h"
#include "Hero.h"
#include "Entity.h"
#include "Message.h"
#include <memory>

namespace Module
{

Controller::Controller(Module::AbstractViewFactory *abstractViewFactory) :
    abstractViewFactory(abstractViewFactory), m_personalModel(new PersonalModel())
{


}

Controller::~Controller()
{
}
//
///////////////////////////////////////////////////////////////////////////////
// Load a View from a factory
///////////////////////////////////////////////////////////////////////////////
//
void Controller::load(Module::ViewIdentity viewIdentity,
                      Module::ViewIdentity parentViewIdentity)
{
    // Could we set up the presenter Viewloader here with
    if (abstractViewFactory == 0) {
        return;
    }
    if (parentViewIdentity != Module::ViewIdentity::NullView) {
        abstractViewFactory->createInstance(this, viewIdentity, parentViewIdentity);
    } else {
        abstractViewFactory->createInstance(this, viewIdentity);
    }

    // Check we made a view
    MainWindowInterface *mainview;
    PersonWindowInterface *personview;
    DBLoginWindowInterface *loginview;
    switch (viewIdentity) {
    case Module::ViewIdentity::MainWindow:
        mainview = abstractViewFactory->mainInterface();
        if (mainview == nullptr) {
            return;
        }
        break;
    case Module::ViewIdentity::PersonEdit:
        personview = abstractViewFactory->personInterface();
        if (personview == nullptr) {
            return;
        }
        break;
    case Module::ViewIdentity::LoginUser:
        loginview = abstractViewFactory->loginInterface();
        if (loginview == nullptr) {
            return;
        }
        break;
    default:
        break;
    }


}
//
///////////////////////////////////////////////////////////////////////////////
// Close a view and show next
///////////////////////////////////////////////////////////////////////////////
//
void Controller::close(Module::ViewIdentity viewIdentity)
{
    if (abstractViewFactory == 0) {
        return;
    }
    MainWindowInterface *mainview;
    PersonWindowInterface *personview;
    DBLoginWindowInterface *loginview;
    Module::Message viewMessage; // Setup the module message for Views
    Module::Parameter paramArgs;
    paramArgs.kind = Module::ParameterKind::ONE_ARG;
    paramArgs.args.oneArg.message = 1;
    viewMessage.setParameter(paramArgs);

    switch (viewIdentity) {
    case Module::ViewIdentity::MainWindow:
        mainview = abstractViewFactory->mainInterface();
        if (mainview == nullptr) {
            return;
        }
        break;
    case Module::ViewIdentity::PersonEdit:
		{
			personview = abstractViewFactory->personInterface();
			if (personview == nullptr) {
				return;
			}
			personview->closeView();
			show(Module::ViewIdentity::MainWindow, viewMessage);
		}
        break;
    case Module::ViewIdentity::LoginUser:
        loginview = abstractViewFactory->loginInterface();
        if (loginview == nullptr) {
            return;
        }
        loginview->closeView();
        commandController = CommandController(); // clear user undo redo
        doPopulatePersonViewList();
        show(Module::ViewIdentity::MainWindow, viewMessage);
        break;
    default:
        break;
    }
    // After any other controlls done above ready to finish with this view.
    abstractViewFactory->destroyInstance(viewIdentity);
}
//
///////////////////////////////////////////////////////////////////////////////
// handle MessageBox view IViewLoader interface
///////////////////////////////////////////////////////////////////////////////
//
void Controller::show(Module::ViewIdentity viewIdentity,
                      Module::Message message)
{
    if (abstractViewFactory == 0) {
        return;
    }
    View *view;
    switch (viewIdentity) {
    case Module::ViewIdentity::MainWindow:
        view = abstractViewFactory->mainInterface();
        break;
    case Module::ViewIdentity::PersonEdit:
        view = abstractViewFactory->personInterface();
        break;
    case Module::ViewIdentity::LoginUser:
        view = abstractViewFactory->loginInterface();
        break;
    default:
        view = nullptr;
        break;
    }
    if (view == nullptr) {
        return;
    }
    if (view->moduleViewIdentity() != viewIdentity) {
    	return;
    }
    if (view->m_modulePresenter != nullptr)
        view->m_modulePresenter->loadMessage(message); // Controller can send message

    view->showView(message);
}
//
///////////////////////////////////////////////////////////////////////////////
// Create the main view as defined in the app using this controller
///////////////////////////////////////////////////////////////////////////////
//
void Controller::run()
{
    if (abstractViewFactory == 0) {
        return;
    }
    Module::Message viewMessage;
    Module::Parameter paramArgs;
    paramArgs.kind = Module::ParameterKind::ONE_ARG;
    paramArgs.args.oneArg.message = 1;
    viewMessage.setParameter(paramArgs);
    load(Module::ViewIdentity::MainWindow);
    show(Module::ViewIdentity::MainWindow, viewMessage);

}

/**
 * Called when actions change the data
 */
void Controller::doPopulatePersonViewList()
{
	Module::Message message { };
	Module::Parameter paramArgs;
	paramArgs.kind = Module::ParameterKind::ONE_ARG;
	paramArgs.args.oneArg.message = 2; // Worker complete for view data
	message.setParameter(paramArgs);
    show(Module::ViewIdentity::MainWindow, message);

}


}

