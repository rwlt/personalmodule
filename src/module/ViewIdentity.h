/*
 * ViewIdentity.h
 */

#ifndef VIEWIDENTITY_H_
#define VIEWIDENTITY_H_


namespace Module
{
/// Identity of Views
enum ViewIdentity {
    MainWindow = 0,  ///< Main windows
    ResultView = 1,  ///< Results
    ToolPopup = 2,   ///< Tool popup
    PersonEdit = 3,  ///< Person edit modal window
    LoginUser = 4,   ///< DB Login user modal window
    TotalViews = 5,  ///< Total views
    NullView = 10    ///< Null view
};
/// State category of View
enum class ViewEditState : char {
    AddNew = 0,  ///< New item is to be added
    Change = 1,  ///< An item is edited after being selected
	Removed = 2  ///< An item was removed
};
/// Role category of User
enum class UserRole : char {
    GUEST = 0,  ///< All users with Guest role
    ADMIN = 1,  ///< Administrator role
};

/// LoggedState of User
enum class LoggedState : char {
    LOGGED = 0,     ///< A user is logged in
    NOTLOGGED = 1,  ///< No user logged
};

}


#endif /* VIEWIDENTITY_H_ */
