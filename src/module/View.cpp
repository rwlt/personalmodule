/*
 * view.cpp
 */

#include "View.h"

namespace Module
{

View::View(Module::ViewIdentity identity, bool emit_view) : 
   m_viewIdentity{identity},
   m_emit_view{emit_view}
{

}

View::~View()
{
}

ViewIdentity View::moduleViewIdentity()
{
    return (m_viewIdentity);
}

void View::modulePresenter(AbstractPresenter* abstract_presenter) { 
    m_modulePresenter = abstract_presenter;
};



}

