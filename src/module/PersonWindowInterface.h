/*
 * PersonWindowInterface.h
 */

#ifndef PERSONWINDOWINTERFACE_H_
#define PERSONWINDOWINTERFACE_H_

#include "View.h"
#include "ViewList.h"
#include "ViewItem.h"
#include "validate/ValidationError.h"
#include "Entity.h"

namespace Module
{
    
class Person;

/// Person view interface which should be implemented to use the person presenter
/**
 * \sa Module::PersonPresenter()
 */
class PersonWindowInterface: public View
{
public:
    /// Constructor
    PersonWindowInterface() : View ( ViewIdentity::PersonEdit ) {};
    /// Virtual deconstructor
    virtual ~PersonWindowInterface() {};

    /// Person view interface method to get a person
    /**
     * \return ViewItem<Module::Person>
     */
    virtual ViewItem<Person> &getPerson() = 0;
    /// Person view interface method to get a person validation errors
    /**
     * \return ViewList<Validate::ValidationError>
     */
    virtual ViewList<Validate::ValidationError> &getValidationError() = 0;
    /// Person view interface method to get presenter status
    /**
     * \return ViewItem<std::string>
     */
    virtual ViewItem<std::string> &getStatus() = 0;
    /// Person view interface method to read the edit control
    virtual void readEditControl() = 0;
    /// get view edit state to set state
    /**
     * \return ViewItem of View edit state enum
     * \sa Module::ViewEditState
     */
    virtual ViewItem<Module::ViewEditState> &getState () = 0;
};

} /* namespace Module */

#endif /* PERSONWINDOWINTERFACE_H_ */
