#ifndef MODULE_EXCEPTION_H_
#define MODULE_EXCEPTION_H_

#include <exception>
#include <string>

namespace Module
{

/// Standard exception override
class ModuleException: public std::exception
{
public:
    /// Constructor
	ModuleException ( std::string what ) :reason ( what ) {
    }
    /// Virtual Deconstructor
    virtual ~ModuleException() {
    }
    /// What went wrong
    const char *what() const _GLIBCXX_USE_NOEXCEPT {
        return ( reason.c_str() );
    }

private:
    std::string reason;

};

} /* namespace Module */

#endif /* MODULE_EXCEPTION_H_ */
