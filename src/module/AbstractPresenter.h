/*
 * AbstractPresenter.h
 */

#ifndef ABSTRACTPRESENTER_H_
#define ABSTRACTPRESENTER_H_

/// Module Presenter
/**
 * Namespace for module presenter
 */
namespace Module
{

class Controller;
class Message;

/// Abstract Presenter base
class AbstractPresenter
{
public:
    /// Constructor
    AbstractPresenter ( Module::Controller *controller );
    /// Virtual deconstructor
    virtual ~AbstractPresenter();

    /// Module controller access from presenters
    virtual Module::Controller* ctrl() { return m_ctrl; };

protected:
    /// Load message to the presenters from a view
    /**
     * \param message const reference to message
     */
    virtual void loadMessage ( const Module::Message &message ) = 0;

private:
    Module::Controller *m_ctrl;
    friend class Controller;
};

} /* namespace Module */

#endif /* ABSTRACTPRESENTER_H_ */
