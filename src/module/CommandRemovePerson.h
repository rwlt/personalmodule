/*
 * CommandRemovePerson.h
 */

#ifndef COMMANDREMOVEPERSON_H_
#define COMMANDREMOVEPERSON_H_

#include "Command.h"
#include "PersonalModel.h"
namespace Module
{
/// Command Remove Person
class CommandRemovePerson : public Command
{
    PersonalModel* const m_model;
    Person m_person;

public:
    /// Constructor
    /**
     * \param model
     * \param person person added
     */
    CommandRemovePerson (  PersonalModel* const model, Person person ) :
         m_model ( model ), m_person(person) {}

    /// redo
    void redo()       {
        if (m_model->loggedUser()) {
			m_model->removePerson(m_person);
        }
    }
    /// undo
    void undo()       {
        if (m_model->loggedUser()) {
			auto result = m_model->addNewPerson(m_person);
			m_person = result;
        }
    }
};


} /* namespace Module */

#endif /* COMMANDREMOVEPERSON_H_ */

