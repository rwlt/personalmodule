#ifndef D_HEROSTOREBIND_H
#define D_HEROSTOREBIND_H

#include "storage/RowRecord.h"
#include "storage/RowBind.h"
#include "../Hero.h"

namespace Module
{
namespace Data
{
/// StatuteStoreBind
class StatuteStoreBind: public Storage::Bind<Storage::RowRecord>
{
public:
    /// Constructor
    /**
     * \param dest pointer to bind source
     */
    StatuteStoreBind (std::vector<Module::Statute> *dest ) :
        destSource ( dest )
    {};
    /// Virtual deconstructor
    virtual ~StatuteStoreBind() {};
    /// doBind - when an item is add or set with data - flow to dest source
    /**
     * \param item const reference to Storage::RowRecord()
     */
    void doBind(const Storage::RowRecord &item) {
        std::string&&  value = item.getField1();
        destSource->push_back ( Module::Statute(11, value )); // We add the items as new when source binds to dest
    };
    /// doBind - when an item is add or set with data - flow to dest source
    /**
     * \param item const reference to Storage::RowRecord()
     * \param index
     */
    void doBindUpdate(const Storage::RowRecord &item, int index) {
        std::string&&  value = item.getField1();
        destSource->at(index) = Module::Statute(11, value ); // We add the items as new when source binds to dest
    };
    /// doBind - when an item is add or set with data - flow to dest source
    /**
     * \param item const reference to Storage::RowRecord()
     * \param index
     */
    void doBindClear(const Storage::RowRecord &, int index) {
        destSource->at(index) = Module::Statute(); // We add the items as new when source binds to dest
    };
    /// doUpdate - update an item from dest to source (BindSource items)
    /**
     * \param item const reference to Storage::RowRecord()
     * \param index
     */
    void doUpdate(Storage::RowRecord& item, int index) {
        Module::Statute s = destSource->at(index);
        const std::string&& status = s.getTitle();
        item.setField1 ( status );
    };

    /// destination source
    std::vector<Module::Statute> *destSource;
    //HeroStoreBind ( const HeroStoreBind & );            // Default copyable
    //HeroStoreBind operator = ( const HeroStoreBind & ); // Default assign

};


}

}

#endif  // D_HEROSTOREBIND_H
