#ifndef D_PERSONSTOREBIND_H
#define D_PERSONSTOREBIND_H

#include "storage/RowRecord.h"
#include "../Person.h"
#include "sstream"

namespace Module
{

namespace Data
{

/// PersonStoreBind
class PersonStoreBind: public Storage::Bind<Storage::RowRecord>
{
public:
    /// Constructor
    /**
     * \param dest pointer to bind source
     */
    PersonStoreBind ( std::vector<Module::Person>* dest ) :
        destSource ( dest )
    {};
    virtual ~PersonStoreBind() {};
    /// doBind - when an item is add or set with data - flow to dest source
    /**
     * \param item const reference to Storage::RowRecord()
     */
    void doBind ( const Storage::RowRecord &item) {
        // Know its a RowRecord to get from filestorage source
        //Storage::RowRecord storeRow = fileStorage->getStoreSource()->getItem ( notify.getIndex() );
        // Read the item value from file storage record
        std::string&&  value = item.getField1();
        std::string&&  value2 = item.getField2();
        std::string&&  value3 = item.getField3();
        int id;
        try {
            id = std::stoi ( value );
        } catch ( std::invalid_argument& e ) {
            id = 1;
        }
        //std::cout << "FileStorage source to dest source doBind\n" << value << '\n' << value2 << '\n';
        destSource->push_back ( Module::Person ( id, value3, value2 ) ); // We add the items as new when source binds to dest
    };
    /// doBind - when an item is add or set with data - flow to dest source
    /**
     * \param item const reference to Storage::RowRecord()
     * \param index
     */
    void doBindUpdate ( const Storage::RowRecord &item, int index) {
        // Know its a RowRecord to get from filestorage source
        //Storage::RowRecord storeRow = fileStorage->getStoreSource()->getItem ( notify.getIndex() );
        // Read the item value from file storage record
        std::string&&  value = item.getField1();
        std::string&&  value2 = item.getField2();
        std::string&&  value3 = item.getField3();
        int id;
        try {
            id = std::stoi ( value );
        } catch ( std::invalid_argument& e ) {
            id = 1;
        }
        //std::cout << "FileStorage source to dest source doBind\n" << value << '\n' << value2 << '\n';
        destSource->at(index) = ( Module::Person ( id, value3, value2 ) ); // We add the items as new when source binds to dest
    };
    /// doBind - when an item is add or set with data - flow to dest source
    /**
     * \param item const reference to Storage::RowRecord()
     * \param index
     */
    void doBindClear ( const Storage::RowRecord &, int index) {
        destSource->at(index) = ( Module::Person () ); // We add the items as new when source binds to dest
    };
    /// doUpdate - update an item from dest to source (BindSource items)
    /**
     * \param item const reference to Storage::RowRecord()
     * \param index
     */
    void doUpdate ( Storage::RowRecord &item, int index ) {
        // Know its a RowRecord to get from filestorage source
        //Storage::RowRecord storeRow = fileStorage->getStoreSource()->getItem ( notify.getIndex() );
        // Read the item value from dest source
        Module::Person person = destSource->at ( index );
        std::stringstream num;
        num << person.getPersonID();
        const std::string  id = num.str();
        const std::string&& firstname = person.getFirstname();
        const std::string&& surname = person.getSurname();
        //std::cout << "Dest source to FileStorage source doUpdate\n" << firstname << '\n' << surname << '\n';
        item.setField1 ( id );
        item.setField2 ( firstname );
        item.setField3 ( surname );
        //Update RowRecord to filestorage source
        //fileStorage->getStoreSource()->setItem ( notify.getIndex(), storeRow );
    };

    /// destination source
   std::vector<Module::Person> *destSource;

};


}

}

#endif  // D_PERSONSTOREBIND_H
