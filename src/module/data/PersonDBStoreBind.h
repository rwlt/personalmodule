#ifndef D_PERSONDBSTOREBIND_H
#define D_PERSONDBSTOREBIND_H

#include "storage/RowItem.h"
#include "../ViewBindList.h"
#include "../Person.h"
#include <iostream>
namespace Module
{

namespace Data
{

/// Person DBStoreBind
class PersonDBStoreBind: public Storage::Bind<Storage::RowItem>
{
public:
    /// Constructor
    /**
     * \param dest pointer to bind source
     */
    PersonDBStoreBind ( Module::ViewBindList<Module::Person>* dest ) :
        destList ( dest )
    {};
    virtual ~PersonDBStoreBind() {};
    /// doBind - when an item is add or set with data
    /**
     * \param item const reference to Storage::RowItem()
     */
    void doBind ( const Storage::RowItem &item ) {
        int id;
        std::string fname;
        std::string sname;
        bool is_set;
        item.getField ( 1, id, is_set );
        item.getField ( 2, fname, is_set);
        item.getField ( 3, sname, is_set);
        Module::Person p ( id, sname, fname );
        //std::cout << "doBind main fromDB " << id << " " << fname << " " << sname << std::endl;
        destList->add( p ); // We add the items as new when source binds to dest
    };
    /// doBind - when an item is add or set with data
    /**
     * \param item const reference to Storage::RowItem()
     * \param index
     */
    void doBindUpdate ( const Storage::RowItem &item, int index ) {
    };
    /// doBind - when an item is add or set with data
    /**
     * \param item const reference to Storage::RowItem()
     * \param index
     */
    void doBindClear ( const Storage::RowItem &item, int index ) {
    };
    /// doUpdate - update an item from dest to source 
    /**
     * \param item const reference to Storage::RowItem()
     * \param index
     */
    void doUpdate ( Storage::RowItem &item, int index ) {
    };

    /// destination view list
    Module::ViewBindList<Module::Person> *destList;

};


}

}

#endif  // D_PERSONDBSTOREBIND_H
