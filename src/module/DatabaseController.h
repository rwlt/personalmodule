/*
 * DatabaseController.h
 */

#ifndef MODULEDATABASECONTROLLER_H_
#define MODULEDATABASECONTROLLER_H_

#include "storage/DBStorage.h"


namespace Module
{

/// DatabaseController
class DatabaseController
{
    //Module::Data::PersonDBStoreBind m_personDBStoreBind(&viewPersonDBList);
    //Module::Data::HeroDBStoreBind m_heroDBStoreBind(&ctrl()->sourceDBHero);
    Storage::DBStorage dbStorage;
    
public:
    DatabaseController(): dbStorage("", std::string("").c_str()) 
    {
    };

};

} /* namespace Module */

#endif /* MODULEDATABASECONTROLLER_H_ */
