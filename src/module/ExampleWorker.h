#ifndef GTKMM_EXAMPLEWORKER_H
#define GTKMM_EXAMPLEWORKER_H

#include <thread>
#include <mutex>

namespace Module
{

class View;

/// ExampleWorker
/**
 * Process as thread to do work
 */
class ExampleWorker
{
public:
    /// Example worker constructor
    ExampleWorker();
    /// Example worker deconstructor
    ~ExampleWorker();

    /// get data thread safe
    /**
     * \param fraction_done double
     * \param message string
     */
    void get_data ( double *fraction_done, std::string *message ) const;
    /// start work
    /**
     * \param caller View interface pointer
     */
    void start_work (View *caller);
    /// stop work
    void stop_work();
    /// has work stopped
    bool has_stopped() const;
    /// join completed work (or detach)
    void join_detach();

private:
    // Thread function.
    void do_work ();

    std::thread m_worker_thread;
    View *m_caller_view;
    
    // Synchronizes access to member data.
    mutable std::mutex m_Mutex;

    // Data used by both GUI thread and worker thread.
    bool m_shall_stop;
    bool m_has_stopped;
    double m_fraction_done;
    std::string m_message;
};

}
#endif // GTKMM_EXAMPLEWORKER_H
